# micdisk
 智云盘,基于 [go-fastdfs](https://github.com/sjqzhang/go-fastdfs)的可用于企业的私有云盘。

# 基础配置
通过修改`conf/app.conf`文件来对软件的基础配置进行设置。

# 使用
## 登录
![](http://mining-icloud.com/showdoc/server/../Public/Uploads/2021-02-18/602dc922cdde9.png)

## 注册
![](http://mining-icloud.com/showdoc/server/../Public/Uploads/2021-02-18/602dc9629ce06.png)

## 主页面
![](http://mining-icloud.com/showdoc/server/../Public/Uploads/2021-02-18/602dca51f06bc.png)

## 文件操作(单击文件)
![](http://mining-icloud.com/showdoc/server/../Public/Uploads/2021-02-18/602dca9cb3d92.png)

## 文件夹操作
### 单击文件夹
![](http://mining-icloud.com/showdoc/server/../Public/Uploads/2021-02-18/602dcaeca3e88.png)
### 双击文件夹
打开文件夹
![](http://mining-icloud.com/showdoc/server/../Public/Uploads/2021-02-18/602dcb1644373.png)

# 其他
文件共享等相关选项正在开发中。
## 共享系统的基本设计
作为企业私有云盘，不能像百度网盘那样只能以连接的形式进行共享文件。而应该像`Windows`的共享文件夹那样，直接将文件和文件夹共享给相关用户组或者个人，而共享时可选`可以上传(对文件夹)`和`可以下载`选项。
>`可以上传`是指用户可以在共享文件夹中上传文件，上传的文件被共享的所有用户都能看到。
>`可以下载`是指可以用户可以下载被共享的文件。
>以上两者皆不选，则被共享的用户只能浏览有哪些文件，而不能上传和下载。
>亦可像百度网盘那样，以连接和密码的形式公开限时分享文件。

用户被共享了文件和文件夹后，无需接受，被共享的文件和文件夹自动出现在`分享给我的`文件夹中。
