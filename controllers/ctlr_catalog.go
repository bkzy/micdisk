package controllers

import (
	"fmt"
	"micdisk/models"
	"strconv"
	"strings"
	"time"
)

type CatalogController struct {
	MainController
}

//新建文件夹
func (c *CatalogController) NewFolder() {
	resp := new(JsonResult)
	user := c.CheckSession()
	if user.Id > 0 {
		//参数
		type paramslist struct {
			Pid  int64  `form:"pid"`         //父节点
			Name string `form:"name"`        //名称
			Desc string `form:"decsription"` //说明
		}
		params := new(paramslist)
		if err := c.ParseForm(params); err != nil { //获取参数错误
			resp.Status = "fail-a"
			resp.Message = fmt.Sprintf("Parameter input error:%s", err.Error())
			c.NewLog("Access API", "WARN", "C", resp.Message)
		} else {
			uc := new(models.UserCatalog)
			puc := new(models.UserCatalog)
			puc.Id = params.Pid
			if err := puc.Read(); err != nil || puc.User.Id == 0 { //无父层级
				uc.User = user //属于当前登录用户所有
			} else { //有父层级
				uc.User = puc.User //继承父层级的用户
			}
			uc.UploadUser = user
			uc.Pid = params.Pid
			uc.IsDir = true

			uc.Description = params.Desc
			uc.Name = params.Name
			uc.CheckAndReName()
			if err := uc.Create(); err != nil {
				resp.Status = "fail-b"
				resp.Message = fmt.Sprintf("Create folder fault:%s!", err.Error())
				c.NewLog("Access API", "WARN", "C", resp.Message)
			} else {
				resp.Status = "ok"
				resp.Message = fmt.Sprintf("Folder %s created successfully!", uc.Name)
				resp.Data = uc
				c.NewLog("Access API", "INFO", "C", resp.Message)
			}
		}
		c.Data["json"] = resp
		c.ServeJSON()
	}
}

//新建文件
func (c *CatalogController) NewFile() {
	resp := new(JsonResult)
	user := c.CheckSession()
	if user.Id > 0 {
		//参数
		type paramslist struct {
			Pid    int64  `form:"pid"`    //父节点
			Name   string `form:"name"`   //名称
			FileId int64  `form:"fileid"` //文件id
		}
		params := new(paramslist)
		if err := c.ParseForm(params); err != nil { //获取参数错误
			resp.Status = "fail-a"
			resp.Message = fmt.Sprintf("Parameter input error:%s", err.Error())
			c.NewLog("Access API", "WARN", "C", resp.Message)
		} else {
			uc := new(models.UserCatalog)
			puc := new(models.UserCatalog)
			puc.Id = params.Pid
			if err := puc.Read(); err != nil || puc.User.Id == 0 { //无父层级
				uc.User = user //属于当前登录用户所有
			} else { //有父层级
				uc.User = puc.User //继承父层级的用户
			}
			uc.UploadUser = user
			uc.Pid = params.Pid
			uc.IsDir = false
			file := new(models.DfsFileIndex)
			file.Id = params.FileId
			if err := file.Read(); err != nil {
				resp.Status = "fail-b"
				resp.Message = fmt.Sprintf("The file ID does not exist:%s!", err.Error())
				c.NewLog("Access API", "WARN", "C", resp.Message)
			} else {
				uc.File = file
				uc.Name = params.Name
				uc.CheckAndReName()
				uc.FlashTrans = file.ModTime-time.Now().Unix() < 0
				if err := uc.Create(); err != nil {
					resp.Status = "fail-c"
					resp.Message = fmt.Sprintf("Create file fault:%s!", err.Error())
					c.NewLog("Access API", "WARN", "C", resp.Message)
				} else {
					resp.Status = "ok"
					resp.Message = fmt.Sprintf("File %s created successfully!", uc.Name)
					resp.Data = uc
					c.NewLog("Access API", "INFO", "C", resp.Message)
				}
			}
		}
		c.Data["json"] = resp
		c.ServeJSON()
	}
}

//(逻辑)移除文件或者文件夹
func (c *CatalogController) Remove() {
	resp := new(JsonResult)
	user := c.CheckSession()
	if user.Id > 0 {
		//参数
		type paramslist struct {
			Id        int64 `form:"id"`         //文件或者文件夹节点ID
			ExtendSub bool  `form:"extend_sub"` //true:子文件和文件夹一同移除;false:有子文件和文件夹返回错误信息
		}
		params := new(paramslist)
		if err := c.ParseForm(params); err != nil { //获取参数错误
			resp.Status = "fail-a"
			resp.Message = fmt.Sprintf("Parameter input error:%s", err.Error())
			c.NewLog("Access API", "WARN", "D", resp.Message)
		} else { //获取参数正确
			uc := new(models.UserCatalog)
			uc.Id = params.Id
			if err := uc.Read(); err != nil { //读取目录错误
				resp.Status = "fail-b"
				resp.Message = "The given ID number does not exist!"
				c.NewLog("Access API", "WARN", "D", resp.Message)
			} else {
				if ok := uc.Remove(params.ExtendSub); !ok { //移除过程异常
					resp.Status = "fail-c"
					resp.Message = "The given folder contains subfolders or files, please remove them first. Or extend the remove command to subitems!"
					c.NewLog("Access API", "WARN", "D", resp.Message)
				} else {
					resp.Status = "ok"
					resp.Message = "Remove catalog successfully!"
					resp.Data = ""
					c.NewLog("Access API", "INFO", "D", resp.Message)
				}
			}
		}
		c.Data["json"] = resp
		c.ServeJSON()
	}
}

//物理删除文件或者文件夹
func (c *CatalogController) Delete() {
	resp := new(JsonResult)
	user := c.CheckSession()
	if user.Id > 0 {
		//参数
		type paramslist struct {
			Id        int64 `form:"id"`         //文件或者文件夹节点ID
			Force     bool  `form:"force"`      //true:无条件删除,false:只删除已经(逻辑)移除的项目
			ExtendSub bool  `form:"extend_sub"` //true:子文件和文件夹一同删除;false:有子文件和文件夹返回错误信息
		}
		params := new(paramslist)
		if err := c.ParseForm(params); err != nil { //获取参数错误
			resp.Status = "fail-a"
			resp.Message = fmt.Sprintf("Parameter input error:%s", err.Error())
			c.NewLog("Access API", "WARN", "D", resp.Message)
		} else { //获取参数正确
			uc := new(models.UserCatalog)
			uc.Id = params.Id
			if err := uc.Read(); err != nil { //读取目录错误
				resp.Status = "fail-b"
				resp.Message = "The given ID number does not exist!"
				c.NewLog("Access API", "WARN", "D", resp.Message)
			} else {
				if ok := uc.DeleteMul(params.Force, params.ExtendSub); !ok { //移除过程异常
					resp.Status = "fail-c"
					resp.Message = "The given folder contains subfolders or files, please delete them first. Or extend the delete command to subitems!"
					c.NewLog("Access API", "WARN", "D", resp.Message)
				} else {
					resp.Status = "ok"
					resp.Message = "Delete catalog successfully!"
					resp.Data = ""
					c.NewLog("Access API", "INFO", "D", resp.Message)
				}
			}
		}
		c.Data["json"] = resp
		c.ServeJSON()
	}
}

//清空回收站
func (c *CatalogController) ClearRecycleBin() {
	resp := new(JsonResult)
	user := c.CheckSession()
	if user.Id > 0 {
		n := user.ClearRecycleBin()
		resp.Status = "ok"
		resp.Message = "Clear recycle bin successfully!"
		resp.Data = n
		c.NewLog("Access API", "INFO", "D", resp.Message)
		c.Data["json"] = resp
		c.ServeJSON()
	}
}

//恢复文件或者文件夹
func (c *CatalogController) Recovery() {
	resp := new(JsonResult)
	user := c.CheckSession()
	if user.Id > 0 {
		//参数
		type paramslist struct {
			Id int64 `form:"id"` //文件或者文件夹节点ID
		}
		params := new(paramslist)
		if err := c.ParseForm(params); err != nil { //获取参数错误
			resp.Status = "fail-a"
			resp.Message = fmt.Sprintf("Parameter input error:%s", err.Error())
			c.NewLog("Access API", "WARN", "U", resp.Message)
		} else { //获取参数正确
			uc := new(models.UserCatalog)
			uc.Id = params.Id
			if err := uc.Read(); err != nil { //读取目录错误
				resp.Status = "fail-b"
				resp.Message = "The given ID number does not exist!"
				c.NewLog("Access API", "WARN", "U", resp.Message)
			} else {
				n := uc.Recovery()
				resp.Status = "ok"
				resp.Message = "Recovery catalog successfully!"
				resp.Data = n
				c.NewLog("Access API", "INFO", "U", resp.Message)
			}
		}
		c.Data["json"] = resp
		c.ServeJSON()
	}
}

//获取用户名下的项目
func (c *CatalogController) GetList() {
	resp := new(JsonResult)
	user := c.CheckSession()
	if user.Id > 0 {
		//参数
		type paramslist struct {
			Pid     int64 `form:"id"`      //目录id,0为用户根目录,负数不限目录
			Removed bool  `form:"removed"` //已经逻辑删除的{true:是,false:否}
		}
		params := new(paramslist)
		if err := c.ParseForm(params); err != nil { //获取参数错误
			resp.Status = "fail-a"
			resp.Message = fmt.Sprintf("Parameter input error:%s", err.Error())
			c.NewLog("Access API", "WARN", "R", resp.Message)
		} else { //获取参数正确
			rst, err := user.GetUserCatalogs(params.Removed, params.Pid)
			if err != nil { //读取目录错误
				resp.Status = "fail-b"
				resp.Message = fmt.Sprintf("Get user catalogs fault:%s", err.Error())
				c.NewLog("Access API", "WARN", "R", resp.Message)
			} else {
				resp.Status = "ok"
				resp.Message = fmt.Sprint("Catalogs number:", len(rst))
				resp.Data = rst
				c.NewLog("Access API", "INFO", "R", resp.Message)
			}
		}
		c.Data["json"] = resp
		c.ServeJSON()
	}
}

//获取用户名下的项目的统计信息
func (c *CatalogController) GetStat() {
	resp := new(JsonResult)
	user := c.CheckSession()
	if user.Id > 0 {
		//参数
		type paramslist struct {
			Pid int64 `form:"id"` //目录id,0为用户根目录,负数不限目录
		}
		params := new(paramslist)
		if err := c.ParseForm(params); err != nil { //获取参数错误
			resp.Status = "fail-a"
			resp.Message = fmt.Sprintf("Parameter input error:%s", err.Error())
			c.NewLog("Access API", "WARN", "R", resp.Message)
		} else { //获取参数正确
			rst := user.GetCatalogStat(params.Pid)
			resp.Status = "ok"
			resp.Message = "Get user catalogs statistics successfully!"
			resp.Data = rst
			c.NewLog("Access API", "INFO", "R", resp.Message)
		}
		c.Data["json"] = resp
		c.ServeJSON()
	}
}

//获取文件夹的路径
func (c *CatalogController) GetPath() {
	resp := new(JsonResult)
	user := c.CheckSession()
	if user.Id > 0 {
		//参数
		type paramslist struct {
			Id int64 `form:"id"` //目录id,0为用户根目录,负数不限目录
		}
		params := new(paramslist)
		if err := c.ParseForm(params); err != nil { //获取参数错误
			resp.Status = "fail-a"
			resp.Message = fmt.Sprintf("Parameter input error:%s", err.Error())
			c.NewLog("Access API", "WARN", "R", resp.Message)
		} else { //获取参数正确
			if params.Id == 0 {
				resp.Status = "ok"
				resp.Message = "Get folder path successfully!"
				resp.Data = map[int64]string{0: "mydisk"}
				c.NewLog("Access API", "INFO", "R", resp.Message)
			} else {
				uc := new(models.UserCatalog)
				uc.Id = params.Id
				if err := uc.Read(); err != nil { //读取目录错误
					resp.Status = "fail-b"
					resp.Message = "The given folder ID number does not exist!"
					c.NewLog("Access API", "WARN", "R", resp.Message)
				} else {
					res := uc.GetPath()
					resp.Status = "ok"
					resp.Message = "Get folder path successfully!"
					resp.Data = res
					c.NewLog("Access API", "INFO", "R", resp.Message)
				}
			}
		}
		c.Data["json"] = resp
		c.ServeJSON()
	}
}

//重命名
func (c *CatalogController) ReName() {
	resp := new(JsonResult)
	user := c.CheckSession()
	if user.Id > 0 {
		//参数
		type paramslist struct {
			Id      int64  `form:"id"`      //目录id
			NewName string `form:"newname"` //新名称
		}
		params := new(paramslist)
		if err := c.ParseForm(params); err != nil { //获取参数错误
			resp.Status = "fail-a"
			resp.Message = fmt.Sprintf("Parameter input error:%s", err.Error())
			c.NewLog("Access API", "WARN", "U", resp.Message)
		} else { //获取参数正确
			uc := new(models.UserCatalog)
			uc.Id = params.Id
			if err := uc.Read(); err != nil { //错误
				resp.Status = "fail-b"
				resp.Message = "The given folder ID number does not exist!"
				c.NewLog("Access API", "WARN", "U", resp.Message)
			} else {
				if uc.Name != params.NewName {
					if !uc.IsDir {
						_, oldextname := uc.DetachExtension() //获取原始后缀名
						uc.Name = params.NewName
						_, exnm := uc.DetachExtension() //检查新的后缀名
						if len(exnm) == 0 {             //如果没有新后缀名
							uc.Name = fmt.Sprintf("%s.%s", params.NewName, oldextname) //使用原始后缀名
						}
					} else {
						uc.Name = params.NewName
					}
					if err := uc.Update("Name"); err != nil { //错误
						resp.Status = "fail-c"
						resp.Message = "Rename item faild!"
						c.NewLog("Access API", "WARN", "U", resp.Message)
					} else {
						resp.Status = "ok"
						resp.Message = "Rename item successfully!"
						resp.Data = uc
						c.NewLog("Access API", "INFO", "U", resp.Message)
					}
				} else {
					resp.Status = "ok"
					resp.Message = ""
					resp.Data = uc
				}
			}
		}
		c.Data["json"] = resp
		c.ServeJSON()
	}
}

//更新备注
func (c *CatalogController) UpdateDesc() {
	resp := new(JsonResult)
	user := c.CheckSession()
	if user.Id > 0 {
		//参数
		type paramslist struct {
			Id          int64  `form:"id"`   //目录id
			Description string `form:"desc"` //新备注
		}
		params := new(paramslist)
		if err := c.ParseForm(params); err != nil { //获取参数错误
			resp.Status = "fail-a"
			resp.Message = fmt.Sprintf("Parameter input error:%s", err.Error())
			c.NewLog("Access API", "WARN", "U", resp.Message)
		} else { //获取参数正确
			uc := new(models.UserCatalog)
			uc.Id = params.Id
			if err := uc.Read(); err != nil { //错误
				resp.Status = "fail-b"
				resp.Message = "The given folder ID number does not exist!"
				c.NewLog("Access API", "WARN", "U", resp.Message)
			} else {
				if uc.Description != params.Description {
					uc.Description = params.Description
					if err := uc.Update("Description"); err != nil { //错误
						resp.Status = "fail-c"
						resp.Message = "Update item's description faild!"
						c.NewLog("Access API", "WARN", "U", resp.Message)
					} else {
						resp.Status = "ok"
						resp.Message = "Update item's description successfully!"
						resp.Data = uc
						c.NewLog("Access API", "INFO", "U", resp.Message)
					}
				} else {
					resp.Status = "ok"
					resp.Message = ""
					resp.Data = uc
				}
			}
		}
		c.Data["json"] = resp
		c.ServeJSON()
	}
}

//在文件夹下搜索
func (c *CatalogController) Search() {
	resp := new(JsonResult)
	user := c.CheckSession()
	if user.Id > 0 {
		//参数
		type paramslist struct {
			Id       int64  `form:"id"`       //目录id
			NameLike string `form:"namelike"` //名称关键词
		}
		params := new(paramslist)
		if err := c.ParseForm(params); err != nil { //获取参数错误
			resp.Status = "fail-a"
			resp.Message = fmt.Sprintf("Parameter input error:%s", err.Error())
			c.NewLog("Access API", "WARN", "R", resp.Message)
		} else { //获取参数正确
			if rst, err := user.Search(params.Id, params.NameLike); err != nil { //错误
				resp.Status = "fail-b"
				resp.Message = "Search items faild!"
				c.NewLog("Access API", "WARN", "R", resp.Message)
			} else {
				resp.Status = "ok"
				resp.Message = fmt.Sprint(len(rst))
				resp.Data = rst
				c.NewLog("Access API", "INFO", "R", resp.Message)
			}
		}
		c.Data["json"] = resp
		c.ServeJSON()
	}
}

//获取下载日志
func (c *CatalogController) GetDownloadLog() {
	resp := new(JsonResult)
	resp.Status = "ok"
	user := c.CheckSession()
	if user.Id > 0 {
		//参数
		type paramslist struct {
			Id int64 `form:"id"` //文件id
		}
		params := new(paramslist)
		if err := c.ParseForm(params); err != nil { //获取参数错误
			resp.Status = "fail-a"
			resp.Message = fmt.Sprintf("Parameter input error:%s", err.Error())
			c.NewLog("Access API", "WARN", "R", resp.Message)
		} else { //获取参数正确
			uc := new(models.UserCatalog)
			uc.Id = params.Id
			uc.LoadRelated("DownloadLogs")
			resp.Message = "Get file downloadloas successfully!"
			c.NewLog("Access API", "INFO", "R", resp.Message)
			if len(uc.DownloadLogs) == 0 {
				resp.Message = "0"
			} else {
				for i, log := range uc.DownloadLogs {
					log.User.Read()
					log.User.MaxSpace = 0
					log.User.Name = log.User.Username
					log.User.CreateTime = time.Now()
					uc.DownloadLogs[i] = log
				}
			}
			resp.Data = uc.DownloadLogs
		}
	} else { //没有当前登录用户
		resp.Status = "fail-0"
		resp.Message = "The user is not logged in!"
	}
	c.Data["json"] = resp
	c.ServeJSON()
}

//获取文件或者文件夹列表
func (c *CatalogController) GetCatelogs() {
	resp := new(JsonResult)
	resp.Status = "ok"
	user := c.CheckSession()
	if user.Id > 0 {
		//参数
		type paramslist struct {
			IsAll bool `form:"isall"` //true:获取全部文件和文件夹,false:只获取文件夹
		}
		params := new(paramslist)
		if err := c.ParseForm(params); err != nil { //获取参数错误
			resp.Status = "fail-a"
			resp.Message = fmt.Sprintf("Parameter input error:%s", err.Error())
			c.NewLog("Access API", "WARN", "R", resp.Message)
		} else { //获取参数正确
			uc := new(models.UserCatalog)
			ctlgs, err := uc.GetCatalogs(user.Id, params.IsAll)
			if err != nil {
				resp.Status = "fail-b"
				resp.Message = fmt.Sprintf("Get catalogs fail:%s", err.Error())
				c.NewLog("Access API", "WARN", "R", resp.Message)
			} else {
				resp.Message = "Get catalogs successfully!"
				resp.Data = ctlgs
				c.NewLog("Access API", "INFO", "R", resp.Message)
			}
		}
		c.Data["json"] = resp
		c.ServeJSON()
	}
}

//项目移动到目标文件夹
func (c *CatalogController) MoveTo() {
	resp := new(JsonResult)
	resp.Status = "ok"
	user := c.CheckSession()
	if user.Id > 0 {
		//参数
		type paramslist struct {
			Catalogs string `form:"catalogs"` //需要移动的项目id数组
			DirId    int64  `form:"dirid"`    //目标文件夹的id
		}
		params := new(paramslist)
		if err := c.ParseForm(params); err != nil { //获取参数错误
			resp.Status = "fail-a"
			resp.Message = fmt.Sprintf("Parameter input error:%s", err.Error())
			c.NewLog("Access API", "WARN", "R", resp.Message)
		} else { //获取参数正确
			catalogs := strings.Split(params.Catalogs, ",")
			for _, item := range catalogs {
				id, err := strconv.ParseInt(item, 10, 64)
				if err == nil {
					uc := new(models.UserCatalog)
					uc.Id = id
					uc.Pid = params.DirId
					if err := uc.Update("Pid"); err != nil {
						c.NewLog("Access API", "WARN", "U", fmt.Sprintf("Update pid fail:%s", err.Error()))
					}
				}
			}
		}
		c.Data["json"] = resp
		c.ServeJSON()
	}
}

//项目复制到目标文件夹
func (c *CatalogController) CopyTo() {
	resp := new(JsonResult)
	resp.Status = "ok"
	user := c.CheckSession()
	if user.Id > 0 {
		//参数
		type paramslist struct {
			Catalogs string `form:"catalogs"` //需要复制的项目id数组
			DirId    int64  `form:"dirid"`    //目标文件夹的id
		}
		params := new(paramslist)
		if err := c.ParseForm(params); err != nil { //获取参数错误
			resp.Status = "fail-a"
			resp.Message = fmt.Sprintf("Parameter input error:%s", err.Error())
			c.NewLog("Access API", "WARN", "R", resp.Message)
		} else { //获取参数正确
			catalogs := strings.Split(params.Catalogs, ",")
			var cnt int64
			for _, item := range catalogs {
				id, err := strconv.ParseInt(item, 10, 64)
				if err == nil {
					uc := new(models.UserCatalog)
					uc.Id = id
					if err := uc.Read(); err == nil {
						cnt += uc.CopyTo(params.DirId)
					} else {
						c.NewLog("Access API", "WARN", "U", fmt.Sprintf("Copyto fail:%s", err.Error()))
					}
				}
			}
			resp.Data = cnt
			c.NewLog("Access API", "INFO", "U", fmt.Sprintf("Copyto successfully,copyed %d items!", cnt))
		}
		c.Data["json"] = resp
		c.ServeJSON()
	}
}

//重载用户名下的文件名
func (c *CatalogController) ReloadCatalogFileName() {
	resp := new(JsonResult)
	resp.Status = "ok"
	user := c.CheckSession()
	if user.Id > 0 {
		ctlg := new(models.UserCatalog)
		cnt := ctlg.ReloadCatalogFileName(user.Id)
		resp.Data = cnt
		c.Data["json"] = resp
		c.ServeJSON()
	}
}
