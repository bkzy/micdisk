package controllers

import (
	"fmt"
	"micdisk/models"
)

type DownloadlogController struct {
	MainController
}

//新建文件夹
func (c *DownloadlogController) NewDownloadLog() {
	resp := new(JsonResult)
	user := c.CheckSession()
	if user.Id > 0 {
		//修改密码用户信息
		type paramslist struct {
			CatalogId int64 `form:"fileid"`   //被下载的文件节点
			Download  int64 `form:"download"` //下载模式(1) 还是 预览模式(0)
		}
		params := new(paramslist)
		if err := c.ParseForm(params); err != nil { //获取参数错误
			resp.Status = "fail-a"
			resp.Message = fmt.Sprintf("Parameter input error:%s", err.Error())
			c.NewLog("Access API", "WARN", "C", resp.Message)
		} else {
			puc := new(models.UserCatalog)
			puc.Id = params.CatalogId
			if err := puc.Read(); err != nil { //文件存在
				resp.Status = "fail-b"
				resp.Message = fmt.Sprintf("File ID error:%s!", err.Error())
				c.NewLog("Access API", "WARN", "C", resp.Message)
			} else { //文件不存在
				log := new(models.DownloadLog)
				log.User = user
				log.Catalog = puc
				log.Download = params.Download
				if err := log.Create(); err != nil {
					resp.Status = "fail-c"
					resp.Message = fmt.Sprintf("Create downloadlog fault:%s!", err.Error())
					c.NewLog("Access API", "WARN", "C", resp.Message)
				} else {
					resp.Status = "ok"
					resp.Message = "Download log created successfully!"
					resp.Data = log
					c.NewLog("Access API", "INFO", "C", resp.Message)
				}
			}
		}
		c.Data["json"] = resp
		c.ServeJSON()
	}
}
