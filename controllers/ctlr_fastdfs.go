package controllers

import (
	"micdisk/models"
)

type FastDfs struct {
	MainController
}

//获取文件夹下的文件和文件夹列表
func (c *FastDfs) GetListDir() {
	c.NewLog("Access API", "INFO", "R")
	sesion := c.CheckSession()
	if sesion.Id > 0 {
		dir := c.GetString("dir", "") //不填写的时候为根文件目录
		var dirs []string
		if len(dir) > 0 {
			dirs = append(dirs, dir)
		}
		rawresp, _, _ := models.DFS.GetDirList(dirs...)
		c.Data["json"] = rawresp
		c.ServeJSON()
	}
}

//获取文件信息
//path与md5二选一
func (c *FastDfs) GetFileInfo() {
	c.NewLog("Access API", "INFO", "R")
	sesion := c.CheckSession()
	if sesion.Id > 0 {
		path := c.GetString("path", "")
		md5 := c.GetString("md5", "")

		rawresp, _, _ := models.DFS.GetFileInfo(path, md5)
		c.Data["json"] = rawresp
		c.ServeJSON()
	}
}

//在服务器上删除文件
//path与md5二选一
func (c *FastDfs) DeleteFile() {
	c.NewLog("Access API", "INFO", "D")
	sesion := c.CheckSession()
	if sesion.Id > 0 {
		path := c.GetString("path", "")
		md5 := c.GetString("md5", "")

		rawresp, _, _ := models.DFS.DeleteFile(path, md5)
		c.Data["json"] = rawresp
		c.ServeJSON()
	}
}

//获取文件统计信息
func (c *FastDfs) GetStat() {
	c.NewLog("Access API", "INFO", "R")
	sesion := c.CheckSession()
	if sesion.Id > 0 {
		rawresp, _, _ := models.DFS.GetStat()
		c.Data["json"] = rawresp
		c.ServeJSON()
	}
}

//获取配置全局信息
func (c *FastDfs) GetGloablConfig() {
	c.NewLog("Access API", "INFO", "R")
	sesion := c.CheckSession()
	if sesion.Id > 0 {
		rawresp, _, _ := models.DFS.GetGloablConfig()
		c.Data["json"] = rawresp
		c.ServeJSON()
	}
}
