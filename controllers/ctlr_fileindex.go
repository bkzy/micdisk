package controllers

import (
	"fmt"
	"math"
	"micdisk/models"
	"time"
)

type FileIndexController struct {
	MainController
}

//新建
//Token=md5(scene,modtime)
func (c *FileIndexController) NewFileIndex() {
	resp := new(JsonResult)
	user := c.CheckSession()
	if user.Id > 0 {
		type fileresult struct {
			Url     string `form:"url"`    //存储全路径,Domain+Src
			Md5     string `form:"md5"`    //文件的MD5摘要
			Path    string `form:"path"`   //存储路径
			Domain  string `form:"domain"` //域名
			Scene   string `form:"scene"`  //场景
			Size    int64  `form:"size"`   //文件大小,字节
			ModTime int64  `form:"mtime"`  //创建时间,UTC秒
			Token   string `form:"token"`  //token
		}
		fmsg := new(fileresult)
		if err := c.ParseForm(fmsg); err != nil { //获取参数错误
			resp.Status = "fail-a"
			resp.Message = fmt.Sprintf("Parameter input error:%s", err.Error())
			c.NewLog("NewFileIndex", "WARN", "C", resp.Message)
		} else {
			fidx := new(models.DfsFileIndex)
			fidx.Domain = fmsg.Domain
			fidx.Md5 = fmsg.Md5
			fidx.ModTime = fmsg.ModTime
			fidx.Path = fmsg.Path
			fidx.Scene = fmsg.Scene
			fidx.Size = fmsg.Size
			fidx.Token = fmsg.Token
			fidx.Url = fmsg.Url
			if create, _, err := fidx.ReadOrCreate(); err != nil {
				resp.Status = "fail-b"
				resp.Message = fmt.Sprintf("Create file index fail:%s", err.Error())
				c.NewLog("NewFileIndex", "WARN", "C", resp.Message)
			} else {
				resp.Status = "ok"
				resp.Data = fidx
				if create {
					resp.Message = "Create file index successfully!"
				} else {
					resp.Message = "File flash trans!"
				}
				c.NewLog("NewFileIndex", "INFO", "C", resp.Message)
			}
		}
		c.Data["json"] = resp
		c.ServeJSON()
	}
}

//检查token
//上传时,token=md5(scene,tstemp,accesskey)
//下载时,token=md5(f.name,tstemp,download,f.token)
func (c *FileIndexController) CheckToken() {
	resp := "fail"
	action := "Check upload token "
	type auth_token struct {
		Token    string `form:"auth_token"` //token
		Scene    string `form:"scene"`      //场景,上传时的参数
		Tstemp   int64  `form:"tstemp"`     //时间戳
		Name     string `form:"name"`       //下载时的参数
		Id       int64  `form:"id"`         //下载参数
		Md5      string `form:"md5"`        //下载参数
		Download int64  `form:"download"`
		Path     string `form:"__path__"`
	}
	tk := new(auth_token)
	if err := c.ParseForm(tk); err == nil { //获取参数错误
		if tk.Path == "/favicon.ico" {
			action = "Check download token "
			resp = "ok"
		} else {
			tstemp := time.Now().Unix()
			if math.Abs(float64(tstemp-tk.Tstemp/1000)) < 120 { //时间差小于120s
				if len(tk.Scene) > 0 { //上传校验
					u := new(models.SysUser)
					u.Name = tk.Scene
					key, err := u.AccessKey("Name")
					if err == nil {
						md5 := models.Md5str(tk.Scene, fmt.Sprint(tk.Tstemp), key)
						if md5 == tk.Token { //上传md5校验
							resp = "ok"
						}
					}
				} else { //下载校验
					action = "Check download token "
					fid := new(models.DfsFileIndex)
					if tk.Id > 0 { //如果设定了目录的id
						uc := new(models.UserCatalog)
						uc.Id = tk.Id
						if err := uc.Read(); err == nil {
							fid = uc.File
						}
					}
					if len(fid.Token) < 32 { //如果通过id没有读取到文件索引信息
						fid.Md5 = tk.Md5
						fid.Read("Md5") //通过Md5读取文件索引信息
					}
					if fid != nil { //如果读取到了文件索引信息
						md5 := models.Md5str(
							tk.Name,
							fmt.Sprint(tk.Tstemp),
							fmt.Sprint(tk.Download),
							fid.Token)
						fmt.Println(tk.Name, tk.Tstemp, tk.Download, fid.Token)
						if md5 == tk.Token {
							resp = "ok"
						} else {
							c.NewLog("CheckToken", "WARN", "O", "下载Token不匹配", md5, tk.Token)
						}
					} else {
						c.NewLog("CheckToken", "WARN", "O", "没有读取到数据", err.Error())
					}
				}
			} else {
				c.NewLog("CheckToken", "WARN", "O", "时间错误", fmt.Sprintf("当前时间戳:%d,请求时间戳:%d", tstemp, tk.Tstemp))
			}
		}
	}
	c.NewLog("CheckToken", "INFO", "O", action, resp)
	c.Ctx.WriteString(resp)
}

//Token=md5(scene,modtime)
func (c *FileIndexController) DfsWebHook() {
	resp := new(JsonResult)
	type fileresult struct {
		Url      string `form:"url"`      //存储全路径,Domain+Src
		Md5      string `form:"md5"`      //文件的MD5摘要
		Path     string `form:"path"`     //存储路径
		Domain   string `form:"domain"`   //域名
		Scene    string `form:"scene"`    //场景
		RawScene string `form:"rscene"`   //原始输入场景
		SubScene string `form:"subscene"` //子场景,可以用/划分多个
		Size     int64  `form:"size"`     //文件大小,字节
		ModTime  int64  `form:"modtime"`  //创建时间,UTC秒
		Replace  int64  `form:"replace"`  //覆盖同目录下的同名文件
		FileName string `form:"filename"` //文件原始名
		Token    string `form:"--"`       //token
	}
	fmsg := new(fileresult)
	if err := c.ParseForm(fmsg); err != nil { //获取参数错误
		resp.Status = "fail-a"
		resp.Message = fmt.Sprintf("Parameter input error:%s", err.Error())
		c.NewLog("DfsWebHook", "WARN", "C", resp.Message)
	} else {
		fidx := new(models.DfsFileIndex)
		fidx.Domain = fmsg.Domain
		fidx.Md5 = fmsg.Md5
		fidx.ModTime = fmsg.ModTime
		fidx.Path = fmsg.Path
		fidx.Scene = fmsg.Scene
		fidx.Size = fmsg.Size
		fidx.Token = models.Md5str(fmsg.Scene, fmt.Sprint(fmsg.ModTime))
		fidx.Url = fmsg.Url
		if create, _, err := fidx.ReadOrCreate(); err != nil {
			resp.Status = "fail-b"
			resp.Message = fmt.Sprintf("Create file index fail:%s", err.Error())
			c.NewLog("DfsWebHook", "WARN", "C", resp.Message)
		} else {
			resp.Status = "ok"
			if create {
				resp.Message = "Create file index successfully!"
			} else {
				resp.Message = "File flash trans!"
			}
			c.NewLog("DfsWebHook", "INFO", "C", resp.Message)
			uc := new(models.UserCatalog)
			uc.WebHook(fmsg.RawScene, fmsg.SubScene, fmsg.FileName, !create, fmsg.Replace > 0, fidx)
		}
	}
	c.Ctx.WriteString(resp.Status)
}
