package controllers

import (
	"fmt"
	"micdisk/models"
	"strings"
)

func (c *MainController) Get() {
	user := c.CheckSession()
	if user.Id > 0 {
		http := ""
		if !strings.Contains(models.DISKCFG.DfsDomain, "http") {
			http = "http://"
		}
		c.Data["username"] = user.Username
		upitfc := fmt.Sprintf("%s%s", http, models.DISKCFG.DfsDomain)
		if len(models.DISKCFG.DfsGroup) > 0 {
			upitfc = fmt.Sprintf("%s%s/%s", http, models.DISKCFG.DfsDomain, models.DISKCFG.DfsGroup)
		}
		c.Data["DfsDomain"] = upitfc
		c.Data["Scene"] = user.Name
		c.Data["Version"] = models.DISKCFG.Version
		c.TplName = "index.tpl"
	}
}

func (c *MainController) GetDfsDomain() {
	resp := new(JsonResult)
	http := ""
	if !strings.Contains(models.DISKCFG.DfsDomain, "http") {
		if models.DISKCFG.EnableHttps {
			http = "https://"
		} else {
			http = "http://"
		}
	}
	upitfc := fmt.Sprintf("%s%s", http, models.DISKCFG.DfsDomain)
	//if len(models.DISKCFG.DfsGroup) > 0 {
	//	upitfc = fmt.Sprintf("%s%s/%s", http, models.DISKCFG.DfsDomain, models.DISKCFG.DfsGroup)
	//}
	resp.Status = "ok"
	resp.Data = upitfc                     //'http://ip:port'
	resp.Message = models.DISKCFG.DfsGroup //组名
	c.Data["json"] = resp
	c.ServeJSON()
}
