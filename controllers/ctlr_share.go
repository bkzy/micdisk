package controllers

import (
	"fmt"
	"micdisk/models"
	"strconv"
	"strings"
	"time"
)

type ShareController struct {
	MainController
}

//新建共享节点
func (c *ShareController) NewShare() {
	resp := new(JsonResult)
	user := c.CheckSession()
	if user.Id > 0 {
		//参数信息
		type paramslist struct {
			Id          int64  `form:"id"`       //需要被分享的节点
			Validity    int64  `form:"validity"` //有效期(秒),0为长期有效
			CanDownload bool   `form:"candown"`  //允许下载
			CanUpload   bool   `form:"canup"`    //允许上传
			UserIds     string `form:"userids"`  //被共享的用户ID
			GroupIds    string `form:"groupids"` //被共享的用户组ID
		}
		params := new(paramslist)
		if err := c.ParseForm(params); err != nil { //获取参数错误
			resp.Status = "fail-a"
			resp.Message = fmt.Sprintf("Parameter input error:%s", err.Error())
			c.NewLog("Access API", "WARN", "C", resp.Message)
		} else {
			puc := new(models.UserCatalog)
			puc.Id = params.Id
			if err := puc.Read(); err != nil || puc.User.Id == 0 { //无数据
				resp.Status = "fail-b"
				resp.Message = fmt.Sprintf("Invalid node ID:%s", err.Error())
				c.NewLog("Access API", "WARN", "C", resp.Message)
			} else {
				s := new(models.ShareList)
				s.Catalog = puc
				s.CanDownload = params.CanDownload
				s.CanUpload = params.CanUpload
				s.TermOfValidity = params.Validity
				if err := s.Create(); err != nil {
					resp.Status = "fail-c"
					resp.Message = fmt.Sprintf("Create share node fault:%s!", err.Error())
					c.NewLog("Access API", "WARN", "C", resp.Message)
				} else {
					idstrs := strings.Split(params.UserIds, ",")
					var users []int64
					for _, idstr := range idstrs {
						if id, e := strconv.ParseInt(idstr, 10, 64); e == nil {
							users = append(users, id)
						}
					}
					gidstrs := strings.Split(params.GroupIds, ",")
					var grps []int64
					for _, idstr := range gidstrs {
						if id, e := strconv.ParseInt(idstr, 10, 64); e == nil {
							grps = append(grps, id)
						}
					}
					s.ShareWithUsers(users...)
					s.ShareWithGroups(grps...)
					s.LoadRelated()
					resp.Status = "ok"
					resp.Message = "The shared node was successfully created!"
					resp.Data = s
					c.NewLog("Access API", "INFO", "C", resp.Message)
				}
			}
		}
		c.Data["json"] = resp
		c.ServeJSON()
	}
}

//更新共享节点
func (c *ShareController) UpdateShare() {
	resp := new(JsonResult)
	user := c.CheckSession()
	if user.Id > 0 {
		//参数信息
		type paramslist struct {
			Id          int64  `form:"id"`       //共享节点ID
			Validity    int64  `form:"validity"` //有效期(秒,从当前时间开始算),0为长期有效
			CanDownload bool   `form:"candown"`  //允许下载
			CanUpload   bool   `form:"canup"`    //允许上传
			UserIds     string `form:"userids"`  //被共享的用户ID
			GroupIds    string `form:"groupids"` //被共享的用户组ID
		}
		params := new(paramslist)
		if err := c.ParseForm(params); err != nil { //获取参数错误
			resp.Status = "fail-a"
			resp.Message = fmt.Sprintf("Parameter input error:%s", err.Error())
			c.NewLog("Access API", "WARN", "U", resp.Message)
		} else {
			s := new(models.ShareList)
			s.Id = params.Id
			if err := s.Read(true); err != nil { //验证节点
				resp.Status = "fail-b"
				resp.Message = fmt.Sprintf("Invalid node ID:%s", err.Error())
				c.NewLog("Access API", "WARN", "U", resp.Message)
			} else {
				idstrs := strings.Split(params.UserIds, ",")
				var users []int64
				uidmp := make(map[int64]int64)
				for _, idstr := range idstrs {
					if id, e := strconv.ParseInt(idstr, 10, 64); e == nil {
						users = append(users, id)
						uidmp[id] = id
					}
				}
				var rmus []int64 //需要移除的用户
				for _, u := range s.Users {
					if _, ok := uidmp[u.Id]; !ok {
						rmus = append(rmus, u.Id)
					}
				}
				gidstrs := strings.Split(params.GroupIds, ",")
				var grps []int64
				gidmp := make(map[int64]int64) //需要移除的组
				for _, idstr := range gidstrs {
					if id, e := strconv.ParseInt(idstr, 10, 64); e == nil {
						grps = append(grps, id)
						gidmp[id] = id
					}
				}
				var rmgs []int64 //需要移除的用户
				for _, g := range s.Groups {
					if _, ok := gidmp[g.Id]; !ok {
						rmgs = append(rmgs, g.Id)
					}
				}
				if len(rmus) > 0 {
					s.RemoveUsers(rmus...)
				}
				if len(rmgs) > 0 {
					s.RemoveGroups(rmgs...)
				}
				if len(users) > 0 {
					s.ShareWithUsers(users...)
				}
				if len(grps) > 0 {
					s.ShareWithGroups(grps...)
				}

				var cols []string
				updt := false
				if s.CanDownload != params.CanDownload {
					s.CanDownload = params.CanDownload
					cols = append(cols, "CanDownload")
					updt = true
				}
				if s.CanUpload != params.CanUpload {
					s.CanUpload = params.CanUpload
					cols = append(cols, "CanUpload")
					updt = true
				}
				if s.TermOfValidity != params.Validity {
					offset := time.Now().Unix() - s.CreateTime.Unix()
					if params.Validity == 0 {
						offset = 0
					}
					s.TermOfValidity = params.Validity + offset
					cols = append(cols, "TermOfValidity")
					updt = true
				}
				if updt {
					if err := s.Update(cols...); err != nil {
						resp.Status = "fail-c"
						resp.Message = fmt.Sprintf("Update share node fualt!:%s", err.Error())
						c.NewLog("Access API", "WARN", "U", resp.Message)
					} else {
						s.Read(true)
						resp.Status = "ok"
						resp.Message = "" //fmt.Sprintf("The shared node was successfully updated!")
						resp.Data = s
						c.NewLog("Access API", "INFO", "U", resp.Message)
					}
				} else {
					s.Read(true)
					resp.Status = "ok"
					resp.Message = "" //fmt.Sprintf("No shared node was updated!")
					resp.Data = s
					c.NewLog("Access API", "INFO", "U", resp.Message)
				}
			}
		}
		c.Data["json"] = resp
		c.ServeJSON()
	}
}

//取消共享节点
func (c *ShareController) RemoveShare() {
	resp := new(JsonResult)
	user := c.CheckSession()
	if user.Id > 0 {
		//参数信息
		type paramslist struct {
			Id int64 `form:"id"` //需要被取消的共享节点
		}
		params := new(paramslist)
		if err := c.ParseForm(params); err != nil { //获取参数错误
			resp.Status = "fail-a"
			resp.Message = fmt.Sprintf("Parameter input error:%s", err.Error())
			c.NewLog("Access API", "WARN", "D", resp.Message)
		} else {
			s := new(models.ShareList)
			s.Id = params.Id
			if err := s.Delete(); err != nil { //删除
				resp.Status = "fail-b"
				resp.Message = fmt.Sprintf("Invalid node ID:%s", err.Error())
				c.NewLog("Access API", "WARN", "D", resp.Message)
			} else {
				resp.Status = "ok"
				resp.Message = "The shared node was successfully deleted!"
				resp.Data = ""
				c.NewLog("Access API", "INFO", "D", resp.Message)
			}
		}
		c.Data["json"] = resp
		c.ServeJSON()
	}
}

//查询我共享的节点
func (c *ShareController) GetMySharedLists() {
	resp := new(JsonResult)
	user := c.CheckSession()
	if user.Id > 0 {
		if rst, err := user.GetMySharedLists(); err != nil { //
			resp.Status = "fail-a"
			resp.Message = fmt.Sprintf("An error occurred while querying:%s", err.Error())
			c.NewLog("Access API", "WARN", "R", resp.Message)
		} else {
			resp.Status = "ok"
			resp.Message = "Get my shared list successfully!"
			resp.Data = rst
			c.NewLog("Access API", "INFO", "R", resp.Message)
		}
		c.Data["json"] = resp
		c.ServeJSON()
	}
}

//查询共享给我的节点
func (c *ShareController) GetSharedToMeLists() {
	resp := new(JsonResult)
	user := c.CheckSession()
	if user.Id > 0 {
		rst := user.GetSharedToMeLists()
		resp.Status = "ok"
		resp.Message = "Successfully obtained the node shared with me!"
		resp.Data = rst
		c.NewLog("Access API", "INFO", "R", resp.Message)

		c.Data["json"] = resp
		c.ServeJSON()
	}
}
