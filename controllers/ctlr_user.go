package controllers

import (
	"fmt"
	"micdisk/models"
)

type UserController struct {
	MainController
}

func (c *UserController) Get() {
	c.NewLog("Login", "INFO", "LI", "Visit the login page")
	c.TplName = "login.tpl"
}

func (c *UserController) PageRegister() {
	c.NewLog("Login", "INFO", "LI", "Visit the register page")
	c.TplName = "register.tpl"
}

//用户登录逻辑
func (c *UserController) Post() {
	resp := new(JsonResult)
	resp.Status = "ok"
	//用户登录Form信息
	type userLoginInfo struct {
		Name      string `form:"name"`       //用户名
		Pswd      string `form:"pswd"`       //密码
		PageLogin bool   `form:"page_login"` //从页面登录
	}
	var uif userLoginInfo
	if err := c.ParseForm(&uif); err != nil { //传入user指针
		resp.Status = "fail"
		resp.Message = "Get login paramaters fault!"
		c.NewLog("Login failed", "EROR", "LI", "Get login paramaters fault!", err.Error()) //记录用动作作信息
		c.Data["json"] = resp
	} else {
		user := new(models.SysUser)
		user.Name = uif.Name
		user.Password = uif.Pswd

		if ok := user.Login(); !ok { //读取数据库
			resp.Status = "fail"
			resp.Message = "User name or password error, login fault!"
			c.NewLog("Login failed", "WARN", "LI", "User name or password error, login failed!") //记录用动作作信息
			c.Data["json"] = resp
		} else {
			if len(user.Username) == 0 {
				user.Username = user.Name
			}
			c.SetSession("UId", user.Id)
			c.SetSession("Name", user.Name)
			c.SetSession("UserName", user.Username)
			c.SetSession("UserMsg", user)
			c.SetSession("SessionTag", models.Md5str(fmt.Sprintf("%d%s", user.Id, user.Name)))

			resp.Status = "ok"
			resp.Message = "Login successfully!"
			c.Data["json"] = resp
			c.NewLog("User login", "INFO", "LI", resp.Message) //记录用动作作信息
			if uif.PageLogin {
				url := fmt.Sprintf("/%s", _firstPage)
				c.Redirect(url, 302) //跳转到主页
			}
		}
	}
	c.ServeJSON()
}

func (c *UserController) Register() {
	resp := new(JsonResult)
	resp.Status = "ok"
	//用户登录Form信息
	type userLoginInfo struct {
		Name     string `form:"name"`     //用户账号
		UserName string `form:"username"` //用户名
		Pswd     string `form:"pswd"`     //密码
	}
	var uif userLoginInfo
	if err := c.ParseForm(&uif); err != nil { //传入user指针
		resp.Status = "fail"
		resp.Message = "Get register paramaters fault!"
		c.NewLog("Register failed", "EROR", "O", resp.Message, err.Error()) //记录用动作作信息
		c.Data["json"] = resp
	} else {
		user := new(models.SysUser)
		user.Name = uif.Name
		user.Username = uif.UserName
		user.Password = uif.Pswd

		if ok, _, err := user.Register(); !ok || err != nil { //读取数据库
			resp.Status = "fail"
			resp.Message = "The user name already exists!"
			c.NewLog("Register failed", "WARN", "O", resp.Message, err.Error()) //记录用动作作信息
			c.Data["json"] = resp
		} else {
			resp.Status = "ok"
			resp.Message = "Register successfully!"
			c.Data["json"] = resp
			c.NewLog("Register", "INFO", "C", resp.Message) //记录用动作作信息
		}
	}
	c.ServeJSON()
}

//用户修改密码逻辑
func (c *UserController) UpdatePswd() {
	resp := new(JsonResult)
	resp.Status = "ok"
	//修改密码用户信息
	type updatepswdInfo struct {
		OldPswd  string `form:"oldpswd"`  //旧密码
		NewPswd  string `form:"newpswd"`  //新密码
		NewPswd2 string `form:"newpswd2"` //新密码2
	}
	var userpswd updatepswdInfo
	u := c.CheckSession()
	if u.Id > 0 { //当前登录用户名不为空
		if err := c.ParseForm(&userpswd); err != nil { //传入user指针
			resp.Status = "fail-1"
			resp.Message = "Get input message failed!"
			c.NewLog("Update user password", "WARN", "U", resp.Message, err.Error()) //记录用动作作信息
		} else {
			if userpswd.NewPswd != userpswd.NewPswd2 { //两次输入的密码不相同
				resp.Status = "fail-2"
				resp.Message = "The new password entered twice is not the same!"
				c.NewLog("Update user password", "WARN", "U", resp.Message) //记录用动作作信息
			} else {
				u.Password = userpswd.OldPswd
				if err := u.Read("Name", "Password"); err != nil { //旧密码错误
					resp.Status = "fail-3"
					resp.Message = "Old password error!"
					c.NewLog("Update user password", "WARN", "U", resp.Message) //记录用动作作信息
				} else {
					u.Password = userpswd.NewPswd
					if ok, err := u.Update("Password"); err != nil || !ok { //更新数据库
						resp.Status = "fail-4"
						resp.Message = "Error updating password!"
						c.NewLog("Update user password", "EROR", "U", resp.Message, err.Error()) //记录用动作作信息
					} else {
						resp.Status = "ok"
						resp.Message = "Password updated successfully!"             //修改密码成功
						c.NewLog("Update user password", "INFO", "U", resp.Message) //记录用动作作信息
					}
				}
			}
		}
		c.Data["json"] = resp
		c.ServeJSON()
	}
}

//用户修改密码逻辑
func (c *UserController) UpdateUsername() {
	resp := new(JsonResult)
	resp.Status = "ok"
	//修改密码用户信息
	type userInfo struct {
		UserName string `form:"username"` //旧密码
	}
	var uname userInfo
	user := c.CheckSession()
	if user.Id > 0 {
		if err := c.ParseForm(&uname); err != nil { //传入user指针
			resp.Status = "fail"
			resp.Message = "Get input message failed!"
			c.NewLog("Update user password", "WARN", "U", resp.Message, err.Error()) //记录用动作作信息
		} else {
			if user.Username != uname.UserName {
				user.Username = uname.UserName
				if ok, err := user.Update("Username"); err != nil || !ok { //更新数据库
					resp.Status = "fail"
					resp.Message = "Error updating username!"
					c.NewLog("Update user name", "EROR", "U", resp.Message, err.Error()) //记录用动作作信息
				} else {
					resp.Status = "ok"
					resp.Message = "Username updated successfully!"         //修改密码成功
					c.NewLog("Update user name", "INFO", "U", resp.Message) //记录用动作作信息
				}
			}
		}
		c.Data["json"] = resp
		c.ServeJSON()
	}
}

func (c *UserController) ApiLogOut() { //用户退出
	c.NewLog("User logout", "INFO", "LO") //记录用动作作信息
	c.DelSession("SessionTag")            //删除Session
	c.DestroySession()                    //
	c.Redirect("/login", 302)
}

//检查用户名是否存在
func (c *UserController) CheckUserName() {
	resp := new(JsonResult)
	resp.Status = "ok"
	//用户登录Form信息
	type paramslist struct {
		Name string `form:"name"` //用户名
	}
	params := new(paramslist)
	if err := c.ParseForm(params); err != nil { //获取参数错误
		resp.Status = "fail"
		resp.Message = fmt.Sprintf("Parameter input error:%s", err.Error())
		c.NewLog("Access API", "WARN", "O", resp.Message)
	} else { //获取参数正确
		user := new(models.SysUser)
		user.Name = params.Name
		if err := user.Read("Name"); err == nil { //用户名已存在
			resp.Status = "fail"
			resp.Message = "The user name already exists!"
			c.NewLog("Access API", "WARN", "O", resp.Message) //记录用动作作信息
		} else {
			resp.Status = "ok"
			resp.Message = "This user name can be registered!"
			c.NewLog("Access API", "INFO", "O", resp.Message) //记录用动作作信息
		}
	}
	c.Data["json"] = resp
	c.ServeJSON()
}

//读取用户列表
func (c *UserController) GetUserList() {
	resp := new(JsonResult)
	user := c.CheckSession()
	if user.Id > 0 {
		//参数信息
		type paramslist struct { //二选一,不选的设置为空字符串
			NameLike     string `form:"namelike"`     //用户账号
			UserNameLike string `form:"usernamelike"` //用户名
		}
		params := new(paramslist)
		if err := c.ParseForm(params); err != nil { //获取参数错误
			resp.Status = "fail-a"
			resp.Message = fmt.Sprintf("Parameter input error:%s", err.Error())
			c.NewLog("Access API", "WARN", "R", resp.Message)
		} else {
			if uss, err := user.GetUserNameLike(params.NameLike, params.UserNameLike); err != nil { //
				resp.Status = "fail-b"
				resp.Message = fmt.Sprintf("An error occurred while querying:%s", err.Error())
				c.NewLog("Access API", "WARN", "C", resp.Message)
			} else {
				resp.Status = "ok"
				resp.Message = ("Get users list successfully!")
				resp.Data = uss
				c.NewLog("Access API", "INFO", "C", resp.Message)
			}
		}
		c.Data["json"] = resp
		c.ServeJSON()
	}
}

//读取当前登录用户的基本信息
func (c *UserController) GetUserMsg() {
	resp := new(JsonResult)
	user := c.CheckSession()
	resp.Status = "ok"
	resp.Data = user
	c.Data["json"] = resp
	c.ServeJSON()
}
