package controllers

import (
	"fmt"
	"micdisk/models"
)

type UserGroupController struct {
	MainController
}

//新建
func (c *UserGroupController) NewUserGroup() {
	resp := new(JsonResult)
	user := c.CheckSession()
	if user.Id > 0 {
		//修改密码用户信息
		type paramslist struct {
			Pid  int64  `form:"pid"`         //上级用户组ID
			Name string `form:"name"`        //用户组名称
			Desc string `form:"description"` //用户组描述说明
		}
		params := new(paramslist)
		if err := c.ParseForm(params); err != nil { //获取参数错误
			resp.Status = "fail-a"
			resp.Message = fmt.Sprintf("Parameter input error:%s", err.Error())
			c.NewLog("Access API", "WARN", "C", resp.Message)
		} else {
			if params.Pid > 0 {
				pgrp := new(models.SysGroup)
				pgrp.Id = params.Pid
				if err := pgrp.Read(); err != nil { //文件不存在
					params.Pid = 0
				}
			}
			grp := new(models.SysGroup)
			grp.Name = params.Name
			grp.Remarkes = params.Desc
			grp.Pid = params.Pid
			if isnew, _, _ := grp.ReadOrCreate(); !isnew {
				resp.Status = "fail-b"
				resp.Message = "The user group name under the same level already exists!"
				c.NewLog("Access API", "WARN", "C", resp.Message)
			} else {
				resp.Status = "ok"
				resp.Message = "User group created successfully!"
				resp.Data = grp
				c.NewLog("Access API", "INFO", "C", resp.Message)
			}
		}
		c.Data["json"] = resp
		c.ServeJSON()
	}
}

//更新
func (c *UserGroupController) UpdateUserGroup() {
	resp := new(JsonResult)
	user := c.CheckSession()
	if user.Id > 0 {
		//修改密码用户信息
		type paramslist struct {
			Id   int64  `form:"id"`          //用户组ID
			Name string `form:"name"`        //用户组名称
			Desc string `form:"description"` //用户组描述说明
		}
		params := new(paramslist)
		if err := c.ParseForm(params); err != nil { //获取参数错误
			resp.Status = "fail-a"
			resp.Message = fmt.Sprintf("Parameter input error:%s", err.Error())
			c.NewLog("Access API", "WARN", "C", resp.Message)
		} else {
			grp := new(models.SysGroup)
			grp.Id = params.Id
			if err := grp.Read(); err != nil {
				resp.Status = "fail-b"
				resp.Message = fmt.Sprintf("Invalid group ID:%s", err.Error())
				c.NewLog("Access API", "WARN", "D", resp.Message)
			} else {
				var cols []string
				updt := false
				if grp.Name != params.Name {
					grp.Name = params.Name
					g := new(models.SysGroup)
					g.Pid = grp.Pid
					g.Name = grp.Name
					if g.Read("Pid", "Name") == nil {
						resp.Status = "fail-b"
						resp.Message = "The user group name under the same level already exists!"
						c.NewLog("Access API", "WARN", "C", resp.Message)
						c.Data["json"] = resp
						c.ServeJSON()
						return
					}
					cols = append(cols, "Name")
					updt = true
				}
				if grp.Remarkes != params.Desc {
					grp.Remarkes = params.Desc
					cols = append(cols, "Remarkes")
					updt = true
				}
				if updt {
					if _, err := grp.Update(cols...); err != nil {
						resp.Status = "fail-c"
						resp.Message = fmt.Sprintf("Update user group fualt!:%s", err.Error())
						c.NewLog("Access API", "WARN", "U", resp.Message)
					} else {
						resp.Status = "ok"
						resp.Message = ""
						resp.Data = grp
						c.NewLog("Access API", "INFO", "U", resp.Message)
					}
				} else {
					resp.Status = "ok"
					resp.Message = ""
					resp.Data = grp
					c.NewLog("Access API", "INFO", "U", resp.Message)
				}
			}
		}
		c.Data["json"] = resp
		c.ServeJSON()
	}
}

//检查用户组名是否存在
func (c *UserGroupController) CheckName() {
	resp := new(JsonResult)
	user := c.CheckSession()
	if user.Id > 0 {
		//修改密码用户信息
		type paramslist struct {
			Pid  int64  `form:"pid"`  //上级用户组ID
			Name string `form:"name"` //用户组名称
		}
		params := new(paramslist)
		if err := c.ParseForm(params); err != nil { //获取参数错误
			resp.Status = "fail-a"
			resp.Message = fmt.Sprintf("Parameter input error:%s", err.Error())
			c.NewLog("Access API", "WARN", "C", resp.Message)
		} else {
			if params.Pid > 0 {
				pgrp := new(models.SysGroup)
				pgrp.Id = params.Pid
				if err := pgrp.Read(); err != nil { //文件不存在
					params.Pid = 0
				}
			}
			grp := new(models.SysGroup)
			grp.Name = params.Name
			grp.Pid = params.Pid
			if err := grp.Read("Pid", "Name"); err == nil {
				resp.Status = "fail-b"
				resp.Message = "The user group name under the same level already exists!"
				c.NewLog("Access API", "WARN", "C", resp.Message)
			} else {
				resp.Status = "ok"
				resp.Message = "User group name dos not exists!"
				resp.Data = ""
				c.NewLog("Access API", "INFO", "C", resp.Message)
			}
		}
		c.Data["json"] = resp
		c.ServeJSON()
	}
}

//读取用户组列表
func (c *UserGroupController) GetGroupList() {
	resp := new(JsonResult)
	user := c.CheckSession()
	if user.Id > 0 {
		//参数信息
		type paramslist struct { //二选一,不选的设置为空字符串
			NameLike string `form:"namelike"` //名称
			Rmkslike string `form:"rmkslike"` //备注
		}
		params := new(paramslist)
		if err := c.ParseForm(params); err != nil { //获取参数错误
			resp.Status = "fail-a"
			resp.Message = fmt.Sprintf("Parameter input error:%s", err.Error())
			c.NewLog("Access API", "WARN", "R", resp.Message)
		} else {
			grp := new(models.SysGroup)
			if grps, err := grp.GetGroups(params.NameLike, params.Rmkslike); err != nil { //
				resp.Status = "fail-b"
				resp.Message = fmt.Sprintf("An error occurred while querying:%s", err.Error())
				c.NewLog("Access API", "WARN", "C", resp.Message)
			} else {
				resp.Status = "ok"
				resp.Message = ("Get user groups list successfully!")
				resp.Data = grps
				c.NewLog("Access API", "INFO", "C", resp.Message)
			}
		}
		c.Data["json"] = resp
		c.ServeJSON()
	}
}

//删除用户组
func (c *UserGroupController) Delete() {
	resp := new(JsonResult)
	user := c.CheckSession()
	if user.Id > 0 {
		//参数信息
		type paramslist struct {
			Id int64 `form:"id"` //需要被取消的共享节点
		}
		params := new(paramslist)
		if err := c.ParseForm(params); err != nil { //获取参数错误
			resp.Status = "fail-a"
			resp.Message = fmt.Sprintf("Parameter input error:%s", err.Error())
			c.NewLog("Access API", "WARN", "D", resp.Message)
		} else {
			s := new(models.SysGroup)
			s.Id = params.Id
			if _, err := s.Delete(); err != nil { //删除
				resp.Status = "fail-b"
				resp.Message = fmt.Sprintf("Invalid group ID:%s", err.Error())
				c.NewLog("Access API", "WARN", "D", resp.Message)
			} else {
				resp.Status = "ok"
				resp.Message = "The user group was successfully deleted!"
				resp.Data = ""
				c.NewLog("Access API", "INFO", "D", resp.Message)
			}
		}
		c.Data["json"] = resp
		c.ServeJSON()
	}
}
