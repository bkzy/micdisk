package controllers

import (
	"fmt"
	"micdisk/models"
	"strings"

	"github.com/astaxie/beego"
)

const (
	_tmpfiledir = "data/_tmp/" //临时文件目录
	_webtitle   = "MicDisk"
	_icopath    = "static/img/micloud.ico"
	_logopath   = "static/img/micloud.svg"
	_firstPage  = "index" //首页面
)

var _LOG_OPR_TYPE = map[string]string{
	"C":  "Create",
	"R":  "Read",
	"U":  "Update",
	"D":  "Delete",
	"O":  "Other",
	"LI": "Login",
	"LO": "Logout",
}
var _LOG_MSG_TYPE = map[string]string{
	"DEBG": "Debug",     //调试信息
	"INFO": "Info",      //一般信息
	"WARN": "Warning",   //一般警告
	"EROR": "Error",     //一般错误信息
	"CRIT": "Critic",    //中度警告
	"ALRT": "Alert",     //严重警告
	"EMER": "Emergency", //紧急错误
}

//响应信息结构体
type JsonResult struct {
	Status  string      `json:"status"`  //状态,"ok"为正常
	Message string      `json:"message"` //信息
	Data    interface{} `json:"data"`
}

type MainController struct {
	beego.Controller
}

/***********************************************
功能:检查登录状态
输入:无
输出:登录用户的ID
说明:检查用户的登录状态,如果用户登录了,返回用户ID;如果没有登录,则跳转到登录页面
编辑:wang_jp
时间:2020年3月12日
************************************************/
func (c *MainController) CheckSession() *models.SysUser {
	user := new(models.SysUser)
	umsg := c.GetSession("UserMsg") //获取Session状态值
	if umsg == nil {                //如果获取到的值为空
		c.Redirect("/login", 302) //跳转到登录页面
		return user
	}
	user, _ = umsg.(*models.SysUser)
	return user
}

/***********************************************
功能:记录用户访问信息
输入:无
输出:数据库日志
说明:
编辑:wang_jp
时间:2020年3月17日
************************************************/
func (c *MainController) NewLog(act, msgtype, oprtype string, msg ...string) {
	log := new(models.SysLog)
	log.Action = act
	log.OprType = _LOG_OPR_TYPE[oprtype]
	log.MsgType = _LOG_MSG_TYPE[msgtype]
	log.ReqMethod = c.Ctx.Request.Method
	log.Host = c.Ctx.Request.Host

	strs := strings.Split(c.Ctx.Request.RequestURI, "?")
	if len(strs) > 0 {
		log.ReqUrl = strs[0] //保存请求的url
	}
	if len(strs) > 1 {
		if strings.Contains(strs[1], "pswd=") {
			log.ReqParams = "********" //保存请求的url
		} else {
			log.ReqParams = strs[1] //保存请求的url
		}
	} else {
		if log.ReqMethod != "GET" {
			params := c.Ctx.Request.PostForm
			if _, ok := params["pswd"]; ok {
				params["pswd"] = []string{"********"}
			}
			log.ReqParams = fmt.Sprint(params)
		}
	}

	if len(msg) > 0 { //如果输入了信息
		for i, s := range msg {
			log.Description += s
			if i < len(msg)-1 {
				log.Description += ";"
			}
		}
	}

	user := new(models.SysUser)
	uid := c.GetSession("UId")
	if uid != nil {
		user.Id, _ = uid.(int64)
		log.User = user
	}

	log.RemoteIp = c.Ctx.Request.RemoteAddr
	log.Create()
}

func (c *MainController) ApiGetConfig() {
	u := c.CheckSession()
	if u.Id > 0 {
		key, _ := u.AccessKey()
		c.Data["json"] = map[string]interface{}{"dfsCfg": models.DFS,
			"micdiskCfg": models.DISKCFG,
			"AccessKey":  key,
		}
		c.ServeJSON()
	}
}

func (c *MainController) ApiGetDfsConfig() {
	resp := new(JsonResult)
	type paramslist struct { //
		Name      string `form:"name"`      //用户账号
		AccessKey string `form:"accesskey"` //用户密钥
	}
	params := new(paramslist)
	if err := c.ParseForm(params); err != nil { //获取参数错误
		resp.Status = "fail-a"
		resp.Message = fmt.Sprintf("Parameter input error:%s", err.Error())
		c.NewLog("Access API", "WARN", "R", resp.Message)
	} else {
		u := new(models.SysUser)
		u.Name = params.Name
		u.Read("Name")
		if key, _ := u.AccessKey(); key != params.AccessKey {
			resp.Status = "fail-b"
			resp.Message = "Accesskey mismatch!"
			c.NewLog("Access API", "WARN", "R", resp.Message)
		} else {
			if u.Id > 0 {
				resp.Data = map[string]interface{}{"Domain": models.DISKCFG.DfsDomain,
					"Group":    models.DISKCFG.DfsGroup,
					"HashType": models.DISKCFG.DfsHashType,
				}
				resp.Status = "ok"
			}
		}
	}
	c.Data["json"] = resp
	c.ServeJSON()
}
