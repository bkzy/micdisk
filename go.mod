module micdisk

go 1.16

require (
	github.com/astaxie/beego v1.12.3
	github.com/bkzy-wangjp/go-fastdfs-api v1.0.0
	github.com/go-sql-driver/mysql v1.5.0
	github.com/mattn/go-sqlite3 v2.0.3+incompatible
	github.com/shiena/ansicolor v0.0.0-20200904210342-c7312218db18 // indirect
	github.com/smartystreets/goconvey v1.6.4
	golang.org/x/text v0.3.0
	gopkg.in/ini.v1 v1.62.0
)
