package main

import (
	"github.com/astaxie/beego"
)

func init() {
	beego.BConfig.WebConfig.Session.SessionOn = true //默认开启
	beego.BConfig.CopyRequestBody = true
}
