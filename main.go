package main

import (
	_ "micdisk/routers"

	"github.com/astaxie/beego"
)

func main() {
	beego.Run()
}
