package models

import (
	"time"

	"github.com/astaxie/beego/orm"
)

//创建新条目
func (d *DownloadLog) Create() error {
	o := orm.NewOrm()
	o.Using("default")
	d.CreateTime = SqliteAddLocalOffset(time.Now(), true)
	_, err := o.Insert(d)
	d.CreateTime = SqliteAddLocalOffset(d.CreateTime, false)
	return err
}
