package models

import (
	"fmt"

	"github.com/astaxie/beego/orm"
)

//创建非重复索引
func (f *DfsFileIndex) ReadOrCreate() (bool, int64, error) {
	o := orm.NewOrm()
	o.Using("default")
	return o.ReadOrCreate(f, "Md5")
}

//读取
func (f *DfsFileIndex) Read(cols ...string) error {
	o := orm.NewOrm()
	o.Using("default")
	return o.Read(f, cols...)
}

//获取使用该文件的所有目录条目
func (f *DfsFileIndex) GetCatalogs() (int64, []*UserCatalog, error) {
	o := orm.NewOrm()
	o.Using("default")
	var ctlgs []*UserCatalog
	qt := o.QueryTable("UserCatalog").Filter("File__Id", f.Id)
	n, err := qt.All(&ctlgs)
	return n, ctlgs, err
}

//删除
func (f *DfsFileIndex) Delete() error {
	n, _, err := f.GetCatalogs()
	if err != nil {
		return err
	}
	if n > 0 {
		return fmt.Errorf("the file has been referenced %d times and cannot be deleted", n)
	}
	_, ok, _ := DFS.DeleteFile(f.Path, f.Md5)
	if ok {
		o := orm.NewOrm()
		o.Using("default")
		k, err := o.Delete(f)
		if err != nil || k == 0 {
			return fmt.Errorf("the file was successfully deleted, but an error occurred while deleting the file index:%s", err.Error())
		}
	} else {
		return fmt.Errorf("error deleting file! ")
	}
	return nil
}
