package models

import (
	"time"

	//"github.com/astaxie/beego/logs"
	"github.com/astaxie/beego/orm"
)

//创建新条目
func (s *ShareList) Create() error {
	o := orm.NewOrm()
	o.Using("default")
	s.CreateTime = SqliteAddLocalOffset(time.Now(), true)
	s.ShareBegineTime = SqliteAddLocalOffset(time.Now(), true)
	_, err := o.Insert(s)
	s.CreateTime = SqliteAddLocalOffset(s.CreateTime, false)
	s.ShareBegineTime = SqliteAddLocalOffset(s.ShareBegineTime, false)
	return err
}

//读取
func (s *ShareList) Read(loadrel ...bool) error {
	o := orm.NewOrm()
	o.Using("default")
	qt := o.QueryTable("ShareList").Filter("Id", s.Id)
	err := qt.RelatedSel().One(s)
	if err == nil {
		if len(loadrel) > 0 {
			if loadrel[0] {
				s.LoadRelated()
			}
		}
	}
	return err
}

//删除
func (s *ShareList) Delete() error {
	o := orm.NewOrm()
	o.Using("default")
	m2m := o.QueryM2M(s, "Users")
	m2m.Clear()
	m2m = o.QueryM2M(s, "Groups")
	m2m.Clear()
	_, err := o.Delete(s)
	return err
}

//更新
func (s *ShareList) Update(cols ...string) error {
	o := orm.NewOrm()
	o.Using("default")
	s.UpdateTime = SqliteAddLocalOffset(time.Now(), true)
	cols = append(cols, "UpdataTime")
	_, err := o.Update(s, cols...)
	s.UpdateTime = SqliteAddLocalOffset(s.UpdateTime, false)
	return err
}

//加载用户和组
func (s *ShareList) LoadRelated() {
	o := orm.NewOrm()
	o.Using("default")
	o.LoadRelated(s, "Users")
	o.LoadRelated(s, "Groups")
	for i, u := range s.Users { //脱敏
		u.Password = "********"
		u.MaxSpace = 0
		u.CreateTime = time.Now()
		s.Users[i] = u
	}
}

//共享给用户
func (s *ShareList) ShareWithUsers(userids ...int64) (int64, error) {
	o := orm.NewOrm()
	o.Using("default")
	m2m := o.QueryM2M(s, "Users")
	var users []*SysUser
	for _, id := range userids {
		u := &SysUser{Id: id}
		if u.Read() == nil { //id存在
			if !m2m.Exist(u) { //不存在关系
				users = append(users, u)
			}
		}
	}
	if len(users) > 0 {
		return m2m.Add(users)
	} else {
		return 0, nil
	}
}

//共享给用户组
func (s *ShareList) ShareWithGroups(groupids ...int64) (int64, error) {
	o := orm.NewOrm()
	o.Using("default")
	m2m := o.QueryM2M(s, "Groups")
	var grps []*SysGroup
	for _, gid := range groupids {
		g := &SysGroup{Id: gid}
		if g.Read() == nil { //id存在
			if !m2m.Exist(g) { //不存在关系
				grps = append(grps, g)
			}
		}
	}
	if len(grps) > 0 {
		return m2m.Add(grps)
	} else {
		return 0, nil
	}
}

//取消给用户的共享
func (s *ShareList) RemoveUsers(userids ...int64) (int64, error) {
	o := orm.NewOrm()
	o.Using("default")
	m2m := o.QueryM2M(s, "Users")
	var users []*SysUser
	for _, id := range userids {
		u := &SysUser{Id: id}
		if u.Read() == nil { //id存在
			if m2m.Exist(u) { //存在关系
				users = append(users, u)
			}
		}
	}
	if len(users) > 0 {
		return m2m.Remove(users)
	} else {
		return 0, nil
	}
}

//取消给用户组的共享
func (s *ShareList) RemoveGroups(groupids ...int64) (int64, error) {
	o := orm.NewOrm()
	o.Using("default")
	m2m := o.QueryM2M(s, "Groups")
	var grps []*SysGroup
	for _, gid := range groupids {
		g := &SysGroup{Id: gid}
		if g.Read() == nil { //id存在
			if m2m.Exist(g) { //存在关系
				grps = append(grps, g)
			}
		}
	}
	if len(grps) > 0 {
		return m2m.Remove(grps)
	} else {
		return 0, nil
	}
}
