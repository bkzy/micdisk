package models

import (
	"fmt"
	//"fmt"
	//"time"

	//"github.com/astaxie/beego/logs"
	"github.com/astaxie/beego/orm"
)

//读取或者创建
//返回参数:
//bool:创建true,读取false
//int64:创建或者读取的id号
//eror:错误信息
func (g *SysGroup) ReadOrCreate() (bool, int64, error) {
	o := orm.NewOrm()
	o.Using("default")
	return o.ReadOrCreate(g, "Pid", "Name")
}

//初始化插入所有人组
func (g *SysGroup) initData() {
	gs := []SysGroup{
		{Pid: 0, Name: _allUserGroupName, Remarkes: "所有人"},
		{Pid: 0, Name: "admin", Remarkes: "管理员组"},
	}
	for _, gr := range gs {
		gr.ReadOrCreate()
	}
}

//读取
func (g *SysGroup) Read(colsname ...string) error {
	o := orm.NewOrm()
	o.Using("default")
	if g.Id == 0 && len(colsname) == 0 {
		var cols []string
		if g.Pid >= 0 {
			cols = append(cols, "Pid")
		}
		if len(g.Name) > 0 {
			cols = append(cols, "Name")
		}
		if len(g.Remarkes) > 0 {
			cols = append(cols, "Remarkes")
		}
		return o.Read(g, cols...)
	} else {
		return o.Read(g, colsname...)
	}
}

//更新
func (g *SysGroup) Update(cols ...string) (int64, error) {
	o := orm.NewOrm()
	o.Using("default")
	return o.Update(g, cols...)
}

//删除
func (g *SysGroup) Delete() (int64, error) {
	o := orm.NewOrm()
	o.Using("default")
	//清除关联关系
	m2m := o.QueryM2M(g, "Users")
	m2m.Clear()
	m2m = o.QueryM2M(g, "Shares")
	m2m.Clear()
	return o.Delete(g)
}

//获取所有的用户组列表
func (g *SysGroup) GetGroups(namelike, rmkslike string) ([]*SysGroup, error) {
	o := orm.NewOrm()
	o.Using("default")
	var grps []*SysGroup
	qt := o.QueryTable("SysGroup").Filter("Id__gt", 0)
	if len(namelike) > 0 {
		qt = qt.Filter("Name__icontains", namelike)
	}
	if len(rmkslike) > 0 {
		qt = qt.Filter("Remarkes__icontains", rmkslike)
	}
	_, err := qt.All(&grps)
	return grps, err
}

//添加用户
func (g *SysGroup) AddUsers(userids ...int64) (int64, error) {
	if g.Id == 0 {
		if g.Read() != nil {
			return 0, fmt.Errorf("the given group does not exist ")
		}
	}
	o := orm.NewOrm()
	o.Using("default")
	m2m := o.QueryM2M(g, "Users")
	var users []*SysUser
	for _, id := range userids {
		u := &SysUser{Id: id}
		if u.Read() == nil { //id存在
			if !m2m.Exist(u) { //不存在关系
				users = append(users, u)
			}
		}
	}
	if len(users) > 0 {
		return m2m.Add(users)
	} else {
		return 0, nil
	}
}

//获取共享给用户组的资源
func (g *SysGroup) GetShareLists() ([]*ShareList, error) {
	o := orm.NewOrm()
	o.Using("default")
	_, err := o.LoadRelated(g, "Shares", true)
	return g.Shares, err
}
