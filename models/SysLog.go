package models

import (
	"time"

	"github.com/astaxie/beego/orm"
)

//根据指定列读取
func (log *SysLog) Create() error {
	o := orm.NewOrm()
	o.Using("default")
	log.CreateTime = SqliteAddLocalOffset(time.Now(), true)
	id, err := o.Insert(log)
	log.CreateTime = SqliteAddLocalOffset(log.CreateTime, false)
	log.Id = id
	return err
}
