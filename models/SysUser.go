package models

import (
	"fmt"
	"time"

	"github.com/astaxie/beego/logs"
	"github.com/astaxie/beego/orm"
)

//根据指定列读取
func (user *SysUser) Read(cols ...string) error {
	o := orm.NewOrm()
	o.Using("default")
	if IsExistItem("Password", cols) {
		if len(user.Password) != 32 {
			user.Password = Md5str(user.Password)
		}
		user.Password = Md5str(user.Name, user.Password) //对用户名和密码md5摘要
	}
	err := o.Read(user, cols...)
	user.Password = "********"
	return err
}

//获取用户的AccessKey
func (user *SysUser) AccessKey(cols ...string) (string, error) {
	o := orm.NewOrm()
	o.Using("default")
	err := o.Read(user, cols...)
	if err != nil {
		return "", err
	}
	key := Md5str(_md5salt, user.Name, user.Password)
	return key, nil
}

//根据指定列更新数据,不指定则更新所有列,用户名不可更改
func (user *SysUser) Update(cols ...string) (bool, error) {
	o := orm.NewOrm()
	o.Using("default")
	if user.Id == 0 {
		u := new(SysUser)
		u.Name = user.Name
		err := u.Read("Name")
		if err != nil {
			return false, fmt.Errorf("user name '%s' does not exist! ", u.Name)
		}
		user.Id = u.Id
	}
	if IsExistItem("Password", cols) {
		if len(user.Password) != 32 {
			user.Password = Md5str(user.Password)
		}
		user.Password = Md5str(user.Name, user.Password) //对用户名和密码md5摘要
	}
	user.CreateTime = SqliteAddLocalOffset(user.CreateTime, true)
	n, err := o.Update(user, cols...)
	user.CreateTime = SqliteAddLocalOffset(user.CreateTime, false)
	if n > 0 {
		return true, nil
	} else {
		return false, err
	}
}

//注册,返回注册成功,id号,错误信息
func (user *SysUser) Register() (bool, int64, error) {
	o := orm.NewOrm()
	o.Using("default")
	//密码要现在前台Md5,否则对密码先进行Md5
	if len(user.Password) != 32 {
		user.Password = Md5str(user.Password)
	}
	user.Password = Md5str(user.Name, user.Password) //对用户名和密码md5摘要
	user.CreateTime = SqliteAddLocalOffset(time.Now(), true)
	user.MaxSpace = _userDefaultSpance
	crt, uid, err := o.ReadOrCreate(user, "Name")
	if crt {
		user.CreateTime = SqliteAddLocalOffset(user.CreateTime, false)
		m2m := o.QueryM2M(user, "Groups")
		grp := &SysGroup{Name: _allUserGroupName, Pid: 0}
		if err := grp.Read("Pid", "Name"); err == nil {
			m2m.Add(grp)
		}
	}
	return crt, uid, err
}

//登录
func (user *SysUser) Login() bool {
	//密码要现在前台Md5,否则对密码先进行Md5
	if len(user.Password) != 32 {
		user.Password = Md5str(user.Password)
	}
	user.Password = Md5str(user.Name, user.Password) //对用户名和密码md5摘要
	key, err := user.AccessKey("Name", "Password")
	user.Password = key
	if err == nil {
		return true
	} else {
		return false
	}
}

//初始化插入管理员
func (user *SysUser) initData() {
	users := []SysUser{
		{Name: "admin", Password: "admin123", Username: "系统管理员"},
	}
	for _, u := range users {
		u.Register()
		if u.Id > 0 && u.Name == "admin" { //将管理员添加到管理员组
			u.AddToGroup(&SysGroup{Name: "admin", Pid: 0})
		}
	}
}

//添加到用户组
func (user *SysUser) AddToGroup(g *SysGroup) (int64, error) {
	o := orm.NewOrm()
	o.Using("default")
	m2m := o.QueryM2M(user, "Groups")
	g.Read()
	if g.Id > 0 {
		if !m2m.Exist(g) {
			return m2m.Add(g)
		} else {
			return 0, nil
		}
	} else {
		return 0, fmt.Errorf("the given group does not exist! ")
	}
}

//获取用户名下所有活动节点
//disabled bool:false 只获取活动节点,true 只获取已经移除到回收站的节点
//pid int64:如果小于零,不限pid;如果大于等于0,则仅获取该pid的子节点。0为用户根目录
func (user *SysUser) GetUserCatalogs(disabled bool, pid int64) ([]*UserCatalog, error) {
	o := orm.NewOrm()
	o.Using("default")
	var ctlgs []*UserCatalog
	qt := o.QueryTable("UserCatalog").Filter("Disabled", disabled)
	if pid >= 0 {
		qt = qt.Filter("Pid", pid)
	}
	if pid <= 0 {
		qt = qt.Filter("User__Id", user.Id)
	}
	_, err := qt.RelatedSel("File", "UploadUser").All(&ctlgs)
	for i := range ctlgs { //用户信息脱敏,加载关联
		ctlgs[i].User.CreateTime = time.Now()
		ctlgs[i].User.Password = "********"
		ctlgs[i].User.MaxSpace = 0
		if ctlgs[i].UploadUser != nil {
			ctlgs[i].UploadUser.CreateTime = time.Now()
			ctlgs[i].UploadUser.Password = "********"
			ctlgs[i].UploadUser.MaxSpace = 0
		}
		ctlgs[i].LoadRelated("ShareLists")
	}
	return ctlgs, err
}

//获取用户名下的文件夹统计信息
//disabled bool:false 只获取活动节点,true 只获取已经移除到回收站的节点
//pid int64:如果大于等于0,则仅获取该pid的子节点。0为用户根目录
func (user *SysUser) GetCatalogStat(pid int64) interface{} {
	var ctlgs []*UserCatalog
	if pid <= 0 {
		o := orm.NewOrm()
		o.Using("default")
		qt := o.QueryTable("UserCatalog").Filter("User__Id", user.Id)
		_, err := qt.RelatedSel("File").All(&ctlgs)
		if err != nil {
			logs.Warn("user.GetCatalogStat error:%s", err.Error())
		}
	} else {
		uc := new(UserCatalog)
		uc.Id = pid
		err := uc.Read()
		if err != nil {
			logs.Warn("user.GetCatalogStat error:%s", err.Error())
		}
		ctlgs = uc.GetSubItems(user.Id)
	}
	var stat = map[string]int64{
		"TotalFileCnt":      0, //文件总数量
		"FileCnt":           0, //在用文件数量
		"RecycleBinFileCnt": 0, //在回收站中文件总数量
		"TotalDirCnt":       0, //文件夹总数量
		"DirCnt":            0, //在用文件夹数量
		"RecycleBinDirCnt":  0, //在回收站中的文件夹总数量
		"TotalSize":         0, //文件总大小
		"FileSize":          0, //在用文件大小
		"RecycleBinSize":    0, //在回收站中的文件占用的空间大小
	}
	for _, uc := range ctlgs {
		if uc.IsDir {
			stat["TotalDirCnt"] += 1
			if uc.Disabled {
				stat["RecycleBinDirCnt"] += 1
			} else {
				stat["DirCnt"] += 1
			}
		} else {
			stat["TotalFileCnt"] += 1
			stat["TotalSize"] += uc.File.Size
			if uc.Disabled {
				stat["RecycleBinFileCnt"] += 1
				stat["RecycleBinSize"] += uc.File.Size
			} else {
				stat["FileCnt"] += 1
				stat["FileSize"] += uc.File.Size
			}
		}
	}
	return stat
}

//清空回收站
//返回清空的项目条数
func (user *SysUser) ClearRecycleBin() int {
	ctlgs, err := user.GetUserCatalogs(true, -1)
	if err != nil {
		logs.Warn("user.ClearRecycleBin.GetUserCatalogs error:%s", err.Error())
	}
	n := 0
	for _, ctlg := range ctlgs {
		err := ctlg.Delete()
		if err != nil {
			logs.Warn("user.ClearRecycleBin.Delete error:%s", err.Error())
		} else {
			n += 1
		}
	}
	return n
}

//在文件夹下搜索
//dirid:指定的文件夹的id
//namelike:名称中的关键字
func (user *SysUser) Search(dirid int64, namelike string) ([]*UserCatalog, error) {
	o := orm.NewOrm()
	o.Using("default")
	uc := new(UserCatalog)
	uc.Id = dirid
	sids := uc.GetSids()
	var ctlgs []*UserCatalog
	qt := o.QueryTable("UserCatalog").Filter("User__Id", user.Id).Filter("Name__icontains", namelike)
	qt = qt.Filter("Disabled", false)
	if dirid > 0 {
		qt = qt.Filter("Pid__in", sids)
	}
	_, err := qt.RelatedSel("File").All(&ctlgs)
	return ctlgs, err
}

//查询我共享的节点
func (user *SysUser) GetMySharedLists() ([]*ShareList, error) {
	o := orm.NewOrm()
	o.Using("default")
	var sls []*ShareList
	qt := o.QueryTable("ShareList").Filter("Catalog__User__Id", user.Id)
	qt = qt.Filter("Catalog__Disabled", false)
	_, err := qt.RelatedSel().All(&sls)
	for i := range sls { //用户信息脱敏,加载关联
		sls[i].Catalog.User.CreateTime = time.Now()
		sls[i].Catalog.User.Password = "********"
		sls[i].Catalog.User.MaxSpace = 0
		if sls[i].Catalog.UploadUser != nil {
			sls[i].Catalog.UploadUser.CreateTime = time.Now()
			sls[i].Catalog.UploadUser.Password = "********"
			sls[i].Catalog.UploadUser.MaxSpace = 0
		}

		sls[i].LoadRelated()
	}
	return sls, err
}

//查询共享给我的节点
func (user *SysUser) GetSharedToMeLists() []*ShareList {
	o := orm.NewOrm()
	o.Using("default")
	o.LoadRelated(user, "Shares", true)
	o.LoadRelated(user, "Groups")
	sls := user.Shares
	for _, g := range user.Groups {
		if s, e := g.GetShareLists(); e == nil {
			sls = append(sls, s...) //获取通过组分享给我的
		}
	}
	sharemap := make(map[int64]*ShareList) //使用map可以去掉id重复的分享
	nowunix := time.Now().Unix()
	for _, s := range sls {
		//过滤掉超时的分享
		if s.TermOfValidity == 0 || s.ShareBegineTime.Add(time.Duration(s.TermOfValidity)*time.Second).Unix() > nowunix {
			if s.Catalog.User.Id != user.Id { //去掉自己分享出去的
				sharemap[s.Id] = s
			}
		}
	}
	var rst []*ShareList
	for _, v := range sharemap { //用户信息脱敏
		v.Catalog.User.Password = "********"
		v.Catalog.User.MaxSpace = 0
		v.Catalog.User.CreateTime = time.Now()
		if v.Catalog.UploadUser != nil {
			v.Catalog.UploadUser.Password = "********"
			v.Catalog.UploadUser.MaxSpace = 0
			v.Catalog.UploadUser.CreateTime = time.Now()
		}
		rst = append(rst, v)
	}
	return rst
}

//查询用户列表
func (user *SysUser) GetUserNameLike(namelike, usernamelike string) ([]*SysUser, error) {
	o := orm.NewOrm()
	o.Using("default")
	var uss []*SysUser
	qt := o.QueryTable("SysUser")
	if len(namelike) > 0 {
		qt = qt.Filter("Name__icontains", namelike)
	} else {
		qt = qt.Filter("UserName__icontains", usernamelike)
	}
	_, err := qt.All(&uss, "Id", "Name", "UserName")
	return uss, err
}

//获取所有的用户列表
func (u *SysUser) GetUsers(namelike, unamelike string) ([]*SysUser, error) {
	o := orm.NewOrm()
	o.Using("default")
	var users []*SysUser
	qt := o.QueryTable("SysUser").Filter("Id__gt", 0)
	if len(namelike) > 0 {
		qt = qt.Filter("Name_icontains", namelike)
	}
	if len(unamelike) > 0 {
		qt = qt.Filter("Username_icontains", unamelike)
	}
	_, err := qt.All(users, "Id", "Name", "Username")
	return users, err
}
