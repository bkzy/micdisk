package models

import (
	"fmt"
	"strings"
	"time"

	"github.com/astaxie/beego/logs"
	"github.com/astaxie/beego/orm"
)

//创建新目录条目
func (c *UserCatalog) Create() error {
	o := orm.NewOrm()
	o.Using("default")
	if c.UploadUser == nil {
		c.UploadUser = c.User
	}
	c.CheckAndReName()
	c.CreateTime = SqliteAddLocalOffset(c.CreateTime, true)
	_, err := o.Insert(c)
	c.CreateTime = SqliteAddLocalOffset(c.CreateTime, false)
	return err
}

//创建或者读取目录
func (c *UserCatalog) ReadOrCreate(cols ...string) (bool, int64, error) {
	o := orm.NewOrm()
	o.Using("default")
	c.CreateTime = SqliteAddLocalOffset(c.CreateTime, true)
	b, n, e := o.ReadOrCreate(c, "User", cols...)
	c.CreateTime = SqliteAddLocalOffset(c.CreateTime, false)
	return b, n, e
}

//按Id读取
func (c *UserCatalog) Read() error {
	o := orm.NewOrm()
	o.Using("default")
	qt := o.QueryTable(c).Filter("Id", c.Id)
	err := qt.RelatedSel("File").One(c)
	return err
}

//删除
func (c *UserCatalog) Delete(force ...bool) error {
	o := orm.NewOrm()
	o.Using("default")
	fc := false
	if len(force) > 0 {
		fc = force[0]
	}
	if fc || c.Disabled {
		if _, err := o.Delete(c); err == nil { //删除目录
			if c.File != nil {
				c.File.Read()
				c.File.Delete() //删除文件和文件索引
			}
		} else {
			return err
		}
		return nil
	} else {
		return fmt.Errorf("please move to the recycle bin before deleting or force deletion! ")
	}
}

//检查目录条目名称并在必要时重命名
//如果发生了重命名,返回true, 否则返回 false
func (c *UserCatalog) CheckAndReName() (renamed bool) {
	o := orm.NewOrm()
	o.Using("default")
	var ctl []*UserCatalog
	mast_name, ext_name := c.DetachExtension()
	qt := o.QueryTable(c)
	qt = qt.Filter("User__Id", c.User.Id).Filter("Pid", c.Pid).Filter("IsDir", c.IsDir).Filter("Name__istartswith", mast_name)
	qt.All(&ctl)
	n := 0
	for i := 0; i < len(ctl); i++ {
		uc := ctl[i]
		if uc.Name == c.Name {
			renamed = true
			n += 1
			if c.IsDir {
				c.Name = fmt.Sprintf("%s(%d)", mast_name, n)
			} else {
				c.Name = fmt.Sprintf("%s(%d).%s", mast_name, n, ext_name)
			}
			i = 0
		}
	}
	return
}

//分离文件名的主名称和扩展名
func (c *UserCatalog) DetachExtension() (mast_name, ext_name string) {
	if c.IsDir {
		mast_name = c.Name
	} else {
		names := strings.Split(c.Name, ".")
		n := len(names)
		if n > 1 {
			ext_name = names[n-1]
			for i, nm := range names[:n-1] {
				mast_name += nm
				if i < n-2 {
					mast_name += "."
				}
			}
		} else {
			mast_name = c.Name
		}
	}
	return
}

//获取文件或者文件夹的父id列表
//输入参数仅用于递归调用
func (c *UserCatalog) GetPids(pids ...int64) []int64 {
	ids := []int64{c.Id}
	pids = append(ids, pids...)
	if c.Pid > 0 {
		ctlg := new(UserCatalog)
		ctlg.Id = c.Pid
		if ctlg.Read() == nil {
			pids = ctlg.GetPids(pids...)
		}
	}
	return pids
}

//获取文件下的所有文件和文件夹的ID列表
//输入参数仅用于递归调用
func (c *UserCatalog) GetSids(sids ...int64) []int64 {
	sids = append(sids, c.Id)
	if c.IsDir {
		o := orm.NewOrm()
		o.Using("default")
		var ctlgs []*UserCatalog
		qt := o.QueryTable(c).Filter("Pid", c.Id)
		rows, err := qt.All(&ctlgs) //读取子文件夹和文件
		if err != nil {
			logs.Warn(err.Error())
		}
		if rows > 0 {
			for _, uc := range ctlgs {
				sids = uc.GetSids(sids...)
			}
		}
	}
	return sids
}

//获取文件下的所有文件和文件夹的列表
//输入参数仅用于递归调用
func (c *UserCatalog) GetSubItems(userid int64, subs ...*UserCatalog) []*UserCatalog {
	if c.Id > 0 {
		subs = append(subs, c)
	}
	if c.IsDir {
		o := orm.NewOrm()
		o.Using("default")
		var ctlgs []*UserCatalog
		qt := o.QueryTable(c).Filter("User__Id", userid).Filter("Pid", c.Id)
		rows, err := qt.RelatedSel("File").All(&ctlgs) //读取子文件夹和文件
		if err != nil {
			logs.Warn(err.Error())
		}
		if rows > 0 {
			for _, uc := range ctlgs {
				subs = uc.GetSubItems(userid, subs...)
			}
		}
	}
	return subs
}

//获取文件或者文件夹路径
func (c *UserCatalog) GetPath() (path map[int64]string) {
	pids := c.GetPids()
	path = make(map[int64]string)
	path[0] = "mydisk"
	for _, id := range pids {
		uc := new(UserCatalog)
		uc.Id = id
		if uc.Read() == nil {
			path[id] = uc.Name
		}
	}
	return
}

//获取全部活动的文件或者文件夹列表
func (c *UserCatalog) GetCatalogs(userid int64, isall bool) ([]*UserCatalog, error) {
	o := orm.NewOrm()
	o.Using("default")
	var ctlgs []*UserCatalog
	qt := o.QueryTable(c).Filter("User__Id", userid).Filter("Disabled", false)
	if !isall {
		qt = qt.Filter("IsDir", true)
	} else {
		qt = qt.RelatedSel("File")
	}
	_, err := qt.All(&ctlgs) //读取子文件夹和文件
	return ctlgs, err
}

//更新 disable字段
func (c *UserCatalog) UpdateDisabled(disable bool) error {
	o := orm.NewOrm()
	o.Using("default")
	c.Disabled = disable
	c.DisableTime = SqliteAddLocalOffset(time.Now(), true)
	c.UpdateTime = SqliteAddLocalOffset(time.Now(), true)
	_, err := o.Update(c, "Disabled", "DisableTime", "UpdateTime")
	c.DisableTime = SqliteAddLocalOffset(c.DisableTime, false)
	c.UpdateTime = SqliteAddLocalOffset(c.UpdateTime, false)
	return err
}

//更新
func (c *UserCatalog) Update(cols ...string) error {
	o := orm.NewOrm()
	o.Using("default")
	c.UpdateTime = SqliteAddLocalOffset(time.Now(), true)
	cols = append(cols, "UpdateTime")
	for _, col := range cols {
		if col == "Name" {
			c.CheckAndReName()
			break
		}
	}
	_, err := o.Update(c, cols...)
	c.UpdateTime = SqliteAddLocalOffset(c.UpdateTime, false)
	return err
}

//禁用文件或者文件夹(软删除)
func (c *UserCatalog) Remove(disablesub ...bool) (disabled bool) {
	o := orm.NewOrm()
	o.Using("default")
	disabled = true
	if !c.IsDir { //是文件
		c.UpdateDisabled(true) //直接设置禁用标记
	} else { //文件夹
		sids := c.GetSids()
		if len(sids) == 1 {
			c.UpdateDisabled(true) //直接设置禁用标记
		} else {
			dissub := false
			if len(disablesub) > 0 {
				dissub = disablesub[0]
			}
			if dissub { //如果设置了子文件夹同步禁用
				for _, id := range sids {
					uc := new(UserCatalog)
					uc.Id = id
					uc.Read()
					uc.UpdateDisabled(true) //子文件/文件夹逐一设置禁用
				}
			} else {
				disabled = false //没有设置子文件夹同步禁用标记,返回禁用失败
			}
		}
	}
	return
}

//删除已经禁用的文件或者文件夹(物理删除)
//force bool:{true:强制删除,false:只删除已经禁用的}
//deletesub bool:{true:连同子文件/文件夹一同删除,false:只自己,如果有子文件或者文件夹,返回false}
func (c *UserCatalog) DeleteMul(force bool, deletesub ...bool) (deleted bool) {
	o := orm.NewOrm()
	o.Using("default")
	deleted = false
	if !c.IsDir { //是文件
		if c.Delete(force) == nil {
			deleted = true
		}
	} else { //文件夹
		sids := c.GetSids()
		if len(sids) == 1 { //如果没有读取到
			if c.Delete(force) == nil {
				deleted = true
			}
		} else {
			dissub := false
			if len(deletesub) > 0 {
				dissub = deletesub[0]
			}
			if dissub { //如果设置了子文件夹同步禁用
				for _, id := range sids {
					uc := new(UserCatalog)
					uc.Id = id
					uc.Read()
					//子文件/文件夹逐一设置禁用
					if uc.Delete(force) == nil {
						deleted = true
					}
				}
			} else {
				deleted = false //没有设置子文件夹同步禁用标记,返回禁用失败
			}
		}
	}
	return
}

//恢复使用,如果有被禁用的父级文件夹,同步恢复
//返回恢复使用的层级数量
func (c *UserCatalog) Recovery() (n int) {
	pids := c.GetPids()
	n = 0
	for _, id := range pids {
		uc := new(UserCatalog)
		uc.Id = id
		uc.Read()
		if uc.Disabled {
			uc.UpdateDisabled(false)
			n += 1
		}
	}
	return n
}

//加载下载日志和共享信息
func (c *UserCatalog) LoadRelated(related ...string) {
	o := orm.NewOrm()
	o.Using("default")
	for _, rel := range related {
		if rel == "ShareLists" || rel == "DownloadLogs" {
			o.LoadRelated(c, rel)
		}
	}
}

//复制
func (c *UserCatalog) Copy() *UserCatalog {
	uc := new(UserCatalog)
	uc.User = c.User
	uc.Pid = c.Pid
	uc.Name = c.Name
	uc.Description = c.Description
	uc.IsDir = c.IsDir
	uc.FlashTrans = c.FlashTrans
	uc.File = c.File
	return uc
}

//复制到指定目录
func (c *UserCatalog) CopyTo(dirid int64) int64 {
	o := orm.NewOrm()
	o.Using("default")
	var cnt int64
	if c.IsDir {
		uc := c.Copy()
		uc.Pid = dirid
		if err := uc.Create(); err == nil {
			cnt += 1
			var ucs []*UserCatalog
			qt := o.QueryTable(c).Filter("Pid", c.Id).Filter("Disabled", false)
			if _, err := qt.All(&ucs); err == nil {
				for _, subc := range ucs {
					cnt += subc.CopyTo(uc.Id)
				}
			}
		}
	} else {
		uc := c.Copy()
		uc.Pid = dirid
		if uc.Create() == nil {
			cnt += 1
		}
	}
	return cnt
}

//自动创建通过API上传的文件的目录
//上传文件时的scene必须是用户表中存在的Name,否则无法创建目录
func (c *UserCatalog) WebHook(rscene, subscene, filename string, flashtrans, replace bool, fidex *DfsFileIndex) error {
	u := new(SysUser)
	u.Name = rscene
	if err := u.Read("Name"); err != nil {
		return err
	}

	sc := new(UserCatalog) //文件目录
	sc.User = u
	sc.Pid = 0
	sc.IsDir = true
	if len(subscene) > 0 { //如果有子场景
		scenes := strings.Split(subscene, "/") //分割路径获取文件夹名
		for _, v := range scenes {
			sc.Name = v
			if _, _, e := sc.ReadOrCreate("Name", "Pid", "IsDir"); e != nil {
				return e
			}
			sc.Pid = sc.Id
			sc.Id = 0
		}
	}
	c.User = u
	c.UploadUser = u
	c.Pid = sc.Pid
	c.IsDir = false
	c.FlashTrans = flashtrans
	c.File = fidex

	fpaths := strings.Split(filename, "/") //分割文件路径获取文件名
	if len(fpaths) == 0 {
		return fmt.Errorf("file name error")
	}
	c.Name = fpaths[len(fpaths)-1]

	var e error
	if replace { //替换文件
		n, _, err := c.ReadOrCreate("Name", "Pid", "IsDir")
		if !n && err == nil { //非新建的情况下
			if c.File.Id != fidex.Id { //如果不同
				e = c.Update("File") //则更新
				if e == nil {
					logs.Info("User [%s] update catalog item [%s/%s] by webhook", rscene, subscene, c.Name)
				}
			}
		} else {
			e = err
			if e == nil {
				logs.Info("User [%s] create catalog item [%s/%s] by webhook", rscene, subscene, c.Name)
			}
		}
	} else {
		e = c.Create()
		if e == nil {
			logs.Info("User [%s] create catalog item [%s/%s] by webhook", rscene, subscene, c.Name)
		}
	}
	return e
}

//重载用户名下的文件名
//只更新文件名与上传时的文件名不匹配的目录
//用于解决小文件合并后造成的文件名变更的问题
//2021年6月15日
func (c *UserCatalog) ReloadCatalogFileName(userid int64) int {
	o := orm.NewOrm()
	o.Using("default")
	var cnt int
	var ctlgs []*UserCatalog
	qt := o.QueryTable(c).Filter("User__Id", userid).Filter("IsDir", false)
	_, err := qt.RelatedSel("File").All(&ctlgs) //读取文件
	if err == nil {
		for _, ctlg := range ctlgs {
			if _, fileinfo, e := DFS.GetFileInfo("", ctlg.File.Md5); e == nil {
				if ctlg.Name != fileinfo.Name {
					ctlg.Name = fileinfo.Name
					if ctlg.Update("Name") == nil {
						cnt += 1
					}
				}
			}
		}
	}
	return cnt
}
