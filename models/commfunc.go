package models

import (
	"bytes"
	"crypto/md5"
	"fmt"
	"io/ioutil"
	"os/exec"
	"time"

	"github.com/astaxie/beego/logs"
	"golang.org/x/text/encoding/simplifiedchinese"
	"golang.org/x/text/transform"
)

/*************************************************
功能:对字符串进行MD5加密
输入:待加密字符串
输出:加密后的字符串
说明:
时间:2020年3月12日
*************************************************/
func Md5str(strs ...string) string {
	var str string
	for _, s := range strs {
		str += s
	}
	data := []byte(str)
	has := md5.Sum(data)
	md5str := fmt.Sprintf("%x", has)
	return md5str
}

//设置Sqlite3时间加上或者减去本地时间
func SqliteAddLocalOffset(utc time.Time, add bool) time.Time {
	if utc.Unix() < 100000 {
		utc = time.Now()
	}
	if DISKCFG.DbType == "sqlite3" {
		_, offset := time.Now().Zone()
		if !add {
			offset *= -1
		}
		utc = utc.Add(time.Duration(offset) * time.Second)
	}
	return utc
}

func GbkToUtf8(s []byte) []byte {
	reader := transform.NewReader(bytes.NewReader(s), simplifiedchinese.GBK.NewDecoder())
	d, e := ioutil.ReadAll(reader)
	if e != nil {
		return nil
	}
	var bstr []byte
	for _, c := range d {
		if c > 0 {
			bstr = append(bstr, c)
		}
	}
	return bstr
}

func ExecuteBatFile(batfilepath string) error {
	//重启服务 及地址配置
	path := batfilepath
	c := exec.Command(path)
	var stderr bytes.Buffer
	c.Stderr = &stderr
	if err := c.Run(); err != nil {
		cmderr := GbkToUtf8(stderr.Bytes())
		msg := fmt.Sprintf("%s:%s", err.Error(), cmderr)
		logs.Error("Reboot go-fastdfs fail:%s", msg)
		return fmt.Errorf(msg)
	} else {
		logs.Info("Reboot go-fastdfs ok!")
		return nil
	}
}
