package models

import (
	"github.com/bkzy-wangjp/go-fastdfs-api"
)

type FastDfs struct {
	fastdfs.GoFastDfs
}

var DFS *FastDfs

//创建 fastdfs 连接
func NewFastDfs(host, group string) *FastDfs {
	dfs := new(FastDfs)
	dfs.DfsHost = host
	dfs.Group = group
	dfs.MakeHost()
	return dfs
}

//上传文件
//输入参数:
//file string:文件路径
//scene string:场景
//output string:输出结果类型,json或者text
//path string:自定义路径
//filename string:重命名
//description string:描述
//code string:goolge认证码
//auth_token string:自定义认证码
//md5 string:用于秒传的文件md5摘要
//输出信息:
//string:go-fastdfs 返回的原始字符串
//*fastdfs.FileResult:go-fastdfs 返回的文件上传信息结构体
//*DfsFileIndex:将 go-fastdfs返回的文件信息保存到文件索引中时的文件索引信息
//error:错误信息
func (dfs *FastDfs) UploadFileToDfs(file, scene, output, path, filename, code,
	auth_token, md5 string) (string, *fastdfs.FileResult, *DfsFileIndex, error) {
	var params []fastdfs.ParamKeyVal
	var param fastdfs.ParamKeyVal
	if len(scene) > 0 {
		param.Key = "scene"
		param.Value = scene
		params = append(params, param)
	}
	if len(output) > 0 {
		param.Key = "output"
		param.Value = output
		params = append(params, param)
	}
	if len(path) > 0 {
		param.Key = "path"
		param.Value = path
		params = append(params, param)
	}
	if len(filename) > 0 {
		param.Key = "filename"
		param.Value = filename
		params = append(params, param)
	}
	if len(auth_token) > 0 {
		param.Key = "auth_token"
		param.Value = auth_token
		params = append(params, param)
	}
	if len(md5) > 0 {
		param.Key = "md5"
		param.Value = md5
		params = append(params, param)
	}
	rawstr, resp, err := dfs.UploadFile(file, params...)
	if err == nil {
		f := NewFileIndex(resp)
		f.ReadOrCreate()
		return rawstr, resp, f, err
	} else {
		return rawstr, resp, nil, err
	}
}

//通过文件上传结果新建文件索引
func NewFileIndex(res *fastdfs.FileResult) *DfsFileIndex {
	f := new(DfsFileIndex)
	f.Domain = res.Domain
	f.Md5 = res.Md5
	f.ModTime = res.ModTime
	f.Path = res.Path
	f.Scene = res.Scene
	f.Size = res.Size
	f.Url = res.Url
	return f
}
