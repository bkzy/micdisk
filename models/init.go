package models

import (
	"fmt"
	"os"
	"time"

	"github.com/astaxie/beego/logs"
	"github.com/astaxie/beego/orm"
	_ "github.com/go-sql-driver/mysql"
	_ "github.com/mattn/go-sqlite3"
)

func init() {
	readCfg() //读取配置信息
	//设置日志信息
	logs.Async(1e3)                                                 //异步模式
	logs.SetLogger(logs.AdapterConsole, `{"level":6,"color":true}`) //屏幕输出设置
	logset := fmt.Sprintf(`{"filename":"%s","level":%d,"maxlines":%d,"maxsize":0,"daily":true,"maxdays":%d,"separate":["emergency", "alert", "critical", "error", "warning", "notice", "info", "debug"]}`,
		"log/micdisk.log", 6, 10000, 30)
	logs.SetLogger(logs.AdapterMultiFile, logset) //文件输出设置
	logs.EnableFuncCallDepth(false)

	if err := orm.RegisterDataBase("default", DISKCFG.DbType, DISKCFG.DbHost, 30); err != nil {
		logs.Emergency("Connect to database fault[连接数据库失败]: %v", err)
		fmt.Printf("The program will closed after 20 second[程序将在20秒后关闭]:")
		for i := 20; i > 0; i-- {
			time.Sleep(time.Second)
			fmt.Printf(".")
		}
		os.Exit(1)
	}
	//注册数据库表
	orm.RegisterModel(new(DfsFileIndex), new(ShareList), new(UserCatalog), new(DownloadLog))
	orm.RegisterModel(new(SysLog), new(SysUser), new(SysGroup))
	//自动创建数据库表
	orm.RunSyncdb("default", false, true)
	//调试模式
	orm.Debug = DISKCFG.Debug

	//初始化用户组
	grp := new(SysGroup)
	grp.initData()
	//初始化用户
	user := new(SysUser)
	user.initData()

	DFS = NewFastDfs(DISKCFG.DfsHost, DISKCFG.DfsGroup)
	_, dfscfg, err := DFS.GetGloablConfig()
	if err == nil {
		DISKCFG.DfsDomain = dfscfg.DownloadDomain

		if len(DISKCFG.DfsDomain) == 0 {
			DISKCFG.DfsDomain = dfscfg.Host
		}
		DISKCFG.EnableHttps = dfscfg.EnableHttps
		DISKCFG.DfsHashType = dfscfg.FileSumArithmetic
		DISKCFG.DfsErr = nil
	} else {
		DISKCFG.DfsErr = err
		logs.Error("获取go-fastdfs配置信息失败:%s", err.Error())
		go func() {
			for {
				ExecuteBatFile(DISKCFG.DfsRebootPath)
				time.Sleep(10 * time.Second)
				_, dfscfg, err := DFS.GetGloablConfig()
				if err == nil {
					DISKCFG.DfsDomain = dfscfg.DownloadDomain
					if len(DISKCFG.DfsDomain) == 0 {
						DISKCFG.DfsDomain = dfscfg.Host
					}
					DISKCFG.EnableHttps = dfscfg.EnableHttps
					DISKCFG.DfsHashType = dfscfg.FileSumArithmetic
					DISKCFG.DfsErr = nil
					break
				} else {
					logs.Error("获取go-fastdfs配置信息失败:%s", err.Error())
				}
				time.Sleep(60 * time.Second)
			}
		}()
	}
}
