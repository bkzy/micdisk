package models

import (
	"testing"
)

/*
func TestMd5str(t *testing.T) {
	tests := []struct {
		strs []string
		res  string
	}{
		{[]string{"abcd"}, "e2fc714c4727ee9395f324cd2e7f331f"},
		{[]string{"abcd", "12345"}, "a99442d2a736365f5fe637e299b0e339"},
	}
	for _, tt := range tests {
		md5 := Md5str(tt.strs...)
		if md5 != tt.res {
			t.Error("得到的值:", md5, "期望的值:", tt.res)
		}
	}
}

func TestRegister(t *testing.T) {
	tests := []struct {
		name string
		pswd string
	}{
		{"admin", "admin123"},
	}
	for _, tt := range tests {
		user := new(SysUser)
		user.Name = tt.name
		user.Password = Md5str(tt.pswd)
		ist, uid, err := user.Register()
		t.Log("新建:", ist, "Id:", uid, "错误信息:", err)
	}
}

func TestUserUpdate(t *testing.T) {
	tests := []SysUser{
		{Name: "admin", Password: "admin123", MaxSpace: 100, CreateTime: time.Now()},
	}
	for _, tt := range tests {
		tt.Password = Md5str(tt.Name, Md5str(tt.Password))
		ok, err := tt.Update()
		t.Log("更新成功:", ok, "用户信息:", tt, "错误信息:", err)
	}
}

func TestUserLogin(t *testing.T) {
	tests := []struct {
		name string
		pswd string
		ok   bool
	}{
		{"admin", "admin123", true},
		{"admine", "admin123", false},
		{"admin", "admin1234", false},
	}
	for _, tt := range tests {
		user := new(SysUser)
		user.Name = tt.name
		user.Password = tt.pswd
		ok := user.Login()
		if ok == tt.ok {
			t.Log(user)
		} else {
			t.Error("登录错误")
		}
	}
}

func TestDetachExtension(t *testing.T) {
	tests := []struct {
		isdir    bool
		fullname string
		name     string
		extname  string
	}{
		{false, "admin", "admin", ""},
		{false, "admin.exe", "admin", "exe"},
		{false, "a.b.c.xels", "a.b.c", "xels"},
		{true, "a.b.c.xels", "a.b.c.xels", ""},
	}
	for _, tt := range tests {
		uc := new(UserCatalog)
		uc.Name = tt.fullname
		uc.IsDir = tt.isdir
		n, e := uc.DetachExtension()
		if n != tt.name || e != tt.extname {
			t.Errorf("期望值:%s,%s;获得值:%s,%s", tt.name, tt.extname, n, e)
		}
	}
}

func TestCheckAndReName(t *testing.T) {
	tests := []struct {
		isdir bool
		name  string
		res   string
	}{
		{false, "重命名.xlsx", "重命名(3).xlsx"},
	}
	for _, tt := range tests {
		uc := new(UserCatalog)
		uc.Name = tt.name
		uc.IsDir = tt.isdir
		uc.CheckAndReName()
		t.Log(uc.Name)
		if tt.res != uc.Name {
			t.Errorf("期望值:%s;获得值:%s", tt.res, uc.Name)
		}
	}
}

func TestCatalogGetPids(t *testing.T) {
	tests := []struct {
		id int64
	}{
		{9},
		{14},
		{16},
		{18},
	}
	for _, tt := range tests {
		uc := new(UserCatalog)
		uc.Id = tt.id
		uc.Read()
		res := uc.GetPids()
		t.Log(res)
	}
}

func TestCatalogGetPath(t *testing.T) {
	tests := []struct {
		id int64
	}{
		{9},
		{10},
		{11},
		{12},
	}
	for _, tt := range tests {
		uc := new(UserCatalog)
		uc.Id = tt.id
		uc.Read()
		res := uc.GetPath()
		t.Log(res)
	}
}

func TestCatalogGetSids(t *testing.T) {
	tests := []struct {
		id int64
	}{
		{9},
		{14},
	}
	for _, tt := range tests {
		uc := new(UserCatalog)
		uc.Id = tt.id
		uc.Read()
		res := uc.GetSids()
		t.Log(res)
	}
}

// func TestCatalogDisable(t *testing.T) {
// 	tests := []struct {
// 		id     int64
// 		dissub bool
// 	}{
// 		{9, true},
// 		{14, true},
// 	}
// 	for _, tt := range tests {
// 		uc := new(UserCatalog)
// 		uc.Id = tt.id
// 		uc.Read()
// 		res := uc.Remove(tt.dissub)
// 		t.Log(res)
// 	}
// }

// func TestCatalogEnable(t *testing.T) {
// 	tests := []struct {
// 		id int64
// 	}{
// 		{9},
// 		{19},
// 		{16},
// 		{20},
// 	}
// 	for _, tt := range tests {
// 		uc := new(UserCatalog)
// 		uc.Id = tt.id
// 		uc.Read()
// 		res := uc.Recovery()
// 		t.Log(res)
// 	}
// }

func TestUserGetRecycleBin(t *testing.T) {
	tests := []struct {
		id      int64
		disable bool
		pid     int64
	}{
		{1, true, -1},
		{1, true, 0},
		{1, false, -1},
		{1, false, 15},
	}
	for _, tt := range tests {
		u := new(SysUser)
		u.Id = tt.id
		res, err := u.GetUserCatalogs(tt.disable, tt.pid)
		if err != nil {
			t.Error(err)
		} else {
			t.Log("已删除:", tt.disable, "根目录:", tt.pid)
			for _, uc := range res {
				t.Log(uc)
			}
		}
	}
}

func TestCatalogRead(t *testing.T) {
	tests := []struct {
		id int64
	}{
		{9},
	}
	for _, tt := range tests {
		uc := new(UserCatalog)
		uc.Id = tt.id
		err := uc.Read()
		if err != nil {
			t.Error(err.Error())
		} else {
			t.Log(uc)
		}
	}
}

func TestShareListLoadRelated(t *testing.T) {
	tests := []struct {
		id int64
	}{
		{4},
	}
	for _, tt := range tests {
		s := new(ShareList)
		s.Id = tt.id
		s.Read(true)
		t.Log(s)
	}
}
*/
func TestReadOrCreateCatalog(t *testing.T) {
	u := new(SysUser)
	u.Name = "admin"
	key, e := u.AccessKey("Name")
	t.Log(e, key, u)
}
