package models

import (
	"reflect"
	"time"
)

const (
	_version           = "1.0.2101"        //版本号
	_cfgPath           = "conf/app.conf"   //配置文件路径
	_sqlitePath        = "data/micdisk.db" //SQLite3数据库路径
	_dfsHost           = "http://127.0.0.1:8080"
	_dfgGroup          = "group1"
	_allUserGroupName  = "allusers" //所有人用户组名称
	_userDefaultSpance = 100        //新用户默认空间(MB)
	_md5salt           = "61674a573ec131ded5e6c3b4e673720f"
)

//云盘的配置信息
var DISKCFG = config{
	Version: _version,
}

type config struct { //配置结构体
	Version       string //版本号
	DfsHost       string //分布式文件服务器路径
	DfsDomain     string //分布式文件服务器下载路径
	DfsGroup      string //默认组
	DfsHashType   string //哈希类型
	DfsRebootPath string //重启文件服务器的路径
	DbType        string //数据库类型
	DbHost        string //数据库连接地址
	EnableHttps   bool   //开启https
	Debug         bool   //调试模式
	DfsErr        error  //分布式文件服务故障
}

//用户表
type SysUser struct {
	Id           int64          `orm:"auto"`                //主键自增ID
	Name         string         `orm:"null;size(50)"`       //账号名
	Username     string         `orm:"null;size(50)"`       //用户名
	Password     string         `orm:"null;size(50)"`       //密码
	MaxSpace     int64          `orm:"default(10)"`         //最大空间(MB),0无限制
	CreateTime   time.Time      `orm:"null;type(datetime)"` //创建时间
	UserCatalogs []*UserCatalog `orm:"reverse(many)"`       //设置反向一对多关系
	Logs         []*SysLog      `orm:"reverse(many)"`       //设置反向一对多关系
	Groups       []*SysGroup    `orm:"reverse(many)"`       //设置反向多对多关系
	//Roles        []*SysRole     `orm:"reverse(many)"`       //设置反向多对多关系
	Shares       []*ShareList   `orm:"reverse(many)"` //设置反向多对多关系
	DownloadLogs []*DownloadLog `orm:"reverse(many)"` //设置反向一对多关系
}

//用户组
type SysGroup struct {
	Id       int64        `orm:"auto"`                               //主键自增ID
	Pid      int64        `orm:"null;default(0)"`                    //父节点
	Name     string       `orm:"null;size(50)"`                      //用户组名称
	Remarkes string       `orm:"null;size(255)"`                     //说明
	Users    []*SysUser   `orm:"rel(m2m);rel_table(sys_user_group)"` //多对多关系
	Shares   []*ShareList `orm:"reverse(many)"`                      //设置反向多对多关系
}

//文件索引表
type DfsFileIndex struct {
	Id           int64          `orm:"auto"`           //主键自增ID
	Domain       string         `orm:"null;size(255)"` //域名
	Path         string         `orm:"null;size(255)"` //存储路径
	Url          string         `orm:"null;size(255)"` //存储全路径,Domain+path
	Md5          string         `orm:"size(50);index"` //文件的MD5摘要
	Size         int64          `orm:"null"`           //文件大小,字节
	ModTime      int64          `orm:"null"`           //创建时间,UTC秒
	Scene        string         `orm:"null;size(255)"` //场景
	Token        string         `orm:"null;size(50)"`  //文件的下载 token
	UserCatalogs []*UserCatalog `orm:"reverse(many)"`  //设置反向一对多关系
}

//用户目录
type UserCatalog struct {
	Id           int64          `orm:"auto"`                         //主键自增ID
	User         *SysUser       `orm:"rel(fk)"`                      //用户ID
	UploadUser   *SysUser       `orm:"null;rel(fk)"`                 //文件上传者用户ID
	Pid          int64          `orm:"null;default(0)"`              //父节点ID
	Name         string         `orm:"null;size(50)"`                //名称
	Description  string         `orm:"null;size(255)"`               //说明
	IsDir        bool           `orm:"null"`                         //是否文件夹
	FlashTrans   bool           `orm:"null"`                         //是否秒传文件(IsDir=false时有效)
	Disabled     bool           `orm:"null"`                         //是否已经删除
	File         *DfsFileIndex  `orm:"null;rel(fk)"`                 //文件ID(IsDir=false时有效)
	CreateTime   time.Time      `orm:"type(datetime)"`               //创建时间
	UpdateTime   time.Time      `orm:"null;auto_now;type(datetime)"` //更新时间
	DisableTime  time.Time      `orm:"null;type(datetime)"`          //删除时间
	ShareLists   []*ShareList   `orm:"reverse(many)"`                //设置反向一对多关系
	DownloadLogs []*DownloadLog `orm:"reverse(many)"`                //设置反向一对多关系
}

//分享列表
type ShareList struct {
	Id              int64        `orm:"auto"`                         //主键自增ID
	Catalog         *UserCatalog `orm:"rel(fk)"`                      //被分享的用户目录ID
	ShareBegineTime time.Time    `orm:"type(datetime)"`               //开始分享时间
	TermOfValidity  int64        `orm:"null;default(0)"`              //有效期(秒),0为长期有效
	ShareCode       string       `orm:"null"`                         //分享码(公开分享时需要分享码,为Catalog的md5)
	CanDownload     bool         `orm:"null;default(1)"`              //可以下载,0否,1是
	CanUpload       bool         `orm:"null;default(0)"`              //可以上传(针对文件夹),0否,1是
	CreateTime      time.Time    `orm:"type(datetime)"`               //创建时间
	UpdateTime      time.Time    `orm:"null;auto_now;type(datetime)"` //创建时间
	Users           []*SysUser   `orm:"rel(m2m)"`                     //多对多关系
	Groups          []*SysGroup  `orm:"rel(m2m)"`                     //多对多关系
}

//系统日志
type SysLog struct {
	Id          int64     `orm:"auto"`                //主键自增ID
	User        *SysUser  `orm:"null;rel(fk)"`        //用户ID
	Action      string    `orm:"null;size(50)"`       //动作
	Description string    `orm:"null;size(255)"`      //描述
	OprType     string    `orm:"null;size(20)"`       //操作类型
	MsgType     string    `orm:"null;size(20)"`       //信息类型
	Host        string    `orm:"null;size(50)"`       //请求类名
	RemoteIp    string    `orm:"null;size(50)"`       //请求IP
	ReqUrl      string    `orm:"null;size(50)"`       //URI
	ReqMethod   string    `orm:"null;size(50)"`       //请求方式
	ReqParams   string    `orm:"null;size(255)"`      //提交参数
	CreateTime  time.Time `orm:"null;type(datetime)"` //开始分享的时间
}

type DownloadLog struct {
	Id         int64        `orm:"auto"`                //主键自增ID
	User       *SysUser     `orm:"rel(fk)"`             //下载文件的用户
	Catalog    *UserCatalog `orm:"rel(fk)"`             //被下载的文件
	Download   int64        `orm:"default(1)"`          //模式:1=下载,0=预览
	CreateTime time.Time    `orm:"null;type(datetime)"` //下载时间
}

/****************************************************
功能：判断元素在数组、Map中是否存在
输入：元素、数组或者Map、Slice
输出：存在输出true，不存在输出false
说明：对于数组、Slice，判断的是值是否存在，对于Map，判断的是Key是否存在
时间：2019年12月15日
编辑：wang_jp
****************************************************/
func IsExistItem(obj interface{}, target interface{}) bool {
	targetValue := reflect.ValueOf(target)
	switch reflect.TypeOf(target).Kind() {
	case reflect.Slice, reflect.Array:
		for i := 0; i < targetValue.Len(); i++ {
			if targetValue.Index(i).Interface() == obj {
				return true
			}
		}
	case reflect.Map:
		if targetValue.MapIndex(reflect.ValueOf(obj)).IsValid() {
			return true
		}
	}
	return false
}
