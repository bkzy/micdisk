package models

import (
	"fmt"
	"os"
	"time"

	"github.com/astaxie/beego/logs"
	"gopkg.in/ini.v1"
)

func readCfg() {
	cfg, err := ini.Load(_cfgPath)
	if err != nil {
		logs.Emergency("Fail to read Config file[读取配置文件失败]: %v", err)
		fmt.Printf("The program will closed after 20 second[程序将在20秒后关闭]:")
		for i := 20; i > 0; i-- {
			time.Sleep(time.Second)
			fmt.Printf(".")
		}
		os.Exit(1)
	}
	micdisk, err := cfg.GetSection("diskcfg")
	if err != nil {
		DISKCFG.DbType = "sqlite3"
		DISKCFG.DbHost = _sqlitePath
		DISKCFG.DfsHost = _dfsHost
		DISKCFG.DfsGroup = _dfgGroup
	} else {
		DISKCFG.DbType = micdisk.Key("dbtype").String()
		switch DISKCFG.DbType {
		case "sqlite3":
			DISKCFG.DbHost = micdisk.Key("dbhost").String()
		case "mysql":
			DISKCFG.DbHost = fmt.Sprintf("%s:%s@tcp(%s:%s)/%s?charset=utf8&loc=Local", //mysql地址
				micdisk.Key("dbuser").String(),
				micdisk.Key("dbpswd").String(),
				micdisk.Key("dbhost").String(),
				micdisk.Key("dbport").String(),
				micdisk.Key("dbname").String())
		}
		DISKCFG.DfsHost = micdisk.Key("dfshost").String()
		DISKCFG.DfsGroup = micdisk.Key("dfsgroup").String()
		DISKCFG.DfsRebootPath = micdisk.Key("dfsrebootpath").String()
		DISKCFG.Debug, _ = micdisk.Key("debug").Bool()
	}
}
