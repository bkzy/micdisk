package routers

import (
	"micdisk/controllers"

	"github.com/astaxie/beego"
)

func init() {
	beego.Router("/", &controllers.MainController{})
	beego.Router("/index/", &controllers.MainController{})
	beego.Router("/api/config/", &controllers.MainController{}, "*:ApiGetConfig")
	beego.Router("/api/dfscfg/", &controllers.MainController{}, "*:ApiGetDfsConfig")
	beego.Router("/api/getdomain/", &controllers.MainController{}, "*:GetDfsDomain")

	beego.Router("/api/fastdfs/list_dir/", &controllers.FastDfs{}, "*:GetListDir")
	beego.Router("/api/fastdfs/get_file_info/", &controllers.FastDfs{}, "*:GetFileInfo")
	beego.Router("/api/fastdfs/delete/", &controllers.FastDfs{}, "*:DeleteFile")
	beego.Router("/api/fastdfs/stat/", &controllers.FastDfs{}, "*:GetStat")
	beego.Router("/api/fastdfs/config/", &controllers.FastDfs{}, "*:GetGloablConfig")

	beego.Router("/api/fileindex/new/", &controllers.FileIndexController{}, "*:NewFileIndex")
	beego.Router("/api/fileindex/checktoken/", &controllers.FileIndexController{}, "*:CheckToken")
	beego.Router("/api/fileindex/checktoken/hook/", &controllers.FileIndexController{}, "*:DfsWebHook")

	beego.Router("/api/catalog/newfile/", &controllers.CatalogController{}, "*:NewFile")
	beego.Router("/api/catalog/newfolder/", &controllers.CatalogController{}, "*:NewFolder")
	beego.Router("/api/catalog/remove/", &controllers.CatalogController{}, "*:Remove")
	beego.Router("/api/catalog/recovery/", &controllers.CatalogController{}, "*:Recovery")
	beego.Router("/api/catalog/delete/", &controllers.CatalogController{}, "POST:Delete")
	beego.Router("/api/catalog/clearrecyclebin/", &controllers.CatalogController{}, "POST:ClearRecycleBin")
	beego.Router("/api/catalog/getlist/", &controllers.CatalogController{}, "*:GetList")
	beego.Router("/api/catalog/stat/", &controllers.CatalogController{}, "*:GetStat")
	beego.Router("/api/catalog/getpath/", &controllers.CatalogController{}, "*:GetPath")
	beego.Router("/api/catalog/rename/", &controllers.CatalogController{}, "*:ReName")
	beego.Router("/api/catalog/updatedesc/", &controllers.CatalogController{}, "*:UpdateDesc")
	beego.Router("/api/catalog/search/", &controllers.CatalogController{}, "*:Search")
	beego.Router("/api/catalog/getdownloadlog/", &controllers.CatalogController{}, "*:GetDownloadLog")
	beego.Router("/api/catalog/catalogs/", &controllers.CatalogController{}, "*:GetCatelogs")
	beego.Router("/api/catalog/moveto/", &controllers.CatalogController{}, "*:MoveTo")
	beego.Router("/api/catalog/copyto/", &controllers.CatalogController{}, "*:CopyTo")
	beego.Router("/api/catalog/reloadname/", &controllers.CatalogController{}, "*:ReloadCatalogFileName")

	beego.Router("/api/downloadlog/new/", &controllers.DownloadlogController{}, "*:NewDownloadLog")

	beego.Router("/api/share/new/", &controllers.ShareController{}, "POST:NewShare")
	beego.Router("/api/share/remove/", &controllers.ShareController{}, "*:RemoveShare")
	beego.Router("/api/share/update/", &controllers.ShareController{}, "*:UpdateShare")
	beego.Router("/api/share/myshare/", &controllers.ShareController{}, "*:GetMySharedLists")
	beego.Router("/api/share/withme/", &controllers.ShareController{}, "*:GetSharedToMeLists")

	beego.Router("/api/usergroup/new/", &controllers.UserGroupController{}, "*:NewUserGroup")
	beego.Router("/api/usergroup/update/", &controllers.UserGroupController{}, "*:UpdateUserGroup")
	beego.Router("/api/usergroup/checkname/", &controllers.UserGroupController{}, "*:CheckName")
	beego.Router("/api/usergroup/getgrouplist/", &controllers.UserGroupController{}, "*:GetGroupList")
	beego.Router("/api/usergroup/delete/", &controllers.UserGroupController{}, "*:Delete")

	beego.Router("/api/user/checkname/", &controllers.UserController{}, "*:CheckUserName")
	beego.Router("/api/user/getusermsg/", &controllers.UserController{}, "*:GetUserMsg")
	beego.Router("/api/user/getuserlist/", &controllers.UserController{}, "*:GetUserList")
	beego.Router("/api/user/updatepswd/", &controllers.UserController{}, "POST:UpdatePswd")
	beego.Router("/api/user/updateusername/", &controllers.UserController{}, "POST:UpdateUsername")

	beego.Router("/login/", &controllers.UserController{})
	beego.Router("/register/", &controllers.UserController{}, "GET:PageRegister")
	beego.Router("/register/", &controllers.UserController{}, "POST:Register")
	beego.Router("/logout/", &controllers.UserController{}, "*:ApiLogOut")
}
