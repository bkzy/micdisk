//JavaScript代码区域
var DIR_NODES={};//当前文件夹项目列表
var LAST_NODE=null;//上一个层级的文件夹
var DIR_ID=0;//当前文件夹的ID
var DIR_PID=0;//当前文件夹的父级文件夹
var DIR_NAME='';//当前文件夹的名称
var DIR_ITEM_CNT=0;//当前文件夹下的项目数
var GROUP_LIST=[];//用户组列表
var USER_LIST=[];//用户列表
var USER_MSG={};//登录用户的基本信息
var DFS_DOMAIN='';//文件服务器的域名,http://ip:port
var DFS_GROUP='';//文件服务器的组名
var DIR_SHARE={};//共享文件列表
var DIR_SHARE_RAW={};//获取到的原始共享数据
var DIR_NODES_RAW={};//获取到的原始列表数据
var MAIN_PATH='_MYDISK';//{'_MYDISK':我的网盘,'_MYRECYCLEBINE':我的回收站,'_MYSHARED':我的分享,'_SHAREDWITHME':分享给我的}
var SHARE_ROOT_DIR_ID=0;//点击共享文件下的文件夹后记录的第一层被点击的文件夹的ID
var SHARE_ROOT_ID=-1;//点击共享文件下的文件夹后记录的被共享文件夹的共享节点ID
var VIEW_TYPE=0;//0图标模式,1列表模式
var SELECTED_DIRTREE_ID=0;//从树种选中的项目ID

$(document).ready(function(){
    RequestUserMsg();
    RequestDirList(0,false);
    RequestDirStat(0,true);
    RequestGroups();
    RequestUsers();
    RequestDfsDomain();
    OnViewChange(-1);
});

//我的网盘
function OnMyDisk(){
    layer.closeAll();
    MAIN_PATH='_MYDISK';
    SHARE_ROOT_DIR_ID=0;
    SHARE_ROOT_ID=-1;
    RequestDirList(0,false);
    $("#myrecyclebinmune").attr("class","layui-hide");
    $("#sharewithmemenu").attr("class","layui-hide");
    $("#mydiskmenu").attr("class","layui-show");
}
//我的回收站
function OnMyRecyclebin(){
    layer.closeAll();
    MAIN_PATH='_MYRECYCLEBINE';
    SHARE_ROOT_DIR_ID=0;
    SHARE_ROOT_ID=-1;
    RequestDirList(-1,true);
    $("#myrecyclebinmune").attr("class","layui-show");
    $("#sharewithmemenu").attr("class","layui-hide");
    $("#mydiskmenu").attr("class","layui-hide");
}

//我的分享
function OnMyShared(){
    layer.closeAll();
    MAIN_PATH='_MYSHARED';
    SHARE_ROOT_DIR_ID=0;
    SHARE_ROOT_ID=0;
    RequestMyShare();
    DecodeDirPath({0:'myshare'});
    
    $("#myrecyclebinmune").attr("class","layui-hide");
    $("#sharewithmemenu").attr("class","layui-hide");
    $("#mydiskmenu").attr("class","layui-hide");
}
//分享给我的
function OnSharedWithMe(){
    layer.closeAll();
    MAIN_PATH='_SHAREDWITHME';
    SHARE_ROOT_DIR_ID=0;
    SHARE_ROOT_ID=0;
	RequestShareWithMe();
    DecodeDirPath({0:'sharedwithme'});

    $("#myrecyclebinmune").attr("class","layui-hide");
    $("#sharewithmemenu").attr("class","layui-hide");
    $("#mydiskmenu").attr("class","layui-hide");
}


function OnViewChange(islist){//-1读取cookie,0图标模式,1列表模式
    if (islist <0){
        VIEW_TYPE = $.cookie('view_type');
        if (VIEW_TYPE==undefined){
            VIEW_TYPE = 'icon';
            $.cookie('view_type',VIEW_TYPE,{expires:30});
        }
    }else{
        //0图标模式,1列表模式
        if (islist==1){
            VIEW_TYPE='list'
        }else{
            VIEW_TYPE='icon'
        }
        $.cookie('view_type',VIEW_TYPE,{expires:30});
    }
    if (VIEW_TYPE=='icon'){
        $("#view_icon").attr("class","layui-btn layui-btn-normal");
        $("#view_list").attr("class","layui-btn layui-btn-primary");
    }else{
        $("#view_icon").attr("class","layui-btn layui-btn-primary");
        $("#view_list").attr("class","layui-btn layui-btn-normal");
    }
    switch(MAIN_PATH){
        case '_MYSHARED':
        case'_SHAREDWITHME':
            DecodeDirList(DIR_SHARE_RAW);
            break;
        case '_MYDISK':
        case '_MYRECYCLEBINE':
            DecodeDirList(DIR_NODES_RAW);
            break;
    }
}

//获取文件夹下的项目列表
function RequestDirList(id,removed){
    if(id<0){
        DIR_ID=0
    }else{
        DIR_ID=id;
    }
    if(id>0){
        LAST_NODE=DIR_NODES[id];
        $("#renamefolder").attr("class","");
        $("#deletefolder").attr("class","");
        $("#sharefolder").attr("class","")
    }else{
        LAST_NODE=null;
        $("#renamefolder").attr("class","layui-hide");
        $("#deletefolder").attr("class","layui-hide");
        $("#sharefolder").attr("class","layui-hide")
    }
    if(MAIN_PATH=='_MYSHARED' || MAIN_PATH=='_SHAREDWITHME'){
        if (SHARE_ROOT_DIR_ID==0){
            SHARE_ROOT_DIR_ID=id;
        }
    }

    $.ajax({
        url:'api/catalog/getlist',
        data:{id:id,removed:removed},
        dataType:'json',
        type:'post',
        success:function (data) {
            if (data.status == 'ok'){
                DIR_NODES_RAW=data.data;
                DecodeDirList(data.data);
                RequestDirPath(id);
            }else{
                console.log(data.message);
                if(data.status == 'fail-0'){
                    location.reload();
                }else{
                    layer.msg(data.message);
                }
            }
        }
    })
}
//获取文件夹的路径
function RequestDirPath(id){
    if(id<0){
        id=0;
    }
        $.ajax({
            url:'api/catalog/getpath',
            data:{id:id},
            dataType:'json',
            type:'post',
            success:function (data) {
                if (data.status == 'ok'){
                    DecodeDirPath(data.data);
                    RequestDirStat(id);
                }else{
                    if(data.status == 'fail-0'){
                        location.reload();
                    }else{
                        layer.msg(data.message);
                    }
                }
            }
        })
}
//解析文件夹路径
function DecodeDirPath(data){
    var prev='',htmlmsg='',removed=0; 
    var have_get_root=true;
    switch(MAIN_PATH){
        case '_MYDISK'://我的网盘
            htmlmsg='<a href="javascript:void(0)" onclick="RequestDirList(0,false)">我的网盘</a>';
            have_get_root=true;
            break;
        case '_MYRECYCLEBINE'://我的回收站
            htmlmsg='<a href="javascript:void(0)" onclick="OnMyRecyclebin()">我的回收站</a>';
            removed=1;
            have_get_root=true;
            break;
        case '_MYSHARED'://我的共享
            htmlmsg='<a href="javascript:void(0)" onclick="OnMyShared()">我分享的</a>';
            have_get_root=false;
            break;
        case '_SHAREDWITHME'://共享给我的
            htmlmsg='<a href="javascript:void(0)" onclick="OnSharedWithMe()">分享给我的</a>';
            have_get_root=false;
            break;
    }

    if (SHARE_ROOT_DIR_ID==0){
        have_get_root=true;
    }
    for (let id in data) {//
        if(id!=DIR_ID){//不是当前id
            DIR_PID=id;//赋值给父id
        }
        if(MAIN_PATH=='_MYSHARED' || MAIN_PATH=='_SHAREDWITHME'){
            if (SHARE_ROOT_DIR_ID==id){//是当前共享文件的根ID
                have_get_root=true;
            }
            if (have_get_root==false || SHARE_ROOT_DIR_ID==id){//根ID之前的节点都不能有父ID
                DIR_PID=0;
            }
        }
        if (id>0 && have_get_root){
            htmlmsg+='>><a href="javascript:void(0)" onclick="RequestDirList('+id+','+removed+')">'+data[id]+'</a>';
            DIR_NAME=data[id];
        }
    }
    if (DIR_PID>0 && SHARE_ROOT_DIR_ID>0){
        prev='<a href="javascript:void(0)" onclick="RequestDirList('+DIR_PID+','+removed+')">上一级</a>||';
    }
    $("#dirpath").html(prev+htmlmsg);
}
//获取文件夹的统计信息
function RequestDirStat(id,sys){
    $.ajax({
        url:'api/catalog/stat',
        data:{id:id},
        dataType:'json',
        type:'post',
        success:function (data) {
            if (data.status == 'ok'){
                DecodeDirStat(data.data,sys);
            }else{
                if(data.status == 'fail-0'){
                    location.reload();
                }else{
                    layer.msg(data.message);
                }
            }
        }
    })
}
//解析文件夹统计信息
function DecodeDirStat(data,sys){
    var htmlmsg='';
    var unit='MB';
    var tsize=data["FileSize"]/(1024*1024);
    if (tsize>1024){
        tsize/=1024;
        unit='GB';
    }
    tsize=tsize.toFixed(2);
    if(sys==true){
        htmlmsg='<div style="padding-left: 15px;font-size:15px;">';
        htmlmsg+='<dd>文件和文件夹:</dd>';
        htmlmsg+='<dd style="padding-left: 15px;">文件夹:'+data["DirCnt"]+'</dd>';
        htmlmsg+='<dd style="padding-left: 15px;">文件:'+data["FileCnt"]+'</dd>';
        htmlmsg+='<dd style="padding-left: 15px;">大小:'+tsize+unit+'</dd>';

        htmlmsg+='<dd><h3>回收站:</h3></dd>';
        unit='MB';
        tsize=data["RecycleBinSize"]/(1024*1024);
        if (tsize>1024){
            tsize/=1024;
            unit='GB';
        }
        tsize=tsize.toFixed(2);
        htmlmsg+='<dd style="padding-left: 15px;">文件夹:'+data["RecycleBinDirCnt"]+'</dd>';
        htmlmsg+='<dd style="padding-left: 15px;">文件:'+data["RecycleBinFileCnt"]+'</dd>';
        htmlmsg+='<dd style="padding-left: 15px;">大小:'+tsize+unit+'</dd>';

        htmlmsg+='<dd><h3>合计</h3></dd>';
        unit='MB';
        tsize=data["TotalSize"]/(1024*1024);
        if (tsize>1024){
            tsize/=1024;
            unit='GB';
        }
        tsize=tsize.toFixed(2);
        htmlmsg+='<dd style="padding-left: 15px;">文件夹:'+data["TotalDirCnt"]+'</dd>';
        htmlmsg+='<dd style="padding-left: 15px;">文件:'+data["TotalFileCnt"]+'</dd>';
        htmlmsg+='<dd style="padding-left: 15px;">大小:'+tsize+unit+'</dd>';
        htmlmsg+='</div>';
        $("#diskstat").html(htmlmsg);
    }else{
        htmlmsg=$("#footer").text();
        htmlmsg+=', 总大小'+tsize+unit;
        $("#footer").text(htmlmsg);
    }
}

function DecodeDirList(inputdata){
    for (let nm in DIR_NODES) {//清空键值对
        delete (DIR_NODES[nm]);
    }

    var data=[];
    var htmlmsg=GetViewTitle(VIEW_TYPE,MAIN_PATH);
    var dircnt=0;
    var filecnt=0;
    var filesize=0.0;
    if(SHARE_ROOT_ID==0){
        var tmpnod={},n=-1;
        for(i=0;i<inputdata.length;i++){
            var id=inputdata[i].Catalog.Id;
            if(tmpnod.hasOwnProperty(id)==false){
                n+=1;
                tmpnod[id]=n;
                data.push(inputdata[i]);
            }else{
                if(inputdata[i].CanUpload){
                    data[tmpnod[id]].CanUpload=true;
                }

                if(inputdata[i].CanDownload){
                    data[tmpnod[id]].CanDownload=true;
                }
            }
        }
    }else{
        data=inputdata;
    }
    for(j = 0,len=data.length; j < len; j++) {
        var item=data[j];
        if(SHARE_ROOT_ID==0){
            item=data[j].Catalog;
        }
        var node= new Object;
        node.Id=item.Id;
        node.Pid=item.Pid;
        node.Name=item.Name;
        node.Desc=item.Description;
        node.IsDir=item.IsDir;
        node.CreateTime = item.CreateTime;
        node.DisableTime=item.DisableTime;
        node.User=item.User;
        node.UploadUser=item.UploadUser;
        if (item.File!=null){
            filecnt+=1;
            filesize+=item.File.Size;
            node.Size=item.File.Size;
            node.FlashTrans=item.FlashTrans;
            node.Domain=item.File.Domain;
            node.Path=item.File.Path;
            node.Md5=item.File.Md5;
            node.Token=item.File.Token;
        }else{
            dircnt+=1;
            node.FlashTrans='';
            node.Domain='';
            node.Path='';
            node.Md5='';
            node.Size='';
        }
        if(item.ShareLists!=null){
            if(item.ShareLists.length>0)
                node.HasShared=true;
        }else{
            node.HasShared=false;
        }
        DIR_NODES[node.Id]=node;
        htmlmsg+=GetViewBody(VIEW_TYPE,MAIN_PATH,data[j]);
        //htmlmsg+=ShowItem(node);
    }
    if(VIEW_TYPE=='list'){
        htmlmsg+='</tbody></table>';
    }
    $("#master").html(htmlmsg);
    filesize/=1024;
    filesize/=1024;
    unit='MB';
    if(filesize>1024){
        filesize/=1024;
        unit='GB'
    }
    filesize=filesize.toFixed(2);
    DIR_ITEM_CNT=filecnt+dircnt;
    $("#footer").text('共 '+dircnt+'个文件夹, '+filecnt+'个文件; 文件大小 '+filesize+unit);
}

//获取图标
function GetIconImg(isdir,itemname){
    var img='./static/img/';
    if (isdir==true){
        img+='folder.svg'
    }else{
        names=itemname.split(".");
        var etnm=''
        if (names.length>0){
            etnm=names[names.length-1]//获取扩展名
        }
        //console.log(etnm);
        switch(etnm.toLowerCase()){
        case 'exe':
            img+='exe.svg';
            break;
        case 'docx':
        case 'doc':
            img+='word.svg';
            break;
        case 'xls':
        case 'xlsx':
        case 'csv':
            img+='excel.svg';
            break;
        case 'ppt':
        case 'pptx':
            img+='ppt.svg';
            break;
        case 'pdf':
            img+='pdf.svg';
            break;
        case 'txt':
        case 'json':
            img+='txt.svg';
            break;
        case 'ini':
            img+='ini.svg';
            break;
        case 'rar':case 'zip':
            img+='rar.svg';
            break;
        case 'conf':
            img+='conf.svg';
            break;
        case 'jpg':
        case 'jpeg':
        case 'gif':
        case 'svg':
        case 'png':case 'bmp':
            img+='picture.svg';
            break;
        case 'rmvb':case 'wmv':case 'asf':case 'avi':case '3gp':case 'mpg':case 'mkv':case 'mp4':case 'dvd':case 'ogm':case 'mov':case 'mpeg2':case 'mpeg4':
            img+='video.svg';
            break;
        case 'mp3':case 'ogg':case 'wav':case 'ape':case 'cda':case 'au':case 'midi':case 'mac':case 'aac':
            img+='audio.svg';
            break;
        default:
            img+='doc.svg';
            break;
        }
    }
    return img;
}

function GetViewTitle(view_type,main_path){
    var tb='<table class="layui-table" lay-even lay-skin="nob"><thead><tr>';
    var viewdict={
        'icon':{
            '_MYSHARED':''
            ,'_SHAREDWITHME':''
            ,'_MYDISK':''
            ,'_MYRECYCLEBINE':''
        }
        ,'list':{
            '_MYSHARED':tb+'<th>名称</th><th>分享时间</th><th>剩余有效期</th><th>上传者</th><th>可上传</th><th>可下载</th><th>取消</th><th>详情</th></tr></thead><tbody>'
            ,'_SHAREDWITHME':tb+'<th>名称</th><th>分享者</th><th>上传者</th><th>分享时间</th><th>剩余有效期</th><th>可上传</th><th>可下载</th></tr></thead><tbody>'
            ,'_MYDISK':tb+'<th>名称</th><th>创建/上传时间</th><th>上传者</th><th>大小</th><th>已共享</th><th>备注</th></tr></thead><tbody>'
            ,'_MYRECYCLEBINE':tb+'<th>名称</th><th>删除时间</th><th>上传者</th><th>大小</th><th>备注</th></tr></thead><tbody>'
        }
    };
    return viewdict[view_type][main_path];
}

function GetViewBody(view_type,main_path,input){
    var node=input;
    var time={'date':null,'str':null},size='',remainder='';
    var no='<i class="layui-icon layui-icon-close-fill" style="font-size: 20px; color: #FF5722;"></i>';
    var yes='<i class="layui-icon layui-icon-ok-circle" style="font-size: 20px; color: #5FB878;"></i>';
    var canup=no,candown=no;
    var btn_cancel,btn_detailed;
    var shareid=input.Id;
    var hasshared='';
    var style = 'style=""'
    switch(main_path){
    case '_MYSHARED':case'_SHAREDWITHME':
        if(SHARE_ROOT_DIR_ID==0){
            node=input.Catalog;
            time=getDate(input.ShareBegineTime);

            var usedsconed=((new Date()).valueOf()-time.date.valueOf())/1000;
            if (input.TermOfValidity>0){
                remainder=((input.TermOfValidity-usedsconed)/86400).toFixed(0)+'天';
            }else{
                remainder='长期';
            }
            if(input.CanUpload){
                canup=yes;
            }
            if(input.CanDownload){
                candown=yes;
            }
            btn_cancel='<button type="button" class="layui-btn layui-btn-sm layui-btn-danger" onclick="OnDisableShare('+input.Id+')">取消</button>';
            btn_detailed='<button type="button" class="layui-btn layui-btn-sm layui-btn" onclick="OnShareDetailed('+input.Id+')">详情</button>';
        }else{
            shareid=SHARE_ROOT_ID;
            sharenode=DIR_SHARE[shareid];
            time=getDate(sharenode.ShareBegineTime);
            var usedsconed=((new Date()).valueOf()-time.date.valueOf())/1000;
            if (sharenode.TermOfValidity>0){
                remainder=((sharenode.TermOfValidity-usedsconed)/86400).toFixed(0)+'天';
            }else{
                remainder='长期';
            }
            if(sharenode.CanUpload){
                canup=yes;
            }
            if(sharenode.CanDownload){
                candown=yes;
            }
            btn_cancel='--';
            btn_detailed='--';
            time=getDate(node.CreateTime);
        }
        break;
    case '_MYDISK':
        time=getDate(node.CreateTime);
        break;
    case '_MYRECYCLEBINE':
        time=getDate(node.DisableTime);
        break;
    }
    if(node.ShareLists!=null){
        if(node.ShareLists.length>0){
            hasshared=yes;
            style='style="color:#FF5722;"'
        }
    }
    if (node.IsDir==false){
        var fsize=getSizeStr(node.File.Size);
        size=fsize.size+fsize.unit;
        canup='';
    }
    //console.log(node);
    var img='<img src="'+GetIconImg(node.IsDir,node.Name)+'" style="height:30px;"></img>';
    var link={
        '_MYSHARED':'<a href="javascript:void(0)" onclick="OnClickItem('+node.Id+','+shareid+')" ondblclick="OnDbClickItem('+node.Id+','+shareid+')"  title="'+node.Name+'" '+style+'>'+img+node.Name+'</a>'
        ,'_SHAREDWITHME':'<a href="javascript:void(0)" onclick="OnClickItem('+node.Id+','+shareid+')" ondblclick="OnDbClickItem('+node.Id+','+shareid+')"  title="'+node.Name+'" '+style+'>'+img+node.Name+'</a>'
        ,'_MYDISK':'<a href="javascript:void(0)" onclick="OnClickItem('+node.Id+','+shareid+')" ondblclick="OnDbClickItem('+node.Id+','+shareid+')"  title="'+node.Name+'" '+style+'>'+img+node.Name+'</a>'
        ,'_MYRECYCLEBINE':'<a href="javascript:void(0)" onclick="OnClickRecycleBinItem('+node.Id+')" title="'+node.Name+'" '+style+'>'+img+node.Name+'</a>'
    }
    uploader='';
    if(node.UploadUser!=null){
        uploader=node.UploadUser.Username;
    }
    var icopre='<div class="layui-col-md3 layui-elip">';
    var viewdict={
        'icon':{
            '_MYSHARED':icopre+link[main_path]+'</div>'
            ,'_SHAREDWITHME':icopre+link[main_path]+'</div>'
            ,'_MYDISK':icopre+link[main_path]+'</div>'
            ,'_MYRECYCLEBINE':icopre+link[main_path]+'</div>'
        }
        ,'list':{
            '_MYSHARED':'<tr><td>'+link[main_path]+'</td><td>'+time.str+'</td><td>'+remainder+'</td><td>'+uploader+'</td><td>'+canup+'</td><td>'+candown+'</td><td>'+btn_cancel+'</td><td>'+btn_detailed+'</td></tr>'
            ,'_SHAREDWITHME':'<tr><td>'+link[main_path]+'</td><td>'+node.User.Username+'</td><td>'+uploader+'</td><td>'+time.str+'</td><td>'+remainder+'</td><td>'+canup+'</td><td>'+candown+'</td></tr>'
            ,'_MYDISK':'<tr><td>'+link[main_path]+'</td><td>'+time.str+'</td><td>'+uploader+'</td><td>'+size+'</td><td>'+hasshared+'</td><td>'+node.Description+'</td></tr>'
            ,'_MYRECYCLEBINE':'<tr><td>'+link[main_path]+'</td><td>'+time.str+'</td><td>'+uploader+'</td><td>'+size+'</td><td>'+node.Description+'</td></tr>'
        }
    };
    return viewdict[view_type][main_path];
}

//单击项目节点时的响应
function OnClickItem(nodeid,shareid){
    var node=DIR_NODES[nodeid];
    SHARE_ROOT_ID=shareid;
    switch(MAIN_PATH){
        case '_MYSHARED'://我的共享
            if(node.IsDir){
                onClickMySharedDir(nodeid,shareid);
            }else{
                onClickMySharedFile(nodeid,shareid);
            }
            break;
        case '_SHAREDWITHME'://共享给我的
            if(node.IsDir){
                onClickSharedWithMeDir(nodeid,shareid);
            }else{
                onClickSharedWithMeFile(nodeid,shareid);
            }
            break;
        case '_MYDISK'://我的网盘
            if(node.IsDir){
                onClickDir(nodeid);
            }else{
                onClickFile(nodeid,false);
            }
            break;
        case '_MYRECYCLEBINE'://我的回收站
            OnClickRecycleBinItem(nodeid);
            break;
    }
}

//双击项目节点时的响应
function OnDbClickItem(nodeid,shareid){
    var node=DIR_NODES[nodeid];
    SHARE_ROOT_ID=shareid;
    switch(MAIN_PATH){
        case '_SHAREDWITHME'://共享给我的
            if(node.IsDir){
                onDbClickDir(nodeid);
                var sharenode=DIR_SHARE[shareid];
                if (sharenode.CanUpload)
                    $("#sharewithmemenu").attr("class","layui-show");
            }
            break;
        case '_MYSHARED'://我的共享
        case '_MYDISK'://我的网盘
            if(node.IsDir){
                onDbClickDir(nodeid);
            }
            break;
        case '_MYRECYCLEBINE'://我的回收站
            break;
    }
}

//响应单击我的共享中的文件节点
function onClickMySharedFile(nodeid,shareid){
    if(SHARE_ROOT_DIR_ID==0){//我的分享根目录
        var index=layer.msg("请选择您要执行的操作!", {
            time: 10000 //10s后自动关闭
            ,btnAlign: 'c'
            ,closeBtn: 1
            ,anim:Math.round(Math.random()*5) //弹出动画:0默认,1从上掉落,2从最底部往上滑入,3从左滑入,4从左翻滚,5渐显,6抖动
            ,title:'文件操作'
            ,btn: ['下载', '预览','取消共享','分享详情','下载记录','属性']
            ,yes: function(){ // 下载
                layer.close(index);
                RequestDownload(nodeid,1);
            },
            btn2: function(){ //预览
                layer.close(index);
                RequestDownload(nodeid,0);
            },
            btn3: function(){ // 取消共享
                layer.close(index);
                OnDisableShare(shareid);
            },
            btn4: function(){ //共享详情
                layer.close(index);
                OnShareDetailed(shareid);
            },
            btn5: function(){ //下载记录
                layer.close(index);
                OnViewDownloadlogs(nodeid);
            },
            btn6: function(){ //属性
                layer.close(index);
                OnViewOption(nodeid);
            },
        });
    }else{//我的分享子目录
        var index=layer.msg("请选择您要执行的操作!", {
            time: 10000 //10s后自动关闭
            ,btnAlign: 'c'
            ,closeBtn: 1
            ,anim:Math.round(Math.random()*5) //弹出动画:0默认,1从上掉落,2从最底部往上滑入,3从左滑入,4从左翻滚,5渐显,6抖动
            ,title:'文件操作'
            ,btn: ['下载', '预览','下载记录','属性']
            ,yes: function(){ // 下载
                layer.close(index);
                RequestDownload(nodeid,1);
            },
            btn2: function(){ //预览
                layer.close(index);
                RequestDownload(nodeid,0);
            },
            btn3: function(){ //下载记录
                layer.close(index);
                OnViewDownloadlogs(nodeid);
            },
            btn4: function(){ //属性
                layer.close(index);
                OnViewOption(nodeid);
            },
        });
    }
}
var sleeptime=null;
//响应单击我的共享中的文件夹节点
function onClickMySharedDir(nodeid,shareid){
    clearTimeout(sleeptime);  //首先清除计时器
    sleeptime = setTimeout(() => {
        if(SHARE_ROOT_DIR_ID==0){//我的分享根目录
            var index=layer.msg("请选择您要执行的操作!", {
                time: 10000 //10s后自动关闭
                ,btnAlign: 'c'
                ,closeBtn: 1
                ,anim:Math.round(Math.random()*5) //弹出动画:0默认,1从上掉落,2从最底部往上滑入,3从左滑入,4从左翻滚,5渐显,6抖动
                ,title:'文件夹操作'
                ,btn: ['打开','取消共享','分享详情','属性']
                ,yes: function(){ // 打开
                    layer.close(index);
                    RequestDirList(nodeid,false);
                },
                btn2: function(){ //取消共享
                    layer.close(index);
                    OnDisableShare(shareid);
                },
                btn3: function(){ //共享详情
                    layer.close(index);
                    OnShareDetailed(shareid);
                },
                btn4: function(){ //属性
                    layer.close(index);
                    OnViewOption(nodeid);
                },
            });
        }else{
            var index=layer.msg("请选择您要执行的操作!", {
                time: 10000 //10s后自动关闭
                ,btnAlign: 'c'
                ,closeBtn: 1
                ,anim:Math.round(Math.random()*5) //弹出动画:0默认,1从上掉落,2从最底部往上滑入,3从左滑入,4从左翻滚,5渐显,6抖动
                ,title:'文件夹操作'
                ,btn: ['打开','属性']
                ,yes: function(){ // 打开
                    layer.close(index);
                    RequestDirList(nodeid,false);
                },
                btn2: function(){ //属性
                    layer.close(index);
                    OnViewOption(nodeid);
                },
            });
        }
    },300);
}

//响应单击共享给我的文件节点
function onClickSharedWithMeFile(nodeid,shareid){
    var sharenode=DIR_SHARE[shareid];
    var dir_node=DIR_NODES[nodeid];
    var index=layer.msg("请选择您要执行的操作!", {
        time: 10000 //10s后自动关闭
        ,btnAlign: 'c'
        ,closeBtn: 1
        ,anim:Math.round(Math.random()*5) //弹出动画:0默认,1从上掉落,2从最底部往上滑入,3从左滑入,4从左翻滚,5渐显,6抖动
        ,title:'文件操作'
        ,btn: ['下载', '预览','重命名','删除','下载记录','属性']
        ,yes: function(){ // 下载
            layer.close(index);
            if(sharenode.CanDownload || dir_node.UploadUser.Id==USER_MSG.Id){
                RequestDownload(nodeid,1);
            }else{
                layer.msg('您没有下载权限!');
            }
        },
        btn2: function(){ //预览
            layer.close(index);
            if(sharenode.CanDownload || dir_node.UploadUser.Id==USER_MSG.Id){
                RequestDownload(nodeid,0);
            }else{
                layer.msg('您没有下载(预览)权限!');
            }
        },
        btn3: function(){ //重命名
            layer.close(index);
            RenameFile(nodeid);
        },
        btn4: function(){ //删除
            layer.close(index);
            RemoveFile(nodeid);
        },
        btn5: function(){ //下载记录
            layer.close(index);
            if(sharenode.CanDownload || dir_node.UploadUser.Id==USER_MSG.Id){
                OnViewDownloadlogs(nodeid);
            }else{
                layer.msg('您没有浏览该文件下载记录的权限!');
            }
        },
        btn6: function(){ //属性
                layer.close(index);
                OnViewOption(nodeid);
        },
    });
}

//响应单击共享给我的文件夹节点
function onClickSharedWithMeDir(nodeid,shareid){
    clearTimeout(sleeptime);  //首先清除计时器
    sleeptime = setTimeout(() => {
        var index=layer.msg("请选择您要执行的操作!", {
            time: 10000 //10s后自动关闭
            ,btnAlign: 'c'
            ,closeBtn: 1
            ,anim:Math.round(Math.random()*5) //弹出动画:0默认,1从上掉落,2从最底部往上滑入,3从左滑入,4从左翻滚,5渐显,6抖动
            ,title:'文件夹操作'
            ,btn: ['打开','重命名','删除','属性']
            ,yes: function(){ // 打开
                layer.close(index);
                RequestDirList(nodeid,false);
                var sharenode=DIR_SHARE[shareid];
                if (sharenode.CanUpload)
                    $("#sharewithmemenu").attr("class","layui-show");
            },
            btn2: function(){ //重命名
                layer.close(index);
                RenameFolder(nodeid);
            },
            btn3: function(){ //删除
                RemoveFolder(nodeid);
                layer.close(index);
            },
            btn4: function(){ //属性
                layer.close(index);
                OnViewOption(nodeid);
            },
        });
    },300);
}

//响应单击文档
function onClickFile(id){
    var index=layer.msg("仅部分文件支持在线浏览模式!", {
        time: 10000 //10s后自动关闭
        ,btnAlign: 'c'
        ,closeBtn: 1
        ,anim:Math.round(Math.random()*5) //弹出动画:0默认,1从上掉落,2从最底部往上滑入,3从左滑入,4从左翻滚,5渐显,6抖动
        ,title:'文件操作'
        ,btn: ['下载', '预览','重命名','删除','分享', '移动到','复制到','修改备注','下载记录','属性']
        ,yes: function(){ // 下载
            layer.close(index);
            RequestDownload(id,1)
        },
        btn2: function(){ //预览
            layer.close(index);
            RequestDownload(id,0)
        },
        btn3: function(){ //重命名
            layer.close(index);
            RenameFile(id)
        },
        btn4: function(){ //删除
            layer.close(index);
            RemoveFile(id);
        },
        btn5: function(){ //分享
            layer.close(index);
            OnNewShare(id);
        },
        btn6: function(){ //移动到
            layer.close(index);
            OnGetCatalogsTree([id],false,'moveto');
        },
        btn7: function(){ //复制到
            layer.close(index);
            OnGetCatalogsTree([id],false,'copyto');
        },
        btn8: function(){ //修改备注
            layer.close(index);
            OnUpdateDescription(id);
        },
        btn9: function(){ //下载记录
            layer.close(index);
            OnViewDownloadlogs(id);
        },
        btn10: function(){ //属性
            layer.close(index);
            OnViewOption(id);
        },
      });
}

//响应单击文件夹
function onClickDir(id){
    clearTimeout(sleeptime);  //首先清除计时器
    sleeptime = setTimeout(() => {
        var index=layer.msg("请选择您要执行的操作!", {
            time: 10000 //10s后自动关闭
            ,btnAlign: 'c'
            ,closeBtn: 1
            ,anim:Math.round(Math.random()*5) //弹出动画:0默认,1从上掉落,2从最底部往上滑入,3从左滑入,4从左翻滚,5渐显,6抖动
            ,title:'文件夹操作'
            ,btn: ['打开','重命名','删除','分享','移动到','复制到','修改备注','属性']
            ,yes: function(){ // 打开
                layer.close(index);
                RequestDirList(id,false);
            },
            btn2: function(){ //重命名
                layer.close(index);
                RenameFolder(id);
            },
            btn3: function(){ //删除
                RemoveFolder(id);
                layer.close(index);
            },
            btn4: function(){ //分享
                layer.close(index);
                OnNewShare(id,true);
            },
            btn5: function(){ //移动到
                layer.close(index);
                OnGetCatalogsTree([id],false,'moveto');
            },
            btn6: function(){ //复制到
                layer.close(index);
                OnGetCatalogsTree([id],false,'copyto');
            },
            btn7: function(){ //修改备注
                layer.close(index);
                OnUpdateDescription(id);
            },
            btn8: function(){ //属性
                layer.close(index);
                OnViewOption(id);
            },
        });
    },300);
}

//双击文件夹
function onDbClickDir(id){
    layer.closeAll();
    clearTimeout(sleeptime);  //首先清除计时器
    RequestDirList(id,false);
}

//执行下载
function RequestDownload(id,download){
    var name=DIR_NODES[id].Name;
    //var domain=DIR_NODES[id].Domain;
    var domain = DFS_DOMAIN;
    var path=DIR_NODES[id].Path;
    var token=DIR_NODES[id].Token;
    var now = new Date();
    token=MD5(name+now.getTime()+download+token)
    var url=domain+path+'?name='+name+'&download='+download+'&tstemp='+now.getTime()+'&id='+id+'&auth_token='+token;
    NewDownloadLog(id,download);
    window.open(url);
}
//
//新建下载日志
function NewDownloadLog(id,isdownload){
    $.ajax({
        url:'api/downloadlog/new',
        data:{fileid:id,download:isdownload},
        dataType:'json',
        type:'post',
        success:function (data) {
            if (data.status == 'ok'){
                ;//console.log(data.data);
            }else{
                ;//console.log(data.message);
            }
        }
    })
}

//新建文件夹
function OnNewFolder(){
    var htmlmsg='';
    htmlmsg=`<form style="padding: 30px 10px 0px 40px;" class="layui-form layui-form-pane">
    <div class="layui-form-item">
    <label class="layui-form-label">文件夹名称</label>
    <div class="layui-input-inline">
      <input type="text" name="username" lay-verify="required" placeholder="请输入名称" autocomplete="off" class="layui-input" id="newfoldername" value="新建文件夹">
    </div>
    </div>
    <div class="layui-form-item">
    <label class="layui-form-label">文件夹描述</label>
    <div class="layui-input-inline">
      <input type="text" name="username" lay-verify="required" placeholder="请输入描述" autocomplete="off" class="layui-input" id="newfolderdesc">
    </div>
  </div></form>`

    layer.open({
        type: 1 //0（信息框，默认）1（页面层）2（iframe层）3（加载层）4（tips层）
        ,anim:4 //弹出动画:0默认,1从上掉落,2从最底部往上滑入,3从左滑入,4从左翻滚,5渐显,6抖动
        ,title: '新建文件夹'
        ,area: ['390px', '260px']
        ,shade: 0.4 //遮蔽系数
        ,maxmin: false 
        ,content: htmlmsg
        ,btn: ['确认', '取消'] 
        ,yes: function(){
          var name=$("#newfoldername").val(); 
          var desc=$("#newfolderdesc").val(); 
          layer.closeAll();
          NewFolder(name,desc);
        }
        ,btn2: function(){
          layer.closeAll();
        }
      });
}
//新建文件夹
function NewFolder(name,desc){
    $.ajax({
        url:'api/catalog/newfolder',
        data:{pid:DIR_ID,name:name,decsription:desc},
        dataType:'json',
        type:'post',
        success:function (data) {
            if (data.status == 'ok'){
                RequestDirStat(0,true);
                RequestDirList(DIR_ID,false);
            }else{
                if(data.status == 'fail-0'){
                    location.reload();
                }else{
                    layer.msg(data.message);
                }
            }
        }
    })
}

//删除
function RequestRemove(id,extend_sub){
    var dirid=DIR_ID;
    if(id==dirid){
        dirid=DIR_PID
    }
    $.ajax({
        url:'api/catalog/remove',
        data:{id:id,extend_sub:extend_sub},
        dataType:'json',
        type:'post',
        success:function (data) {
            if (data.status == 'ok'){
                layer.msg('删除成功!');
                RequestDirStat(0,true);
                RequestDirList(dirid,false);
            }else{
                if(data.status == 'fail-0'){
                    location.reload();
                }else{
                    layer.msg(data.message);
                }
            }
        }
    })
}
//重命名文件
function RenameFile(id){
    var htmlmsg='';
    var node=DIR_NODES[id];
    var oldname= node.Name
    if(node.User.Id!=USER_MSG.Id && node.UploadUser.Id!=USER_MSG.Id){
        layer.msg("您只有修改自己上传的文件的权限!");
    }else{
        htmlmsg=`<form style="padding: 30px 10px 0px 40px;" class="layui-form layui-form-pane">
        <div class="layui-form-item">
        <label class="layui-form-label">新的名称</label>
        <div class="layui-input-inline">`;
        htmlmsg+='<input type="text" lay-verify="required" placeholder="请输入名称" autocomplete="off" class="layui-input" id="newitemname" value="'+oldname+'"></div></div></form>'

        layer.open({
            type: 1 //0（信息框，默认）1（页面层）2（iframe层）3（加载层）4（tips层）
            ,anim:Math.round(Math.random()*6) //弹出动画:0默认,1从上掉落,2从最底部往上滑入,3从左滑入,4从左翻滚,5渐显,6抖动
            ,title: '重命名'
            ,area: ['390px', '200px']
            ,shade: 0.4 //遮蔽系数
            ,maxmin: false 
            ,content: htmlmsg
            ,btn: ['确认', '取消'] 
            ,yes: function(){
            var name=$("#newitemname").val(); 
            layer.closeAll();
            $.ajax({
                url:'api/catalog/rename',
                data:{id:id,newname:name},
                dataType:'json',
                type:'post',
                success:function (data) {
                    if (data.status == 'ok'){
                        layer.msg('重命名成功!');
                        RequestDirList(DIR_ID,false);//重新加载文件夹内的内容
                    }else{
                        if(data.status == 'fail-0'){
                            location.reload();
                        }else{
                            layer.msg(data.message);
                        }
                    }
                }
            })
            }
            ,btn2: function(){
            layer.closeAll();
            }
        });
    }
}

//重命名文件夹
function RenameFolder(id){
    var htmlmsg='',dirid=0,oldname='';
    node=DIR_NODES[id];
    if(id>0){
        oldname=node.Name;
        dirid=id;
    }else{
        node=LAST_NODE;
        oldname= DIR_NAME;
        dirid=DIR_ID;
    }
    
    if(node.User.Id!=USER_MSG.Id && node.UploadUser.Id!=USER_MSG.Id){
        layer.msg("您只有修改自己创建的文件夹的权限!");
    }else{
        htmlmsg=`<form style="padding: 30px 10px 0px 40px;" class="layui-form layui-form-pane">
        <div class="layui-form-item">
        <label class="layui-form-label">新的名称</label>
        <div class="layui-input-inline">`;
        htmlmsg+='<input type="text" lay-verify="required" placeholder="请输入名称" autocomplete="off" class="layui-input" id="newitemname" value="'+oldname+'"></div></div></form>'

        layer.open({
            type: 1 //0（信息框，默认）1（页面层）2（iframe层）3（加载层）4（tips层）
            ,anim:Math.round(Math.random()*6) //弹出动画:0默认,1从上掉落,2从最底部往上滑入,3从左滑入,4从左翻滚,5渐显,6抖动
            ,title: '重命名'
            ,area: ['390px', '200px']
            ,shade: 0.4 //遮蔽系数
            ,maxmin: false 
            ,content: htmlmsg
            ,btn: ['确认', '取消'] 
            ,yes: function(){
            var name=$("#newitemname").val(); 
            layer.closeAll();
            $.ajax({
                url:'api/catalog/rename',
                data:{id:dirid,newname:name},
                dataType:'json',
                type:'post',
                success:function (data) {
                    if (data.status == 'ok'){
                        layer.msg('重命名成功!');
                        if(id>0){
                            RequestDirList(DIR_ID,false);
                        }else{
                            RequestDirPath(DIR_ID);//重新加载路径
                        }
                    }else{
                        if(data.status == 'fail-0'){
                            location.reload();
                        }else{
                            layer.msg(data.message);
                        }
                    }
                }
            })
            }
            ,btn2: function(){
            layer.closeAll();
            }
        });
    }
}
//删除文件
function RemoveFile(id){
    var node=DIR_NODES[id];
    if(node.User.Id!=USER_MSG.Id && node.UploadUser.Id!=USER_MSG.Id){
        layer.msg("您只有删除自己上传的文件的权限!");
    }else{
        var idx=layer.msg("确认删除文件 "+node.Name+" 吗?",{
            time: 10000 //10s后自动关闭
            ,btnAlign: 'c'
            ,anim:6
            ,btn:["确认","取消"]
            ,yes: function(){ // 下载
                layer.close(idx);
                RequestRemove(id,false);
            },
        });
    }
}

//删除文件夹
function RemoveFolder(id){
    node=DIR_NODES[id];
    if(id<=0){
        node=LAST_NODE;
    }
    
    if(node.User.Id!=USER_MSG.Id && node.UploadUser.Id!=USER_MSG.Id){
        layer.msg("您只有删除自己创建的文件夹的权限!");
    }else{
        if(id>0){
            $.ajax({
                url:'api/catalog/getlist',
                data:{id:id,removed:false},
                dataType:'json',
                type:'post',
                success:function (data) {
                    if (data.status == 'ok'){
                        var subitemcnt=data.data.length;
                        RemoveFolderPanel(id,DIR_NODES[id].Name,subitemcnt);
                    }else{
                        if(data.status == 'fail-0'){
                            location.reload();
                        }else{
                            layer.msg(data.message);
                        }
                    }
                }
            })
        }else{
            RemoveFolderPanel(DIR_ID,DIR_NAME,DIR_ITEM_CNT);
        } 
    }   
}
//删除文件夹面板
function RemoveFolderPanel(dirid,dirname,subitemcnt){
    var htmlmsg='',msg='',boxhigh=240;
    var padding='';
    if(subitemcnt==0){
        padding='padding: 10px 10px 0px 10px;';
        msg='<div class="layui-bg-red">确认删除文件夹《'+dirname+'》吗?</div>';
        msg+='<input type="text" name="username" lay-verify="required" placeholder="确认删除请输入:YES" autocomplete="off" class="layui-input layui-hide" id="deleteall" value="YES">';
    }else{
        boxhigh=300
        msg='<div class="layui-bg-red">文件夹非空,如果确认删除文件夹《'+dirname+'》及其子文件/文件夹,请在下面的输入框中输入"YES",否则不会执行删除动作！</div>';
        msg+='<div class="layui-form-item">';
        msg+='<label class="layui-form-label">全部删除</label>';
        msg+='<div class="layui-input-block all">';
        msg+='<input type="text" name="username" lay-verify="required" placeholder="确认删除请输入:YES" autocomplete="off" class="layui-input" id="deleteall">';
        msg+='</div>';
        msg+='</div>';
    }
    htmlmsg='<form style="'+padding+'" class="layui-form layui-form-pane">';
    htmlmsg+=msg;
    htmlmsg+='</form>';

    layer.open({
        type: 0 //0（信息框，默认）1（页面层）2（iframe层）3（加载层）4（tips层）
        ,anim:Math.round(Math.random()*6) //弹出动画:0默认,1从上掉落,2从最底部往上滑入,3从左滑入,4从左翻滚,5渐显,6抖动
        ,title: '删除文件夹:'+dirname
        ,area: ['390px', boxhigh]
        ,shade: 0.4 //遮蔽系数
        ,maxmin: false 
        ,content: htmlmsg
        ,btn: ['确认', '取消'] 
        ,yes: function(){
          var deleteall=$("#deleteall").val(); 
          layer.closeAll();
          if(deleteall=='YES'){
            RequestRemove(dirid,true)//删除
          }else{
              layer.msg("没有确认全部删除,不执行删除动作!");
          }
        }
        ,btn2: function(){
          layer.closeAll();
        }
      });
}


function OnClickRecycleBinItem(id){
    clearTimeout(sleeptime);  //首先清除计时器
    sleeptime = setTimeout(() => {
        var index=layer.msg("请选择您要执行的操作!", {
            time: 10000 //10s后自动关闭
            ,btnAlign: 'c'
            ,anim:Math.round(Math.random()*5) //弹出动画:0默认,1从上掉落,2从最底部往上滑入,3从左滑入,4从左翻滚,5渐显,6抖动
            ,title:'回收站操作'
            ,btn: ['恢复','清除', '关闭','属性']
            ,yes: function(){ // 恢复
                $.ajax({
                    url:'api/catalog/recovery',
                    data:{id:id},
                    dataType:'json',
                    type:'post',
                    success:function (data) {
                        if (data.status == 'ok'){
                            layer.msg("恢复回收站项目成功!")
                            RequestDirList(-1,true);
                        }else{
                            if(data.status == 'fail-0'){
                                location.reload();
                            }else{
                                layer.msg("恢复回收站项目失败:"+data.message);
                            }
                        }
                    }
                })
            },
            btn2: function(){ //清除
                var idx=layer.msg("确认清除文件 "+DIR_NODES[id].Name+" 吗?",{
                    time: 10000 //10s后自动关闭
                    ,btnAlign: 'c'
                    ,anim:6
                    ,btn:["确认","取消"]
                    ,yes: function(){ // 下载
                        layer.close(idx);
                        $.ajax({
                            url:'api/catalog/delete',
                            data:{id:id,extend_sub:true,force:false},
                            dataType:'json',
                            type:'post',
                            success:function (data) {
                                if (data.status == 'ok'){
                                    layer.msg("清除回收站项目 "+DIR_NODES[id].Name+" 成功!")
                                    RequestDirList(-1,true);
                                }else{
                                    if(data.status == 'fail-0'){
                                        location.reload();
                                    }else{
                                        layer.msg("清除回收站项目 "+DIR_NODES[id].Name+" 失败:"+data.message);
                                    }
                                }
                            }
                        })
                    },
                });
            },
            btn3: function(){ //关闭
                layer.close(index);
            },
            btn4: function(){ //属性
                layer.close(index);
                OnViewOption(id);
            },
        });
    },300);
}
//双击回收站中的文件夹
function OnDbClickRecycleBinItem(id){
    clearTimeout(sleeptime);  //首先清除计时器
    RequestDirList(id,true);
}

function OnClearRecyclebin(){
    var index=layer.msg("确定要清空回收站吗?", {
        time: 10000 //10s后自动关闭
        ,btnAlign: 'c'
        ,anim:Math.round(Math.random()*5) //弹出动画:0默认,1从上掉落,2从最底部往上滑入,3从左滑入,4从左翻滚,5渐显,6抖动
        //,title:'回收站操作'
        ,btn: ['确定','取消']
        ,yes: function(){ // 恢复
            $.ajax({
                url:'api/catalog/clearrecyclebin',
                //data:{id:id},
                dataType:'json',
                type:'post',
                success:function (data) {
                    if (data.status == 'ok'){
                        RequestDirList(-1,true);
                        layer.msg("清空回收站成功!")
                    }else{
                        if(data.status == 'fail-0'){
                            location.reload();
                        }else{
                            layer.msg("清空回收站失败:"+data.message);
                        }
                    }
                }
            })
        },
        btn2: function(){ //关闭
            layer.close(index);
        },
    });
}
//搜索
function OnSearch(){
    var namelike=$("#searchfile").val();
    if (namelike.length>0){
        $.ajax({
            url:'api/catalog/search',
            data:{id:DIR_ID,namelike:namelike},
            dataType:'json',
            type:'post',
            success:function (data) {
                if (data.status == 'ok'){
                    DecodeDirList(data.data);
                    $("#dirpath").html('我的搜索');
                }else{
                    if(data.status == 'fail-0'){
                        location.reload();
                    }else{
                        layer.msg(data.message);
                    }
                }
            }
        })
    }
}
//搜索框回车键按下
$("#searchfile").keydown(function(e) {  
    if (e.keyCode == 13) {  
        OnSearch();
    }  
}); 
//鼠标悬停效果(未实现)
function OnHoverItem(itemid,removed){
    var htmlmsg=''
    var node=DIR_NODES[itemid];
    htmlmsg='名称:'+node.Name;
    htmlmsg+='</br>描述:'+node.Desc;
    if(node.IsDir==false){
        var unit="KB",fsize=node.Size/1024;
        if(fsize>1024){
            fsize/=1024;
            unit="MB"
        }
        if(fsize>1024){
            fsize/=1024;
            unit="GB"
        }
        htmlmsg+='</br>大小:'+fsize.toFixed(2)+unit;
    }
    if(removed){
        htmlmsg+='</br>删除时间:'+node.DisableTime;
    }else{
        htmlmsg+='</br>创建时间:'+node.CreateTime;
    }
    layer.msg(htmlmsg);
}

//新建分享
function OnNewShare(id,isdir){
    var disabled='disabled=""';
    if(id<0){
        id=DIR_ID;
    }
    if (isdir){
        disabled='';
    }
    var htmlmsg='';
    htmlmsg='<form style="padding: 30px 10px 0px 40px;" class="layui-form">';
    
    htmlmsg+='<div class="layui-form-item">';
    htmlmsg+='<label class="layui-form-label">有效时间</label>';
    htmlmsg+='<div class="layui-input-inline">';
    htmlmsg+=`<select id="validity" lay-verify="">
        <option value="">请选择</option>
        <option value="0" selected="">长期</option>
        <option value="1">1天</option>
        <option value="7">7天</option>
        <option value="30">30天</option>
      </select>`;
    htmlmsg+='</div>';
    htmlmsg+='</div>';

    htmlmsg+='<div class="layui-form-item">';
    htmlmsg+='<label class="layui-form-label">上传与下载</label>';
    htmlmsg+=`<div class="layui-input-block">
        <input type="checkbox" name="canup" title="允许上传"`+disabled+`>
        <input type="checkbox" name="candown" title="允许下载" checked="">
    </div>`;
    htmlmsg+='</div>';

    htmlmsg+='<div class="layui-form-item">';
    htmlmsg+='<fieldset class="layui-elem-field layui-field-title" style="margin-top: 10px;"><legend>分享给谁</legend></fieldset>';

    htmlmsg+='<fieldset class="layui-elem-field" style="padding:10px;"><legend>按用户组</legend>';
    htmlmsg+='<div id="groupselector" class="demo-transfer"></div>';
    htmlmsg+='</fieldset>';

    htmlmsg+='<fieldset class="layui-elem-field" style="padding:10px;"><legend>按用户</legend>';
    htmlmsg+='<div id="userselector" class="demo-transfer"></div>';
    htmlmsg+='</fieldset>';
    htmlmsg+='</div>';

    htmlmsg+='</form>';

    var groupids=[],userids=[];
    var index=layer.open({
        type: 1 //0（信息框，默认）1（页面层）2（iframe层）3（加载层）4（tips层）
        ,anim:Math.round(Math.random()*5) //弹出动画:0默认,1从上掉落,2从最底部往上滑入,3从左滑入,4从左翻滚,5渐显,6抖动
        ,title: '分享'
        ,area: ['620px', '700px']
        ,shade: 0.4 //遮蔽系数
        ,maxmin: true 
        ,content: htmlmsg
        ,btn: ['确认', '取消'] 
        ,yes: function(){
          var validity=$("#validity").val(); 
          var canup=$("input[name='canup']:checked").val(); 
          var candown=$("input[name='candown']:checked").val(); 
          validity*=86400;
          if(canup=='on'){
            canup=true;
          }else{
            canup=false;
          }
          if(candown=='on'){
            candown=true;
          }else{
            candown=false;
          }
          layer.close(index);
          RequestNewOrUpdateShare(id,validity,candown,canup,userids,groupids,true);
        }
        ,btn2: function(){
          layer.close(index);
        }
    });
    layui.form.render();

    layui.use('transfer', function(){
        var transfer = layui.transfer;     
        //渲染
        transfer.render({
          elem: '#groupselector'  //绑定元素
          ,data:GROUP_LIST
           ,value:[]//默认选中的
          ,title: ['全部组', '已选组']
          ,onchange: function(obj, index){//index:变动的数据来源,0=左边,1=右边
            groupids=this.value;
          }
          ,showSearch: true//显示搜索框
          //,height: 210 //定义高度
          ,id: 'groups' //定义索引
        });

        transfer.render({
          elem: '#userselector'  //绑定元素
          ,data:USER_LIST
          ,value:[]//默认选中的
          ,title: ['全部用户', '已选用户']
          ,onchange: function(obj, index){//index:变动的数据来源,0=左边,1=右边
            userids=this.value;
          }
          ,showSearch: true//显示搜索框
          //,height: 210 //定义高度
          ,id: 'users' //定义索引
        });
    });
}

//分享详情
function OnShareDetailed(shareid){
    var node=DIR_SHARE[shareid];
    var disabled='disabled=""';
    if (node.Catalog.IsDir){
        disabled='';
    }
    var groups=[],users=[];
    if (node.Groups!=null){
        for(i=0;i<node.Groups.length;i++){
            groups.push(node.Groups[i].Id);
        }
    }
    if (node.Users!=null){
        for(i=0;i<node.Users.length;i++){
            users.push(node.Users[i].Id);
        }
    }
    var htmlmsg='';
    htmlmsg='<form style="padding: 30px 10px 0px 40px;" class="layui-form">';
    
    htmlmsg+='<div class="layui-form-item">';
    htmlmsg+='<label class="layui-form-label">有效时间</label>';
    htmlmsg+='<div class="layui-input-inline">';
    htmlmsg+='<select id="validity" lay-verify="">';
    htmlmsg+='<option value="">请选择</option>';
    htmlmsg+='<option value="0" ';
    node.TermOfValidity==0?htmlmsg+='selected="">长期</option>':htmlmsg+='>长期</option>';
    htmlmsg+='<option value="1" ';
    node.TermOfValidity/86400==1?htmlmsg+='selected="">1天</option>':htmlmsg+='>1天</option>';
    htmlmsg+='<option value="7" ';
    node.TermOfValidity/86400==7?htmlmsg+='selected="">7天</option>':htmlmsg+='>7天</option>';
    htmlmsg+='<option value="30" ';
    node.TermOfValidity/86400==30?htmlmsg+='selected="">30天</option>':htmlmsg+='>30天</option>';
    htmlmsg+='</select>';
    htmlmsg+='</div>';
    htmlmsg+='</div>';

    htmlmsg+='<div class="layui-form-item">';
    htmlmsg+='<label class="layui-form-label">上传与下载</label>';
    htmlmsg+='<div class="layui-input-block">';
    htmlmsg+='<input type="checkbox" name="canup" title="允许上传" '+disabled;
    node.CanUpload?htmlmsg+=' checked="" >':htmlmsg+='>';
    htmlmsg+='<input type="checkbox" name="candown" title="允许下载"';
    node.CanDownload?htmlmsg+=' checked="">':htmlmsg+='>';
    htmlmsg+='</div>';
    htmlmsg+='</div>';

    htmlmsg+='<div class="layui-form-item">';
    htmlmsg+='<fieldset class="layui-elem-field layui-field-title" style="margin-top: 10px;"><legend>分享给谁</legend></fieldset>';

    htmlmsg+='<fieldset class="layui-elem-field" style="padding:10px;"><legend>按用户组</legend>';
    htmlmsg+='<div id="groupselector" class="demo-transfer"></div>';
    htmlmsg+='</fieldset>';

    htmlmsg+='<fieldset class="layui-elem-field" style="padding:10px;"><legend>按用户</legend>';
    htmlmsg+='<div id="userselector" class="demo-transfer"></div>';
    htmlmsg+='</fieldset>';
    htmlmsg+='</div>';

    htmlmsg+='</form>';

    var groupids=groups,userids=users;
    var index=layer.open({
        type: 1 //0（信息框，默认）1（页面层）2（iframe层）3（加载层）4（tips层）
        ,anim:Math.round(Math.random()*5) //弹出动画:0默认,1从上掉落,2从最底部往上滑入,3从左滑入,4从左翻滚,5渐显,6抖动
        ,title: '分享'
        ,area: ['620px', '700px']
        ,shade: 0.4 //遮蔽系数
        ,maxmin: true 
        ,content: htmlmsg
        ,btn: ['确认', '取消'] 
        ,yes: function(){
          var validity=$("#validity").val(); 
          var canup=$("input[name='canup']:checked").val(); 
          var candown=$("input[name='candown']:checked").val(); 
          validity*=86400;
          if(canup=='on'){
            canup=true;
          }else{
            canup=false;
          }
          if(candown=='on'){
            candown=true;
          }else{
            candown=false;
          }
          layer.close(index);
          RequestNewOrUpdateShare(shareid,validity,candown,canup,userids,groupids,false);
        }
        ,btn2: function(){
          layer.close(index);
        }
    });
    layui.form.render();

    layui.use('transfer', function(){
        var transfer = layui.transfer;     
        //渲染
        transfer.render({
          elem: '#groupselector'  //绑定元素
          ,data:GROUP_LIST
           ,value:groups//默认选中的
          ,title: ['全部组', '已选组']
          ,onchange: function(obj, index){//index:变动的数据来源,0=左边,1=右边
            groupids=this.value;
          }
          ,showSearch: true//显示搜索框
          //,height: 210 //定义高度
          ,id: 'groups' //定义索引
        });

        transfer.render({
          elem: '#userselector'  //绑定元素
          ,data:USER_LIST
          ,value:users//默认选中的
          ,title: ['全部用户', '已选用户']
          ,onchange: function(obj, index){//index:变动的数据来源,0=左边,1=右边
            userids=this.value;
          }
          ,showSearch: true//显示搜索框
          //,height: 210 //定义高度
          ,id: 'users' //定义索引
        });
    });
}

function RequestNewOrUpdateShare(id,validity,candown,canup,userids,groupids,isnew){
    var url='update',okmsg='更新成功!',failmsg='更新失败:';
    if (isnew){
        url='new',
        okmsg='分享成功!';
        failmsg='分享失败:';
    }
    $.ajax({
        url:'api/share/'+url,
        data:{
            'id':id
            ,'validity':validity
            ,'canup':canup
            ,'candown':candown
            ,'userids':userids.toString()
            ,'groupids':groupids.toString()
        },
        dataType:'json',
        type:'post',
        success:function (data) {
            if (data.status == 'ok'){
                if(isnew==false){
                    OnMyShared();
                }
                layer.msg(okmsg);
            }else{
                if(data.status == 'fail-0'){
                    location.reload();
                }else{
                    layer.msg(failmsg+data.message);
                }
            }
        }
    })
}


//请求用户组列表
function RequestGroups(){
    GROUP_LIST.splice(0,GROUP_LIST.length);
    $.ajax({
        url:'api/usergroup/getgrouplist',
        data:{
            'namelike':''
            ,'rmkslike':''
        },
        dataType:'json',
        type:'post',
        success:function (data) {
            //console.log(data.data);
            if (data.status == 'ok'){
                for(i=0;i<data.data.length;i++){
                    var item={};
                    item['value']=data.data[i].Id;
                    item['title']=data.data[i].Remarkes;
                    item['name']=data.data[i].Name;
                    GROUP_LIST.push(item);
                }
            }else{
                if(data.status == 'fail-0'){
                    location.reload();
                }else{
                    layer.msg(data.message);
                }
            }
        }
    })
}

//请求用户列表
function RequestUsers(){
    USER_LIST.splice(0,USER_LIST.length);
    $.ajax({
        url:'api/user/getuserlist',
        data:{
            'namelike':''
            ,'usernamelike':''
        },
        dataType:'json',
        type:'post',
        success:function (data) {
            //console.log(data.data);
            if (data.status == 'ok'){
                for(i=0;i<data.data.length;i++){
                    var item={};
                    item['value']=data.data[i].Id;
                    item['title']=data.data[i].Username;
                    item['name']=data.data[i].Name;
                    if (data.data[i].Id ==USER_MSG.Id){
                        item['disabled']='1';
                    }
                    USER_LIST.push(item);
                }
            }else{
                if(data.status == 'fail-0'){
                    location.reload();
                }else{
                    layer.msg(data.message);
                }
            }
        }
    })
}

//请求登录用户的基本信息
function RequestUserMsg(){
    $.ajax({
        url:'api/user/getusermsg',
        data:{
        },
        dataType:'json',
        type:'post',
        success:function (data) {
            if (data.status == 'ok'){
                USER_MSG=data.data;
            }else{
                if(data.status == 'fail-0'){
                    location.reload();
                }else{
                    layer.msg(data.message);
                }
            }
        }
    })
}

//请求分布式文件系统的域名
function RequestDfsDomain(){
    $.ajax({
        url:'api/getdomain',
        data:{
        },
        dataType:'json',
        type:'post',
        success:function (data) {
            if (data.status == 'ok'){
                DFS_DOMAIN=data.data;
                DFS_GROUP=data.message;
                console.log(DFS_DOMAIN,DFS_GROUP);
            }else{
                if(data.status == 'fail-0'){
                    location.reload();
                }else{
                    layer.msg(data.message);
                }
            }
        }
    })
}


function NewFileIndex(filename,pid,domain,url,path,md5,scene,size,mtime,token){
    $.ajax({
        url:'api/fileindex/new',
        data:{'domain':domain,
          'url': url,
          'md5': md5,
          'path': path,
          'scene': scene,
          'size': size,
          'mtime': mtime, 
          'token':token,
        },
        dataType:'json',
        type:'post',
        success:function (data) {
            if (data.status == 'ok'){
                //layer.msg(data.data);
                NewFileCatalog(pid,filename,data.data.Id);
            }else{
                if(data.status == 'fail-0'){
                    location.reload();
                }else{
                    layer.msg("上传文件后新建文件索引失败:"+data.message);
                }
            }
        }
      });
}

function NewFileCatalog(pid,name,fileid){
    $.ajax({
        url:'api/catalog/newfile',
        data:{'pid':pid,
          'name': name,
          'fileid': fileid,
        },
        dataType:'json',
        type:'post',
        success:function (data) {
            if (data.status == 'ok'){
                layer.msg("文件《"+name+"》上传成功!");
                RequestDirList(pid,false);
            }else{
                if(data.status == 'fail-0'){
                    location.reload();
                }else{
                    layer.msg("上传文件后新建文件目录失败:"+data.message);
                }
            }
        }
      });
}

//请求我的共享文件
function RequestMyShare(){
    $.ajax({
        url:'api/share/myshare',
        dataType:'json',
        type:'post',
        success:function (data) {
            if (data.status == 'ok'){
                DecodeShare(data.data);
            }else{
                if(data.status == 'fail-0'){
                    location.reload();
                }else{
                    layer.msg("获取我共享的项目失败:"+data.message);
                }
            }
        }
      });
}

//请求共享给我的文件
function RequestShareWithMe(){
    $.ajax({
        url:'api/share/withme',
        dataType:'json',
        type:'post',
        success:function (data) {
            if (data.status == 'ok'){
                DecodeShare(data.data);
            }else{
                if(data.status == 'fail-0'){
                    location.reload();
                }else{
                    layer.msg("获取共享给我的项目失败:"+data.message);
                }
            }
        }
      });
}


function DecodeShare(data){
    DIR_SHARE_RAW=data;
    for (let nm in DIR_SHARE) {//清空键值对
        delete (DIR_SHARE[nm]);
    }
    if(data!=null){
        for(j = 0,len=data.length; j < len; j++) {
            var share = data[j]
            DIR_SHARE[share.Id]=share;
        }
    }else{
        data=[];
    }
    
    DecodeDirList(data);
}

//字符串转日期格式，strDate要转为日期格式的字符串
function getDate(strDate) {
    var strs=strDate.split('.');
    var str=strs[0].replace('T',' ');
    var date=new Date(str);
    return {'date':date,'str':str};
}

//通过文件的大小(字节)获取字符串描述的大小
function getSizeStr(size){
    var unit="KB",fsize=size/1024;
    if(fsize>1024){
        fsize/=1024;
        unit="MB";
    }
    if(fsize>1024){
        fsize/=1024;
        unit="GB";
    }
    fsize=fsize.toFixed(2);
    return {'size':fsize,'unit':unit};
}

//取消分享
function OnDisableShare(shareid){
    var index=layer.msg('您确认要取消分享吗?',{
        time: 10000 //10s后自动关闭
        ,btnAlign: 'c'
        ,anim:6 //弹出动画:0默认,1从上掉落,2从最底部往上滑入,3从左滑入,4从左翻滚,5渐显,6抖动
        ,btn: ['确认','取消']
        ,yes: function(){ // 确认取消共享
            layer.close(index);
            $.ajax({
                url:'api/share/remove',
                data:{id:shareid},
                dataType:'json',
                type:'post',
                success:function (data) {
                    if (data.status == 'ok'){
                        layer.msg('已成功取消!');
                        OnMyShared();
                    }else{
                        if(data.status == 'fail-0'){
                            location.reload();
                        }else{
                            failt=layer.msg('取消分享失败:'+data.message,{
                                time:20000
                                ,btn: ['关闭']
                                ,yes:function(){
                                    layer.close(failt);
                                }
                            });
                        }
                    }
                }
            })
        },
        btn2: function(){ //取消
            layer.close(index);
        },
    });
}

//更新上传进度条
function uploadProgress(percent){
    var htmlmsg='<div class="layui-progress-bar" lay-percent="'+percent+'" id="uploadprogress"></div>'
    $("#uploadprogress").html(htmlmsg);
    layui.element.render();
}


//编辑备注
function OnUpdateDescription(id){
    var htmlmsg='',olddesc='';
    var node=DIR_NODES[id];
    olddesc=node.Desc;
    
    if(node.User.Id!=USER_MSG.Id && node.UploadUser.Id!=USER_MSG.Id){
        layer.msg("您只有修改自己创建的文件和文件夹的权限!");
    }else{
        htmlmsg=`<form style="padding: 25px 40px 0px 30px;" class="layui-form">
        <div class="layui-form-item layui-form-text">
        <label class="layui-form-label">新的备注：</label>
        <div class="layui-input-block">`;
        htmlmsg+='<textarea placeholder="请输入备注" class="layui-textarea" id="newitemdesc">'+olddesc+'</textarea></div></div></form>'

        layer.open({
            type: 1 //0（信息框，默认）1（页面层）2（iframe层）3（加载层）4（tips层）
            ,anim:Math.round(Math.random()*6) //弹出动画:0默认,1从上掉落,2从最底部往上滑入,3从左滑入,4从左翻滚,5渐显,6抖动
            ,title: '修改备注'
            ,area: ['500px', '260px']
            ,shade: 0.4 //遮蔽系数
            ,maxmin: false 
            ,content: htmlmsg
            ,btn: ['确认', '取消'] 
            ,yes: function(){
            var desc=$("#newitemdesc").val(); 
            layer.closeAll();
            $.ajax({
                url:'api/catalog/updatedesc',
                data:{id:id,desc:desc},
                dataType:'json',
                type:'post',
                success:function (data) {
                    if (data.status == 'ok'){
                        layer.msg('更新备注成功!');
                        if(id>0){
                            RequestDirList(DIR_ID,false);
                        }else{
                            RequestDirPath(DIR_ID);//重新加载路径
                        }
                    }else{
                        if(data.status == 'fail-0'){
                            location.reload();
                        }else{
                            layer.msg(data.message);
                        }
                    }
                }
            })
            }
            ,btn2: function(){
            layer.closeAll();
            }
        });
    }
}

//查看属性
function OnViewOption(id){
    var node=DIR_NODES[id];
    var no='<i class="layui-icon layui-icon-close" style="font-size: 20px; color: #FF5722;"></i>';
    var yes='<i class="layui-icon layui-icon-ok-circle" style="font-size: 20px; color: #5FB878;"></i>';
    var htmlmsg='';
    htmlmsg+='<div style="padding:20px">';
    htmlmsg+='<table class="layui-table" lay-even lay-skin="nob">';
    htmlmsg+='<thead><tr><th>属性</th><th>属性值</th></tr></thead>'
    htmlmsg+='<tbody>';
    htmlmsg+='<tr><td>文件名</td><td>'+node.Name+'</td></tr>';
    htmlmsg+='<tr><td>文件备注</td><td>'+node.Desc+'</td></tr>';
    var isdir=no;
    var size='',unit=''
    if (node.IsDir){
        isdir=yes;
    }else{
        fs=getSizeStr(node.Size);
        size=fs.size;
        unit=fs.unit;
    }
    if(node.User==null || node.User.Username.length==0){
        if(MAIN_PATH=='_MYDISK' || MAIN_PATH=='_MYRECYCLEBINE'){
            node.User=USER_MSG;
        }
    }
    if(node.UploadUser==null){
        node.UploadUser=USER_MSG;
    }
    htmlmsg+='<tr><td>文件夹</td><td>'+isdir+'</td></tr>';
    htmlmsg+='<tr><td>拥有者</td><td>'+node.User.Username+'</td></tr>';
    htmlmsg+='<tr><td>创建者</td><td>'+node.UploadUser.Username+'</td></tr>';
    htmlmsg+='<tr><td>创建时间</td><td>'+getDate(node.CreateTime).str+'</td></tr>';
    if(MAIN_PATH=='_MYRECYCLEBINE'){
        htmlmsg+='<tr><td>删除时间</td><td>'+getDate(node.DisableTime).str+'</td></tr>';
    }
    htmlmsg+='<tr><td>文件大小</td><td>'+size+unit+'</td></tr>';
    if(node.HasShared){
        htmlmsg+='<tr><td>已分享</td><td>'+yes+'</td></tr>';
    }else{
        if(MAIN_PATH=='_MYSHARED' || MAIN_PATH=='_SHAREDWITHME'){
            htmlmsg+='<tr><td>已分享</td><td>'+yes+'</td></tr>';
        }else{
            htmlmsg+='<tr><td>已分享</td><td>'+no+'</td></tr>';
        }
    }
    htmlmsg+='<tr><td>MD5|SHA1</td><td>'+node.Md5+'</td></tr>';
    if(MAIN_PATH != '_SHAREDWITHME'){
        htmlmsg+='<tr><td>存储路径</td><td>'+node.Path+'</td></tr>';
    }
    htmlmsg+='</tbody></table>';
    htmlmsg+='</div>';

    var index=layer.open({
        type: 1 //0（信息框，默认）1（页面层）2（iframe层）3（加载层）4（tips层）
        ,anim:Math.round(Math.random()*5) //弹出动画:0默认,1从上掉落,2从最底部往上滑入,3从左滑入,4从左翻滚,5渐显,6抖动
        ,title: '详细属性'
        ,area: '500px'
        ,shade: 0.4 //遮蔽系数
        ,maxmin: true 
        ,content: htmlmsg
        ,btn: ['关闭'] 
        ,yes: function(){
            layer.close(index);
        }
    });
}

function OnViewDownloadlogs(id){
    $.ajax({
        url:'api/catalog/getdownloadlog',
        data:{id:id},
        dataType:'json',
        type:'post',
        success:function (data) {
            if (data.status == 'ok'){
                if(data.message=='0'){
                    layer.msg('文档没有被下载过!')
                }else{
                    DecodeDownloadLogs(data.data);
                }
            }else{
                if(data.status == 'fail-0'){
                    location.reload();
                }else{
                    layer.msg(data.message);
                }
            }
        }
    })
}

function DecodeDownloadLogs(data){
    var htmlmsg='';
    htmlmsg+='<div style="padding:20px">';
    htmlmsg+='<table class="layui-table" lay-even lay-skin="nob">';
    htmlmsg+='<thead><tr><th>序号</th><th>下载者</th><th>下载时间</th><th>下载/预览</th></tr></thead>'
    htmlmsg+='<tbody>';
    for(i=0;i<data.length;i++){
        log=data[i];
        var dtyp='<button type="button" class="layui-btn layui-btn-xs layui-btn-radius layui-btn-normal">下载</button>'
        if(log.Download==0){
            dtyp='<button type="button" class="layui-btn layui-btn-xs layui-btn-radius layui-btn-warm">预览</button>'
        }
        htmlmsg+='<tr><td>'+(i+1)+'</td><td>'+log.User.Name+'</td><td>'+getDate(log.CreateTime).str+'</td><td>'+dtyp+'</td></tr>';
    }
    htmlmsg+='</tbody></table>';
    htmlmsg+='</div>';
    var index=layer.open({
        type: 1 //0（信息框，默认）1（页面层）2（iframe层）3（加载层）4（tips层）
        ,anim:Math.round(Math.random()*5) //弹出动画:0默认,1从上掉落,2从最底部往上滑入,3从左滑入,4从左翻滚,5渐显,6抖动
        ,title: '下载/预览记录'
        ,area: ['800px', '600px']
        ,shade: 0.4 //遮蔽系数
        ,maxmin: true 
        ,content: htmlmsg
        ,btn: ['关闭'] 
        ,yes: function(){
            layer.close(index);
        }
    });
}

//加载文件或者文件夹树
//ids:需要移动或者复制的项目的id数组
//isall: true=加载所有文件和文件夹,false=只加载文件夹
//title: {moveto:移动到,copyto:复制到,mydisk:我的网盘}
function OnGetCatalogsTree(ids,isall,title){
    $.ajax({
        url:'api/catalog/catalogs',
        data:{isall:isall},
        dataType:'json',
        type:'post',
        success:function (data) {
            if (data.status == 'ok'){
                DecodeCatalogsTree(ids,data.data,isall,title);
            }else{
                if(data.status == 'fail-0'){
                    location.reload();
                }else{
                    layer.msg(data.message);
                }
            }
        }
    })
}

//解析文件树
function DecodeCatalogsTree(ids,datas,isall,title){
    var zNodes=[{id:0,pId:0,name:'我的网盘',icon:'static/img/home1.png',open:true}];
    for(i=0;i<datas.length;i++){
        var node={id:datas[i].Id,pId:datas[i].Pid,name:datas[i].Name,icon:'',open:false,isParent :true}
        zNodes.push(node);
    }
    var htmlmsg='';
    htmlmsg=`
    <div  style="padding:20px" id="TreeBar">
	  <div id="TreeButton" class="row border rounded">
		<div class="layui-col-md12 layui-btn-group">
			<button type="button" class="layui-btn layui-btn-sm layui-btn-normal" id="ExpandTreeNode" >展开</button>
			<button type="button" class="layui-btn layui-btn-sm layui-btn-warm" id="CollapseTreeNode" >收起</button>
		</div>
		<div>
			<input class="layui-col-md12" type="text" id="SearchTreeNode" placeholder="搜索">
		</div>
	  </div>
	  <div class="layui-row">
		<div id="NodeTree" class="ztree"></div><!--树形结构-->
	  </div>
    </div>`;
    var titletext={
        'moveto':'移动到'
        ,'copyto':'复制到'
    };
    var index=layer.open({
        type: 1 //0（信息框，默认）1（页面层）2（iframe层）3（加载层）4（tips层）
        ,anim:Math.round(Math.random()*5) //弹出动画:0默认,1从上掉落,2从最底部往上滑入,3从左滑入,4从左翻滚,5渐显,6抖动
        ,title: titletext[title]  //标题
        ,area: '400px' //窗口尺寸
        ,shade: 0.4 //遮蔽系数
        ,maxmin: true //是否显示最大化最小化按钮
        ,content: htmlmsg //填充窗口的html信息
        ,success: function(layero, index){ //窗口成功打开后执行的回调
            treeinit(zNodes);
        }
        ,btn: ['确定','关闭'] //窗口显示的按钮
        ,yes: function(){ //第一个按钮的执行函数
            layer.close(index);
            if (title=='moveto'){
                CatalogsMoveTo(ids,SELECTED_DIRTREE_ID);
            }else if(title='copyto'){
                CatalogsCopyTo(ids,SELECTED_DIRTREE_ID);
            }
        }
        ,btn2: function(){ //第二个按钮的执行函数,以此类推,比如:btn3,btn4等
            layer.close(index);
        }
    });
}

//文件夹移动到
function CatalogsMoveTo(ids,dirid){
    $.ajax({
        url:'api/catalog/moveto',
        data:{catalogs:ids.toString(),dirid:dirid},
        dataType:'json',
        type:'post',
        success:function (data) {
            if (data.status == 'ok'){
                RequestDirStat(0,true);
                RequestDirList(DIR_ID,false);
            }else{
                if(data.status == 'fail-0'){
                    location.reload();
                }else{
                    layer.msg(data.message);
                }
            }
        }
    })
}

//文件夹复制到
function CatalogsCopyTo(ids,dirid){
    $.ajax({
        url:'api/catalog/copyto',
        data:{catalogs:ids.toString(),dirid:dirid},
        dataType:'json',
        type:'post',
        success:function (data) {
            if (data.status == 'ok'){
                var str='复制成功了 '+data.data+'个文件和文件夹。';
                layer.msg(str);
            }else{
                if(data.status == 'fail-0'){
                    location.reload();
                }else{
                    layer.msg(data.message);
                }
            }
        }
    })
}

//我的基本配置信息
function MyConfig(){
    $.ajax({
        url:'api/config',
        data:{},
        dataType:'json',
        type:'post',
        success:function (data) {
            var cfg=data.micdiskCfg;
            var htmlmsg='<div style="padding:10px"><table class="layui-table" lay-even lay-skin="nob"><thead><tr><th>属性</th><th>值</th></tr></thead><tbody>';
            htmlmsg+='<tr><td>程序版本</td><td>'+cfg.Version+'</td></tr>';
            htmlmsg+='<tr><td>网盘域名</td><td>'+cfg.DfsDomain+'</td></tr>';
            htmlmsg+='<tr><td>网盘分组</td><td>'+cfg.DfsGroup+'</td></tr>';
            htmlmsg+='<tr><td>错误信息</td><td>'+cfg.DfsErr+'</td></tr>';
            htmlmsg+='<tr><td>AccessKey</td><td>'+data.AccessKey+'</td></tr>';
            htmlmsg+='</tbody></table>';
            htmlmsg+='<span class="layui-badge">AccessKey会随着密码的改变而改变！变更密码后请及时更新相关接口中的AccessKey！</span></div>';

            var index=layer.open({
                type: 1 //0（信息框，默认）1（页面层）2（iframe层）3（加载层）4（tips层）
                ,anim:Math.round(Math.random()*5) //弹出动画:0默认,1从上掉落,2从最底部往上滑入,3从左滑入,4从左翻滚,5渐显,6抖动
                ,title: "配置信息"  //标题
                ,area: '500px' //窗口尺寸
                ,shade: 0.4 //遮蔽系数
                ,maxmin: false //是否显示最大化最小化按钮
                ,content: htmlmsg //填充窗口的html信息
                ,success: function(layero, index){ //窗口成功打开后执行的回调
                    ;
                }
                ,btn: ['关闭'] //窗口显示的按钮
                ,yes: function(){ //第二个按钮的执行函数,以此类推,比如:btn3,btn4等
                    layer.close(index);
                }
            });
        }
    })
}

//我的基本信息
function MyMessage(){
    var htmlmsg='<div style="padding:10px"><table class="layui-table" lay-even lay-skin="nob"><thead><tr><th>属性</th><th>值</th><th>操作</th></tr></thead><tbody>';
    htmlmsg+='<tr><td>我的账号</td><td>'+USER_MSG.Name+'</td><td>&nbsp</td></tr>';
    htmlmsg+='<tr><td>我的名称</td><td id="username">'+USER_MSG.Username+'</td><td><button type="button" class="ayui-btn layui-btn-primary layui-border-blue layui-btn-sm" onclick="ChangeMyName();">更改</button></td></tr>';
    htmlmsg+='<tr><td>我的密码</td><td>保密,谁都不能看</td><td><button type="button" class="ayui-btn layui-btn-primary layui-border-blue layui-btn-sm" onclick="ChangeMyPswd();">更改</button></td></tr>';
    htmlmsg+='<tr><td>我的容量</td><td>'+USER_MSG.MaxSpace+' MB</td><td>&nbsp</td></tr>';
    htmlmsg+='<tr><td>注册时间</td><td>'+getDate(USER_MSG.CreateTime).str+'</td><td>&nbsp</td></tr>';
    htmlmsg+='<tr><td>AccessKey</td><td>'+USER_MSG.Password+'</td><td>&nbsp</td></tr>';
    htmlmsg+='</tbody></table>';
    htmlmsg+='<span class="layui-badge">AccessKey会随着密码的改变而改变！变更密码后请及时更新相关接口中的AccessKey！</span></div>';

    var index=layer.open({
        type: 1 //0（信息框，默认）1（页面层）2（iframe层）3（加载层）4（tips层）
        ,anim:Math.round(Math.random()*5) //弹出动画:0默认,1从上掉落,2从最底部往上滑入,3从左滑入,4从左翻滚,5渐显,6抖动
        ,title: "我的基本信息"  //标题
        ,area: '600px' //窗口尺寸
        ,shade: 0.4 //遮蔽系数
        ,maxmin: false //是否显示最大化最小化按钮
        ,content: htmlmsg //填充窗口的html信息
        ,success: function(layero, index){ //窗口成功打开后执行的回调
            ;
        }
        ,btn: ['关闭'] //窗口显示的按钮
        ,yes: function(){ //第二个按钮的执行函数,以此类推,比如:btn3,btn4等
            layer.close(index);
        }
    });
}

//更改名称
function ChangeMyName(){
    var htmlmsg=`<form style="padding: 30px 10px 0px 40px;" class="layui-form layui-form-pane">
    <div class="layui-form-item">
    <label class="layui-form-label">新的名称</label>
    <div class="layui-input-inline">`;
    htmlmsg+='<input type="text" lay-verify="required" placeholder="请输入新的名称" autocomplete="off" class="layui-input" id="newname" value="'+USER_MSG.Username+'"></div></div></form>'

    var idex=layer.open({
        type: 0 //0（信息框，默认）1（页面层）2（iframe层）3（加载层）4（tips层）
        ,anim:Math.round(Math.random()*5) //弹出动画:0默认,1从上掉落,2从最底部往上滑入,3从左滑入,4从左翻滚,5渐显,6抖动
        ,title: '更改名称'
        ,area: '450px'
        ,shade: 0.4 //遮蔽系数
        ,maxmin: false 
        ,content: htmlmsg
        ,btn: ['确认', '取消'] 
        ,yes: function(){
            var name=$("#newname").val(); 
            layer.close(idex);
            $.ajax({
                url:'api/user/updateusername',
                data:{username:name},
                dataType:'json',
                type:'post',
                success:function (data) {
                    if (data.status == 'ok'){
                        layer.msg('更名成功!');
                        USER_MSG.Username=name
                        $("#username").html(name);
                        $("#UserName").text(name)
                    }else{
                        layer.msg(data.message);
                    }
                }
            })
        }
        ,btn2: function(){
        layer.close(idex);
        }
    });
}

//更改密码
function ChangeMyPswd(){
    var htmlmsg=`<form style="padding: 30px 10px 0px 40px;" class="layui-form layui-form-pane">`;
    htmlmsg+=`<div class="layui-form-item">`;
    htmlmsg+=`<label class="layui-form-label">旧密码</label>`;
    htmlmsg+=`<div class="layui-input-inline">`;
    htmlmsg+='<input type="password" lay-verify="required" placeholder="请输入旧密码" autocomplete="off" class="layui-input" id="oldpswd" value=""></div><br><br>';

    htmlmsg+=`<label class="layui-form-label">新密码</label>`;
    htmlmsg+=`<div class="layui-input-inline">`;
    htmlmsg+='<input type="password" lay-verify="required" placeholder="请输入新密码" autocomplete="off" class="layui-input" id="newpswd" value=""></div><br><br>';

    htmlmsg+=`<label class="layui-form-label">确认新密码</label>`;
    htmlmsg+=`<div class="layui-input-inline">`;
    htmlmsg+='<input type="password" lay-verify="required" placeholder="请再次输入新密码" autocomplete="off" class="layui-input" id="newpswd2" value=""></div>';
    htmlmsg+=`</div></form>'`;

    var idex=layer.open({
        type: 0 //0（信息框，默认）1（页面层）2（iframe层）3（加载层）4（tips层）
        ,anim:Math.round(Math.random()*5) //弹出动画:0默认,1从上掉落,2从最底部往上滑入,3从左滑入,4从左翻滚,5渐显,6抖动
        ,title: '更改密码'
        ,area: '450px'
        ,shade: 0.4 //遮蔽系数
        ,maxmin: false 
        ,content: htmlmsg
        ,btn: ['确认', '取消'] 
        ,yes: function(){
            if ($("#newpswd").val()!=$("#newpswd2").val()){
                layer.msg("两次输入的新密码不一致!")
            } else{
                $.ajax({
                    url:'api/user/updatepswd',
                    data:{
                        oldpswd:$("#oldpswd").val()
                        ,newpswd:$("#newpswd").val()
                        ,newpswd2:$("#newpswd2").val()
                    },
                    dataType:'json',
                    type:'post',
                    success:function (data) {
                        if (data.status == 'ok'){
                            layer.msg('更改密码成功,3秒后将退出系统重新登录!');
                            layer.close(idex);
                            clearTimeout(sleeptime);  //首先清除计时器
                            sleeptime = setTimeout(() => {
                                location.href="logout";
                            },3000);
                        }else{
                            var msg=data.message;
                            switch(data.status){
                                case "fail-1":
                                    msg="获取输入参数失败!";
                                    break;
                                case "fail-2":
                                    msg="两次输入的新密码不一致!";
                                    break;
                                case "fail-3":
                                    msg="旧密码错误!";
                                    break;
                                case "fail-4":
                                    msg="更新密码失败!";
                                    break;
                            }
                            layer.msg(msg);
                        }
                    }
                });
            }
        }
        ,btn2: function(){
            layer.close(idex);
        }
    });
}