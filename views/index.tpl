<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
  <title>智云盘</title>
  <link rel="stylesheet" href="./static/layui-v2.5.7/css/layui.css">
  <link rel="stylesheet" href="static/zTree-v3/css/zTreeStyle/zTreeStyle.css" type="text/css">
  <link rel="shortcut icon" href="./static/img/micloud.ico" />
</head>
<body class="layui-layout-body">
<div class="layui-layout layui-layout-admin">
  <div class="layui-header">
    <div class="layui-logo"><img src="./static/img/micloud.svg" class="layui-nav-img">智云盘</div>
    <!-- 头部区域（可配合layui已有的水平导航） -->
    <ul class="layui-nav layui-layout-left">
      <div id="mydiskmenu">
        <li class="layui-nav-item"><a href="javascript:void(0);" id="uploadfile"><i class="layui-icon layui-icon-upload"></i>上传文件</a></li>
        <li class="layui-nav-item"><a href="javascript:void(0);" id="newfolder" onclick="OnNewFolder()"><i class="layui-icon layui-icon-add-circle-fine"></i>新建文件夹</a></li>
        <li class="layui-nav-item"><a href="javascript:void(0);" id="renamefolder" onclick="RenameFolder(-1)" class="layui-hide"><img src="./static/img/rename.svg" style="width:15px">重命名当前文件夹</a></li>
        <li class="layui-nav-item"><a href="javascript:void(0);" id="deletefolder" onclick="RemoveFolder(-1)" class="layui-hide"><img src="./static/img/delete.svg" style="width:15px">删除当前文件夹</a></li>
        <li class="layui-nav-item"><a href="javascript:void(0);" id="sharefolder" onclick="OnNewShare(-1,true)" class="layui-hide"><img src="./static/img/share.svg" style="width:15px">分享当前文件夹</a></li>
      </div>
      <div id="myrecyclebinmune" class="layui-hide">
        <li class="layui-nav-item"><a href="javascript:void(0)" id="deletefolder" onclick="OnClearRecyclebin()"><img src="./static/img/clearbin.svg" style="width:20px">清空回收站</a></li>
      </div>
      <div id="sharewithmemenu" class="layui-hide">
        <li class="layui-nav-item"><a href="javascript:void(0);" id="uploadsharefile"><i class="layui-icon layui-icon-upload"></i>上传文件</a></li>
        <li class="layui-nav-item"><a href="javascript:void(0);" id="newfolder" onclick="OnNewFolder()"><i class="layui-icon layui-icon-add-circle-fine"></i>新建文件夹</a></li>
        <li class="layui-nav-item"><a href="javascript:void(0);" id="renamefolder" onclick="RenameFolder(-1)" ><img src="./static/img/rename.svg" style="width:15px">重命名当前文件夹</a></li>
      </div>
    </ul>
    <ul class="layui-nav layui-layout-right">
      <li class="layui-nav-item">
        <a href="javascript:;">
          <img src="./static/img/user.svg" class="layui-nav-img">
          <span id="UserName">{{.username}}</span>
        </a>
        <dl class="layui-nav-child">
          <dd><a href="javascript:void(0);" onclick="MyConfig()">配置信息</a></dd>
          <dd><a href="javascript:void(0);" onclick="MyMessage()">我的资料</a></dd>
        </dl>
      </li>
      <li class="layui-nav-item"><a href="logout">退了</a></li>
    </ul>
  </div>
  
  <div class="layui-side layui-bg-black">
    <div class="layui-side-scroll">
      <!-- 左侧导航区域（可配合layui已有的垂直导航） -->
      <ul class="layui-nav layui-nav-tree"  lay-filter="test">
        <li class="layui-nav-item layui-nav-itemed">
          <a href="javascript:;" id="mydisk" onclick="OnMyDisk()">我的网盘</a>
        </li>
        <li class="layui-nav-item">
          <a href="javascript:;" onclick="OnMyShared()">我分享的</a>
        </li>
        <li class="layui-nav-item"><a href="javascript:;" onclick="OnSharedWithMe()">分享给我的</a></li>
        <li class="layui-nav-item"><a href="javascript:;" onclick="OnMyRecyclebin()">我的回收站</a></li>

        <li class="layui-nav-item" >
          <a href="javascript:;">统计数据</a>
          <dl class="layui-nav-child" id=diskstat></dl>
        </li>

        <li class="layui-nav-item layui-nav-itemed layui-hide" id ="uploadprogressli">
          <a href="javascript:;">上传进度<span class="layui-badge-dot" id="uploadbadge"></span></a>
          <dl class="layui-nav-child" style="padding-left: 15px;color:red;">文件上传时，请勿刷新页面！</dl>
          <dl class="layui-nav-child" id=fileprogress>
              <div class="layui-elip" style="padding-left:15px;" id="uploadfilename">文件1:</div>
              <div class="layui-progress layui-progress-big" lay-showPercent="yes" id="uploadprogress">
                <div class="layui-progress-bar" lay-percent="0%" id="uploadprogress"></div>
              </div>
          </dl>
        </li>
      </ul>

    </div>
  </div>
  
  <div class="layui-body">
    <!-- 内容主体区域 -->
    <div class="layui-row layui-bg-gray layui-form-item" style="padding:2px 0px 2px 0px;">
        <div class="layui-col-xs4 layui-col-sm7 layui-col-md8">
          <div class="layui-elip" style="text-align:left;line-height:40px;" id="dirpath">
            <a href="javascript:void(0)" onclick="RequestDirList(0,false)">我的网盘</a>
          </div>
        </div>
        <div class="layui-col-xs4 layui-col-sm5 layui-col-md4">
          <div class="layui-input-inline">
            <input type="text" name="username" lay-verify="required" placeholder="搜索文件" autocomplete="off" class="layui-input" id="searchfile" onblur="OnSearch()"> 
          </div>
          <button class="layui-btn layui-btn-primary" id="view_icon" onclick="OnViewChange(0)"><i class="layui-icon layui-icon-app"></i>图标</button>
          <button class="layui-btn layui-btn-primary" id="view_list" onclick="OnViewChange(1)"><i class="layui-icon layui-icon-list"></i>列表</button>
        </div>
    </div>
    <!--主题内容区域-->
    <div style="padding: 3px;" class="layui-row" id="master">
      主题区域
    </div>
  <div class="layui-footer">
    <!-- 底部固定区域 -->
    <div class="layui-col-md10"  id="footer"></div>
    <div class="layui-col-md2"  id="version">版本:{{.Version}}</div>
  </div>
</div>
<script src="./static/jquery/jquery-3.4.1.min.js"></script>
<script src="./static/jquery/jquery-cookie-1.4.1.min.js"></script>
<script src="./static/layui-v2.5.7/layui.js"></script>
<script src="./static/js/md5.js"></script>
<script src="static/zTree-v3/js/jquery.ztree.core.js"></script>
<script src="static/zTree-v3/js/fuzzysearch.js"></script>
<script src="static/zTree-v3/js/jquery.ztree.exhide.js"></script>
<script src="./static/js/treeseting.js"></script>
<script src="./static/js/page/index.js"></script>
<script>
  layui.use(['element','form','layer','upload','layedit'], function(){
    var element = layui.element;
    var form = layui.form;
    var layer  = layui.layer;
    var upload = layui.upload;
    var layedit = layui.layedit;

    form.render(); //更新全部
    var filename='',filesize=0,token='{{.Scene}}';
    var domain=DFS_DOMAIN;
    if(DFS_GROUP.length > 0){
      domain = DFS_DOMAIN+'/'+DFS_GROUP;
    }
    var uploadInst=upload.render({
      elem: '#uploadfile'
      ,url: domain+'/upload' //改成您自己的上传接口
      ,data: {'scene':'{{.Scene}}','output':'json','auth_token':'token','tstemp':''}
      //, size: 1024*1024                   //传输大小100k
      //, exts: 'jpg|png|gif|'        //可传输文件的后缀
      , accept: 'file'              //video audio images
      //****************传输操作相关设置
      //, headers:{'Content-Type':'multipart/form-data'}                   //额外添加的请求头
      , auto: true                                 //自动上传,默认是打开的
      //, bindAction: '#uploadfile'                    //auto为false时，点击触发上传
      , multiple: false                             //多文件上传
      //, number: 100                               //multiple:true时有效
      ,choose: function(obj){
        //将每次选择的文件追加到文件队列
        var files = obj.pushFile();
        
        //预读本地文件，如果是多文件，则会遍历。(不支持ie8/9)
        obj.preview(function(index, file, result){
          filename=file.name;
          $("#uploadfilename").text(filename);
          $("#uploadfilename").attr('title',filename);
          $("#uploadprogressli").attr("class","layui-nav-item layui-nav-itemed");
          //console.log(result);//得到文件base64编码，比如图片
          //obj.resetFile(index, file, '123.jpg'); //重命名文件名，layui 2.3.0 开始新增
          
          //这里还可以做一些 append 文件列表 DOM 的操作
          
          //obj.upload(index, file); //对上传失败的单个文件重新上传，一般在某个事件中使用
          //delete files[index]; //删除列表中对应的文件，一般在某个事件中使用
        });
      }
      ,before: function(obj){//执行之前动态加载参数
        var now = new Date();
        token=MD5(token+now.getTime()+USER_MSG.Password);
        this.data.auth_token=token;
        this.data.tstemp=now.getTime();
      }
      ,progress: function(n, elem){//上传进度
        var percent = n + '%' //获取进度百分比
        uploadProgress(percent);
      }
      ,done: function(res){
        if (res.url.length >0){
          NewFileIndex(filename,
            DIR_ID,
            res.domain,
            res.url,
            res.path,
            res.md5,
            res.scene,
            res.size,
            res.mtime,
            token
          );
        }else{
            layer.msg("上传文件失败:"+res.message); 
        }
      }
    });

    var uploadshareInst=upload.render({
      elem: '#uploadsharefile'
      ,url: DFS_DOMAIN+'/upload' //改成您自己的上传接口
      ,data: {'scene':'{{.Scene}}','output':'json','auth_token':'token','tstemp':''}
      , size: 1024*1024                   //传输大小100k
      , accept: 'file'              //video audio images
      //****************传输操作相关设置
      , auto: true                                 //自动上传,默认是打开的
      , multiple: false                             //多文件上传
      ,choose: function(obj){
        //将每次选择的文件追加到文件队列
        var files = obj.pushFile();
        obj.preview(function(index, file, result){
          filename=file.name;
          $("#uploadfilename").text(filename);
          $("#uploadfilename").attr('title',filename);
          $("#uploadprogressli").attr("class","layui-nav-item layui-nav-itemed");
        });
      }
      ,before: function(obj){//执行之前动态加载参数
        var now = new Date();
        token=MD5(token+now.getTime());
        this.data.auth_token=token;
        this.data.tstemp=now.getTime();
      }
      ,progress: function(n, elem){//上传进度
        var percent = n + '%' //获取进度百分比
        uploadProgress(percent);
      }
      ,done: function(res){
        if (res.url.length >0){
          NewFileIndex(filename,
            DIR_ID,
            res.domain,
            res.url,
            res.path,
            res.md5,
            res.scene,
            res.size,
            res.mtime,
            token
          );
        }else{
            layer.msg("上传文件失败:"+res.message); 
        }
      }
    });
});
</script>
</body>
</html>