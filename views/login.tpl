<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
 
    <title>登录页</title>
    <link rel="stylesheet" href="./static/layui-v2.5.7/css/layui.css">
    <link rel="stylesheet" href="./static/css/loginstyle.css">
 
</head>
<body>
 
<div class="login-main">
    <header class="layui-elip">登录</header>
    <div style="padding: 20px; background-color: #F2F2F2;">
    <form class="layui-form">
        <div class="layui-input-inline">
            <input type="text" name="name" required lay-verify="required" placeholder="用户账号" autocomplete="off"
                   class="layui-input">
        </div>
        <div class="layui-input-inline">
            <input type="password" name="pswd" required lay-verify="required" placeholder="密码" autocomplete="off"
                   class="layui-input">
        </div>
        <input name="page_login" id="captcha" type="hidden" value="false" >
        <div class="layui-input-inline login-btn">
            <button lay-submit lay-filter="login" class="layui-btn">登录</button>
        </div>
        <hr/>
        <p><a href="register" class="fl layui-btn layui-btn-primary layui-btn-xs">没有账号?立即注册</a><a href="javascript:;" class="fr layui-btn layui-btn-primary layui-btn-xs layui-btn-disabled">忘记密码？</a></p>
    </form>
    </div>
</div>
 
<script src="./static/layui-v2.5.7/layui.js"></script>
<script src="./static/js/md5.js"></script>
<script type="text/javascript">
    layui.use(['form','layer','jquery'], function () {
        // 操作对象
        var form = layui.form;
        var $ = layui.jquery;
        form.on('submit(login)',function (data) {
            data.field.pswd=MD5(data.field.pswd)
            //console.log(data.field);
            $.ajax({
                url:'login',
                data:data.field,
                dataType:'json',
                type:'post',
                success:function (data) {
                    //console.log(data);
                    if (data.status == 'ok'){
                        location.href = "/";
                    }else{
                        //layer.msg(data.message);
                        layer.msg("用户账号不存在或者密码不正确!");
                    }
                }
            })
            return false;
        })
 
    });
</script>
</body>
</html>